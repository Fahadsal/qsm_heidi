function nii = load_nii_raw(filename, importRoutine,Options)

%% LOAD_NII_RAW loads nifti without applying transformations.
%
% Loads NIFTI, ANALYZE, or MGZ dataset without applying any transformations.
% Support both *.nii and *.hdr/*.img file extension.
% If file extension is not provided, *.hdr/*.img will be used as default.
%
%
%
%   Syntax
%
%   LOAD_NII_RAW()
%   LOAD_NII_RAW(filename)
%   LOAD_NII_RAW(filename,importRoutine,Options)
%
%
%   Description
%
%   LOAD_NII_RAW()  Opens GUI
%
%   LOAD_NII_RAW(filename)   Opens file filename. This may either be a
%                            nifti file (*.nii) or a gzipped nifti-file
%                            (*.nii.gz)
%
%                            Options is piped to dispstatus. Further
%                            Options are:
%
%           isSpecialMultiVolumeFile - Boolean. Default: false
%                                      Use this, e.g., for ANTs warpfield.
%                                      In some cases the 4th dimension is
%                                      coded in a way that the standard
%                                      nifti-matlab package does not
%                                      recognize it. This is a workaround.
%
%
%
%   See also: LOAD_NII, WRITE_NII

% This function is a modified version of the NIFTI-toolbox load-nii.
%
% 2010/08/25 F Schweser, mail@ferdinand-schweser.de
%
% Changelog:
%
%   2010/09/06 - 1.1 - F Schweser - function call simplified + now creates
%                                   mids-object + argument "importRoutine"
%   2011/02/07 - 1.2 - F Schweser - Nowdoes not require explicit
%   specification of filename
%   2011/04/01 - 1.3 - F Schweser - Bugfix: importRoutine
%   2011/04/21 - 1.4 - F Schweser - Header-call changed to new mids type
%   2011/12/13 - 1.5 - F Schweser - Now supports gzipped nii-files + help
%                                   improved
%   2012/01/09 - 1.6 - F Schweser - Critical Bugfix: Now uses untouch_nii
%   2012/01/23 - 1.7 - F Schweser - Change 1.6 reverted, because header and
%                                   data did not match
%   012/03/27 -  1.8 - F Schweser - Improved handling of the case when both
%				     GZIPPED and NII present
%   2012/03/27 - 1.81 - Andreas    - Bugfix: if gzipped nii-files are loaded the
%                                   unzipped version is deleted
%				     
%   2012/12/03 - 1.9 - F Schweser - Now uses /tmp for gunzipping
%   2013/03/04 - 2.0 - A Deistung - Now uses /data/exchange/temparory_files ... for gunzipping
%   2013/03/28 - 2.1 - A Deistung - BugFix in temporary folder selection
%   2013/04/11 - 2.2 - F Schweser - Temporary folder handling improved
%   2013/07/22 - 2.3 - F Schweser - Bugfix: now uses random filename for unzip
%   2013/04/09 - 2.4 - A Deistung - mgz-data can now be imported
%   2013/11/05 - 2.5 - F Schweser - new feature: pipe Optsions to dispstatus
%   2013/12/03 - 2.6 - F Schweser - Now uses exec_system_command
%   2014/01/06 - 2.7 - X Feng     - now interprets the scaling provided by the hdr
%                                   and scales the data values accordingly.
%   2014/03/15 - 2.8 - F Schweser - GLOBAL_PATH_NIFTITOOL_SHEN, GLObAL_TEMPDIR
%   2014/05/05 - 2.9 - F Schweser - Minor code improvement
%   2014/09/29 - 2.0 - F Schweser - Added isSpecialMultiVolumeFile
%   2020/03/08 - 2.1 - F Schweser - Minor bugfix to adapt to R2018a
%   2020/08/27 - 2.2 - F Schweser - Weird bug that prevented loading hdr files (make sure this did not break anything)



global GLOBAL_TEMPDIR
global GLOBAL_PATH_NIFTITOOL_SHEN

if nargin < 1
    [filename, pathName] = uigetfile({'*.*','All Files'});
    filename = fullfile(pathName,filename);
end

if nargin < 3 || isempty(Options)
    Options.dummy = [];
end

if ~myisfield(Options,'isSpecialMultiVolumeFile') || isempty(Options.isSpecialMultiVolumeFile)
    Options.isSpecialMultiVolumeFile = false;
end


temporarydir = tempdir;
img_idx = [];
dim5_idx = [];
dim6_idx = [];
dim7_idx = [];
old_RGB = 0;

%initialize class
nii = mids();
if exist('importRoutine','var')
    nii.setImportRoutine(importRoutine);
end






addpath(GLOBAL_PATH_NIFTITOOL_SHEN);
%  Read the dataset header
%
if exist(filename,'file') && (~isempty(strfind(filename,'.hdr')) || ~isempty(strfind(filename,'.img')) || (~isempty(strfind(filename,'.nii')) && isempty(strfind(filename,'.nii.gz'))))
    % given data set is an analyze file
    wasNiiFile = true;
    [headerTmp,nii.sourceFileInfo.filetype,nii.sourceFileInfo.fileprefix,nii.sourceFileInfo.machine] = load_nii_hdr(filename);
    %filenameNii = filename;
elseif exist([filename '.gz'],'file') 
    wasNiiFile = false;
    % given file is nii but was zipped!
    
    dispstatus(['File ' filename ' is zipped. Unzipping in ' temporarydir ' ...'],Options)
%    temporarydir = tempdir;
    [~,filenameNew] = fileparts(tempname);
    [~,fileName,fileExt] = fileparts(filename);
    
    while exist(fullfile(temporarydir,[filenameNew fileExt '.gz']),'file') %&& ~unix(['touch ' fullfile(temporarydir,[filenameNew fileExt '.gz '])])
        [~,filenameNew] = fileparts(tempname); %find a filename that does not exist yet
    end
    exec_system_command(['cp ' filename '.gz ' fullfile(temporarydir,[filenameNew fileExt '.gz'])]);
    filename = fullfile(temporarydir,[filenameNew fileExt]);
    gunzip([filename '.gz']);
    %filenameNii = filename;
    [headerTmp,nii.sourceFileInfo.filetype,nii.sourceFileInfo.fileprefix,nii.sourceFileInfo.machine] = load_nii_hdr(filename);    
elseif exist(filename,'file') && ~isempty(strfind(filename,'.mgz'))
    % given filename is in mgz format    
    wasNiiFile = false;    
    
    tmpfilenamesuffix = tempname;
    tmpfilenamesuffix  = tmpfilenamesuffix(end-37:end);
    [~,fileName,fileExt] = fileparts(filename);
    niiFileExt = '.nii';
    
    tmpfilename = fullfile(temporarydir, [fileName(1:end-4) tmpfilenamesuffix fileName(end-3:end) niiFileExt]);
    dispstatus(['File ' filename ' is ' fileExt '. Converting to ' tmpfilename ' ...'],Options)
    
    runfreesurfercommand(['mri_convert -it ' fileExt(2:end) ' -ot ' niiFileExt(2:end) ' ' filename ' ' tmpfilename]);    

    filenameNii = tmpfilename;    
    filename = tmpfilename;

    [headerTmp,nii.sourceFileInfo.filetype,nii.sourceFileInfo.fileprefix,nii.sourceFileInfo.machine] = load_nii_hdr(filenameNii); 
    
elseif ~isempty(strfind(filename,'.nii.gz')) && exist(filename,'file')
    wasNiiFile = false;
    % given file is zipped
   
    tmpfilenamesuffix = tempname;
    tmpfilenamesuffix  = tmpfilenamesuffix(end-37:end);
    [~,fileName,fileExt] = fileparts(filename);
    tmpfilename = fullfile(temporarydir, [fileName(1:end-4) tmpfilenamesuffix fileName(end-3:end) fileExt]);
    dispstatus(['File ' filename ' is zipped. Unzipping in ' tmpfilename ' ...'],Options)
 
    copyfile(filename, tmpfilename)


    filename = tmpfilename;
    gunzip(filename);
    filenameNii = filename(1:end-3);
    [headerTmp,nii.sourceFileInfo.filetype,nii.sourceFileInfo.fileprefix,nii.sourceFileInfo.machine] = load_nii_hdr(filenameNii); 
else
    error('This file does not exist.')
end
% 
% if nii.sourceFileInfo.filetype == 0
%     headerTmp = load_untouch0_nii_hdr(nii.sourceFileInfo.fileprefix,nii.sourceFileInfo.machine);
%     nii.sourceFileInfo.ext = [];
% else
%     headerTmp = load_nii_hdr(filenameNii);
%     nii.sourceFileInfo.ext = load_nii_ext(filenameNii);
% end
% 


%  Read the dataset body
%

if Options.isSpecialMultiVolumeFile
    headerTmp.dime.dim = [headerTmp.dime.dim(1:4) headerTmp.dime.dim(6) headerTmp.dime.dim(5) headerTmp.dime.dim(7:end)];
end
try
    [nii.img,headerTmp] = load_nii_img(headerTmp,nii.sourceFileInfo.filetype,nii.sourceFileInfo.fileprefix, ...
    nii.sourceFileInfo.machine);%,img_idx,dim5_idx,dim6_idx,dim7_idx,old_RGB);
catch  %try again with zipped (if unzipping was aborted before, NII file may be corrupted)
    dispstatus('This NII-file is corruped. Trying to find GZ-file...',Options)
    wasNiiFile = false;
    temporarydir = fullfile(tempname);
    exec_system_command(['cp ' filename '.gz ' temporarydir]);
    [~,fileName,fileExt] = fileparts(filename);
    filename = fullfile(temporarydir,[fileName fileExt]);
    gunzip([filename '.gz']);
    delete(filename);
    %filenameNii = filename;
    [headerTmp,nii.sourceFileInfo.filetype,nii.sourceFileInfo.fileprefix,nii.sourceFileInfo.machine] = load_nii_hdr(filename);
    [nii.img,headerTmp] = load_nii_img(headerTmp,nii.sourceFileInfo.filetype,nii.sourceFileInfo.fileprefix, ...
        nii.sourceFileInfo.machine,img_idx,dim5_idx,dim6_idx,dim7_idx,old_RGB);
end


%  if scl_slope field is nonzero, then each voxel value in the
%  dataset should be scaled as: y = scl_slope * x + scl_inter
%  I bring it here because hdr will be modified by change_hdr.
if headerTmp.dime.scl_slope ~= 0 && ...
        ismember(headerTmp.dime.datatype, [2,4,8,16,64,256,512,768]) && ...
        (headerTmp.dime.scl_slope ~= 1 || headerTmp.dime.scl_inter ~= 0)
    
    warning('switoolbox:load_nii_raw', 'Scaling by the NIFTI header was detected. This scaling was applied. Please check whether the imported data is correct.')
    nii.img = ...
        headerTmp.dime.scl_slope * double(nii.img) + headerTmp.dime.scl_inter;
    
    if headerTmp.dime.datatype == 64
        
        headerTmp.dime.datatype = 64;
        headerTmp.dime.bitpix = 64;
    else
        nii.img = single(nii.img);
        
        headerTmp.dime.datatype = 16;
        headerTmp.dime.bitpix = 32;
    end
    
    headerTmp.dime.glmax = max(double(nii.img(:)));
    headerTmp.dime.glmin = min(double(nii.img(:)));  
end


rmpath(GLOBAL_PATH_NIFTITOOL_SHEN);

if exist('importRoutine','var')
    nii = flipimporteddata(nii,importRoutine);
end
nii.setHeader(headerTmp);
% Note, that this function already returns the data in the correct DICOM corrdinate system.
% Therefore, the data has to be converted:
nii.setCoordinateSystemType('NIFTI');
nii.setImportHeaderType('NIFTI');
%nii.untouch = 1;

if ~wasNiiFile
    [~, ~, ext] = fileparts(filename);
    
    if strcmp(ext, '.gz')%% || ~isempty(strcmp(ext, '.gz'))
        delete(filename(1:end-3));
    end
    delete(filename);
end


return				

