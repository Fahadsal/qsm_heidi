function dataObject = flipimporteddata(dataObject,type)


%FLIPIMPORTEDDATA Flips imported data accorting to the import-routine.
%
%   FLIPIMPORTEDDATA Flips imported data accorting to the import-routine.
%
%
%   Syntax
%
%   FLIPIMPORTEDDATA(A)
%   FLIPIMPORTEDDATA(A,type)
%
%
%
%   Description
%
%   FLIPIMPORTEDDATA(A) flips mids data object orientation of A.img
%   according to importRoutine property.
%
%   FLIPIMPORTEDDATA(A,type) flips data object A.img to normal orientation
%   by assuming the import-routine "type" was used. The follwing
%   import-routines are supported:
%           'mri_convert'
%           'niftimatlib' (nifti()-function of niftimatlib toolbox)
%           'getdcmdata' 
%           'getdcmdata_vision_raw'
%           'spm_dicom_convert'
%           'dtk'
%           'override' forces no action
%           'odinreco' for nifti files from odinreco
%           'graz' for data from Graz (Langkammer)
%           'grosskreuz' for data from J. Grosskreuz (Jena, ALS)
%
%
%   
%   See also: IMPORTDICOMDATA

%   2010/09/04 - F Schweser - mail@ferdinand-schweser.de
%
%   Changelog
%
%   2010/09/04 - v1.0 - F Schweser - New.
%   2010/09/13 - v1.1 - F Schweser - Automatic mode added-
%   2010/10/09 - v1.2 - F Schweser - Bugfix: type empty
%   2010/12/01 - v1.3 - F Schweser - override mode added
%   2010/12/06 - v1.31- A Deistung - help supported import routines extended
%   2010/12/10 - v1.4- A Deistung  - flip for 'getdcmdata_vision_raw' added 
%   2010/12/08 - v1.5 - F Schweser - odinreco mode added
%   2010/12/14 - v1.51 - F Schweser - Bugfix: odinreco mode 
%   2010/12/15 - v1.52 - F Schweser - Compatibility with plain matrixes
%   added
%   2010/12/15 - v1.53 - F Schweser - dtk changed (seemes to be
%   incompatible)
%   2011/04/01 - v1.6  - F Schweser - Graz mode added
%   2011/04/12 - v1.7  - F Schweser - 'fsl_bet' mode added
%   2011/04/14 - v1.8  - F Schweser - getdcmdata reorientation removed and
%                                     put to importdicomdata(), since this
%                                     reorientation is necessary in order
%                                     to make data match the dicom header
%   2011/09/13 - v1.9  - A Deistung - philipsdicom mode was added
%   2011/10/25 - v2.0  - F Schweser - grosskreuz mode was added
%   2011/01/04 - v2.1  - F Schweser - bugfix mri_convert mode
%   2011/01/09 - v2.2  - F Schweser - spm_dicom_convert changed
%   2012/06/05 - v2.3  - A Deistung - 'fsl_bet' mode deleted
%   2012/07/16 - v2.4  - F Schweser - new mode ohio
%   2012/09/21 - v2.5  - A Deistung - new mode 'importbrukerrawdata'
%   2012/10/08 - v2.6  - F Schweser - changed grosskreuz mode: added x-flip


if nargin < 2 || isempty(type)
        type = dataObject.importRoutine;
end

if ~isobject(dataObject)
    dataObject.img = dataObject;
    wasObject = false;
else
    wasObject = true;
end




if ~isempty(type)

    % testweise alles deaktiviert
    switch type
        case 'mri_convert'
             %dataObject.img = flipdim(flipdim(dataObject.img,1),2);
             %dataObject.img = flipdim(dataObject.img,3);             
        case 'philipsdicom'
             dataObject.img = flipdim(flipdim(dataObject.img,1),2);
        case 'dtk' % dti tensor reko (DG)
            %dataObject.img = flipdim(dataObject.img,1);
        case 'getdcmdata' 
            % note, this function is for flipping of nifti data only!
        case 'ohio' 
            dataObject.img = flipdim(dataObject.img,3);
        case 'getdcmdata_vision_raw'
            % see getdcmdata
        case {'spm_dicom_convert','xflip'}
            dataObject.img = flipdim(dataObject.img,1);
        case 'odinreco'
            dataObject.img = flipdim(permute(dataObject.img,[2 1 3 4 5 6 7]),1);
        case 'override'
            % do nothing (forced)
        case 'graz'
            dataObject.img = flipdim(flipdim(dataObject.img,2),1);
            warning('Data is flipped now!')
        case 'grosskreuz'
            dataObject.img = flipdim(flipdim(dataObject.img,2),1);
        case 'importbrukerrawdata'   
%             dataObject.img = flipdim(flipdim(permute(dataObject.img,[1 3 2 4 5 6 7]),3),1);
%             dataObjectVoxelSize = dataObject.getvoxelsize;
%             dataObject.setvoxelsize([dataObjectVoxelSize(1),dataObjectVoxelSize(3),dataObjectVoxelSize(2)])
            %dataObject.img = flipdim(flipdim(dataObject.img,2),3);
        case 'yflip'
            dataObject.img = flipdim(dataObject.img,2);
        case 'xyzflip'
            dataObject.img = flipdim(flipdim(flipdim(dataObject.img,2),1),3);
        otherwise
            error('Import routine (type) currently not supported.')

    %inverted, since dcm-header rotation did not match the mri_convert data...
    %     case 'mri_convert'
    %         % dcm-data that were converted using mri_convert has the correct
    %         % DICOM patient coordinate system orientation (compare with
    %         % dicomread())
    %         dataObject.img = flipdim(dataObject.img,1);
    %     case 'dtk' % dti tensor reko (DG)
    %     case 'getdcmdata' 
    %     case 'spm_dicom_convert'
    %         dataObject.img = flipdim(flipdim(dataObject.img,2),1);
    %     otherwise
    %         error('Import routine (type) currently not supported.')
    %   2010/10/04 - v1.2 - F Schweser - second dimension flipped for all cases
    %                                    since inversion with rotational
    %                                    information from dcm header did not
    %                                    succeed. Seems to work now.

    end
end

if ~wasObject
    dataObject = dataObject.img;
end


end