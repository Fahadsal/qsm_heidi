function y =  fftsave(x,sizeVector,dimension)

%FFTSAVE Fast Forier Transform (FFT) for centered data.
%
%   FFTSAVE computes the FFT of data that is centered with respect to
%   origin of coordinate-space, i.e., FFT-shifts are applied correctly.
%
%
%   Syntax
%
%   FFTSAVE(X)
%   FFTSAVE(X,b)
%   FFTSAVE(X,[],d)
%
%
%
%   Description
%
%   Y = FFTSAVE(X) computes the FFT of N-D array X.
%
%   Y = FFTSAVE(X,b) scales X to size specified by b by padding X with
%   zeros (see documentation of FFTN for details).
%
%   Y = FFTSAVE(X,[],d) applies FFT only in dimension d. This may be integer
%   or string of '2d' or '3d' for performing only 2D or 3D FFT,
%   respectively.
%
%
%
%   See also: IFFTSAVE, FFTN, IFFTN, FFTSHIFT, IFFTSHIFT


%   2009/05/28 F Schweser and BW Lehr, mail@ferdinand-schweser.de
%
%   Changelog:
%   2009/06/11 - F Schweser: sizeVector argument added.
%   2009/08/05 - F Schweser: Documentation added.
%   2011/06/20 - A Deistung: dimension and type argument were added
%   2012/01/18 - F Schweser: doc improved + new feature: 2D FFT
%   2012/08/20 - A Deistung: new feature 3D FFT
%   2012/09/23 - F Schweser: in place call no longer supported because does not make sense (different variable type) FFT
%   2013/03/07 - A Deistung: BugFix: fft for a specific dimension


if nargin < 2 || isempty(sizeVector)
    sizeVector = [];
end

if nargin < 3 || isempty(dimension)
    dimension = [];
end



if isempty(dimension)
    y = fftshift(fftn(ifftshift(x),sizeVector));
elseif isnumeric(dimension)
    y = fftshift(fft(ifftshift(x, dimension),sizeVector,dimension), dimension);
elseif strcmpi(dimension,'2d')
    y = fft(ifftshift(x),sizeVector,1);
    y = fftshift(fft(y,sizeVector,2));
elseif  strcmpi(dimension,'3d')
    y = fft(ifftshift(x),sizeVector,1);
    y = fft(y,sizeVector,2);
    y = fftshift(fft(y,sizeVector,3));
end

end