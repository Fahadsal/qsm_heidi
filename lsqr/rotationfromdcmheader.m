function Rotation = rotationfromdcmheader(dcmHeader,regMatrix)

%ROTATIONFROMDCMHEADER Extracts rotation from DCM header.
%
%   ROTATIONFROMDCMHEADER Extracts rotation from DCM header.
%
%
%   Syntax
%
%   ROTATIONFROMDCMHEADER(h)
%   ROTATIONFROMDCMHEADER(h,M)
%
%
%   Description
%
%   ROTATIONFROMDCMHEADER(h) returns the position of the dcm-data with
%   respect to the scanner-system.
%   The values are returned as a struct that contains the following fields:
%       'axis' and 'angle' - rotation axis and angle in degrees.
%       'matrix'           - rotation matrix (does not include scaling!)
%       'affine4x4Matrix'  - 4x4 affine transformation matrix (includes scaling)
%
%   ROTATIONFROMDCMHEADER(h,M) applies 3x3 rotation matrix M before calculation of 
%   the position. This option may be used if registration of the data was
%   performed after acquisition (registration matrix M).
%
%
%   See also: IMPORTDICOMDATA, VRROTVEC

%   F Schweser, 2009/08/25, mail@ferdinand-schweser.de
%
%   Changelog:
%   v1.1 - 2009/08/27 - F Schweser - Added support for matrix and vector output
%                                    and rotation matrix M.
%   v1.2 - 2010/09/01 - F Schweser - Very, very ugly bug in the matrix and
%                                    vector/angle computation fixed.
%   v1.3 - 2010/09/03 - F Schweser - Now also returns 4x4 affine matrix. 
%   v1.4 - 2010/09/22 - F Schweser - Now also returns Euler angles
%   v1.5 - 2011/04/14 - F Schweser - Critical Bugfix: Rotation matrix was wrong!
%                                    + now returns system type
%   v1.51 - 2011/07/13 - K Sommer  - bugfix: more than one input header was requested 
%   v1.6  - 2011/09/20 - F Schweser  - rad2deg replaced by myrad2deg
%                                    because rad2deg is part of MAP_Toolbox
%                                    (Licenses)
%   v1.7  - 2011/10/19 - F Schweser - CRITICAL BUGFIX: Evaluation of regMatrix
%                                    was wrong. regMatrix and header matrix
%                                    do not commute! This bug, e.g., resulted in
%                                    wrong calculation of COSMOS maps!
%   v1.71  - 2012/01/04 - F Schweser - typo
%   v1.8   - 2014/03/31 - F Schweser - Check of rotation matrix
%   v1.9   - 2014/08/06 - F Schweser - Rounding for proper test

if nargin < 2 || isempty(regMatrix)
    regMatrix = 1;
end

if iscell(dcmHeader)
    % input is header array
    dcmHeader = dcmHeader{1};
end

% if numel(dcmHeaderInput) == 1
%     error('Determination ofcorrect orientation information from DICOM header requires all headers.')   -> caused error messages, not clear why this check is performed?!
% end 

tmp =  cellstr(dcmHeader(1).PatientPosition);
switch tmp{1}
    case 'HFS'
    otherwise
        warning('Patient position is not Head-First/Supine (HFS). Rotation information may be erroneous.')
end


Rotation.systemType = 'DICOM';


% the direction coseines for row and column are obtained from the normal
% DICOM header fields. Freesurfer does it the same way
Rotation.matrix(1,1) = dcmHeader(1).ImageOrientationPatient(1);
Rotation.matrix(2,1) = dcmHeader(1).ImageOrientationPatient(2);
Rotation.matrix(3,1) = dcmHeader(1).ImageOrientationPatient(3);
Rotation.matrix(1,2) = dcmHeader(1).ImageOrientationPatient(4);
Rotation.matrix(2,2) = dcmHeader(1).ImageOrientationPatient(5);
Rotation.matrix(3,2) = dcmHeader(1).ImageOrientationPatient(6);


% the normal of the orientation is also obtained from the normal header
% here. Freesurfer extracts this from the
% sSliceArray.asSlice[0].sNormal.dSag/Tra/Cor of the ASCII header. If it turns out that our procudre fails from time to time, we should change this. 
Rotation.matrix(:,3) =  cross(Rotation.matrix(:,1),Rotation.matrix(:,2));
% cross(x,y) (not y,x) because z goes from feet to head in DICOM system.


% note, that this function does not detect SIEMENS-DICOM slice reversal! Sometimes,
% slices are saved in reverse order. This must be compensated for by the
% import routine. This function only returns 'proper' rotation matrices
% that allow calculation of rotation axis and angle. Thus, this function
% assumes that slice reversal has been corrected! See, e.g.,
% importdicomdata.m.





Rotation.matrix = Rotation.matrix * regMatrix;

if abs(det(Rotation.matrix)-1) > 0.0001
    disp('Calculated rotation matrix is improper, i.e. it contains reflections. This would result in improper angle calculations in the following (e.g. using vrrotmat2vec). For this reason I skipping angle caluclations.')
else
    vectorExpressionTmp = vrrotmat2vec(Rotation.matrix);
    
    Rotation.axis  = vectorExpressionTmp(1:3);
    Rotation.angle = myrad2deg(vectorExpressionTmp(4));
    Rotation.eulerAngles = myrad2deg(orth2ang(Rotation.matrix));
end

Rotation.translationVector = reshape(dcmHeader(1).ImagePositionPatient,[1 3]);


Rotation.affine4x4Matrix = eye(4,4);
Rotation.affine4x4Matrix(1:3,1:3) = diag(voxelaspectratio(dcmHeader)) * Rotation.matrix;
Rotation.affine4x4Matrix(1:3,4) = Rotation.translationVector;









%% member functions

function ang = orth2ang(orthm)
    % from Google Groups
    
    ang(1) = asin(orthm(1,3)); %Wei du
    ang(2) = angle( orthm(1,1:2)*[1 ;i] ); %Jing Du
    yz = orthm* ...
        [orthm(1,:)',...
         [-sin(ang(2)); cos(ang(2)); 0],...
         [-sin(ang(1))*cos(ang(2)); -sin(ang(1)*sin(ang(2)));
    cos(ang(1))] ];

    ang(3) = angle(yz(2,2:3)* [1; i]); % Xuan Du 
end


function angleInDegrees = myrad2deg(angleInRadians)

angleInDegrees = (180/pi) * angleInRadians;

end



end