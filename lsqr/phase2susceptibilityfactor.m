function retVal = phase2susceptibilityfactor(Header,CustomValues)

% PHASE2SUSCEPTIBILITYFACTOR Computes conversion factor for susceptibility
%
%   This function computes the conversion factor from phase in rad to
%   susceptibility (SI).
%
%
%   Syntax
%   
%   PHASE2SUSCEPTIBILITYFACTOR(H)
%   PHASE2SUSCEPTIBILITYFACTOR(H,C)
%   PHASE2SUSCEPTIBILITYFACTOR([],C)
%
%
%
%   Description
%
%   PHASE2SUSCEPTIBILITYFACTOR(H) returns the factor corresponding to
%   dcm-header H of the data-set.
%
%   PHASE2SUSCEPTIBILITYFACTOR(H,C) uses the custom-values specified in the 
%   struct C. Currently supported fields:
%       echoTimes 		- echo time in ms.
%	magneticFieldStrength   - static magnetic field strength in Tesla
%	echoNo 			- Use this field for multi-echo data. The value specifies the 
%			          echo number to be used for calculation of the factor.
%				  (default: last echo)
%       signConvention  - string for specification of the sign convention
%                         of the phase data. The string can be one of the
%                         following:
%                           'rotatingFrame'   (default) - venous vessels bright
%                           'complex'                   - venous vessels dark
%                         For details see Hagberg G, MRI 2010;28(2):297-300.
%	
%   PHASE2SUSCEPTIBILITYFACTOR([],C) use this call if no header is available.

%   2010/02/02 - F Schweser - mail@ferdinand-schweser.de
%
%   Changelog:
%
%   v1.1 - 2010/03/16 - F Schweser - magneticFieldStrength custom-value added
%   v1.2 - 2011/02/25 - F Schweser - signConvention custom-value added
%   v1.21 - 2011/02/28 - F Schweser - disp added + default changed to rotating frame
%   v1.21 - 2011/03/28 - F Schweser - default display changed
%   v1.3 - 2011/07/05 - F Schweser - now works with multi-echo data
%   v1.4 - 2012/01/26 - F Schweser - echoTime renamed to echoTimes


DEFAULT_SIGNCONVENTION = 'rotatingFrame';


if nargin < 2 
    CustomValues.dummy = [];
end

% to ensure compatibility...
if isfield(CustomValues,'echoTime')
    warning('PHASE2SUSCEPTIBILITYFACTOR:echoTimeFiledDeprecated','Use of echoTime field is deprecated. Please use echoTimes instead.');
    CustomValues.echoTimes = CustomValues.echoTime;
end


if ~myisfield(CustomValues,'echoTimes') || isempty(CustomValues.echoTimes)
    echoTime = Header.EchoTime;
else
    echoTime = CustomValues.echoTimes;
end

if ~myisfield(CustomValues,'echoNo') || isempty(CustomValues.echoNo)
    echoTime = echoTime(end);
else
    echoTime = echoTime(CustomValues.echoNo);
end


if ~myisfield(CustomValues,'signConvention') || isempty(CustomValues.signConvention)
    CustomValues.signConvention = DEFAULT_SIGNCONVENTION;
    disp('Using default sign convention (rotating frame).');
end




if nargin < 2 || ~myisfield(CustomValues,'magneticFieldStrength') || isempty(CustomValues.magneticFieldStrength)
    magneticFieldStrength = Header.MagneticFieldStrength;
else
    magneticFieldStrength = CustomValues.magneticFieldStrength;
end



Constants = getconstants();


retVal = 1 / (Constants.gyromagneticRatio * (echoTime * 1e-3) * magneticFieldStrength);

switch CustomValues.signConvention
    case 'complex'
        retVal = -retVal;
    case 'rotatingFrame'
    otherwise
        error('Sign convention must be either >complex< or >rotatingFrame<!')
end



end