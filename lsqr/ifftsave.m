function x = ifftsave(x,sizeVector,type, dimension)

%IFFTSAVE Inverse Fast Forier Transform (FFT) for centered data.
%
%   IFFTSAVE computes the inverse FFT of data that is centered with respect
%   to the origin of coordinate-space, i.e., FFT-shifts are applied correctly.
%
%
%   Syntax
%
%   IFFTSAVE(X)
%   IFFTSAVE(X,b)
%   IFFTSAVE(..., 'symmetric')
%   IFFTSAVE(X,b, 'symmetric', d)
%
%
%
%   Description
%
%   Y = IFFTSAVE(X) computes the inverse FFT of N-D array X.
%
%   Y = IFFTSAVE(X,b) scales X to size specified by b by padding X with 
%   zeros (see documentation of ifftn for details). 
%
% 	Y = IFFTSAVE(..., 'symmetric') causes ifftsave to treat X as conjugate 
%   symmetric. This option is useful when X is not exactly conjugate symmetric, 
%   merely because of round-off error.
%
% 	Y = IFFTSAVE(..., 'symmetric', d) causes ifftsave to treat X as conjugate 
%   symmetric. IFFT is only performed in the dimensions specified by d. d
%   may be integer or string '2d' for 2D-FFT or '3d' for 3D-FFT.

%
%   (Function supports in-place calls using X)
%
%   See also: FFTSAVE, FFTN, IFFTN, FFTSHIFT, IFFTSHIFT


%   2009/05/28 F Schweser and BW Lehr, mail@ferdinand-schweser.de
%
%   Changelog:
%   2009/06/11 - F Schweser: sizeVector argument added.
%   2009/08/05 - F Schweser: Documentation added.
%   2009/08/31 - F Schweser: symmetry-argument added.
%   2011/06/20 - A Deistung: dimension argument was added
%   2012/01/18 - F Schweser: new feature: 2D iFFT
%   2012/08/20 - A Deistung: new feature: 3D iFFT
%   2012/10/09 - F Schweser: Bugfix: numeric type did not work
if nargin < 2 || isempty(sizeVector)
    sizeVector = [];
end

if nargin < 3 || isempty(type)
    type = 'nonsymmetric';
end

if nargin < 4 || isempty(dimension)
    dimension = [];
end

if isempty(dimension)
    x = fftshift(ifftn(ifftshift(x),sizeVector,type));
elseif isnumeric(dimension)
    x = fftshift(ifft(ifftshift(x, dimension),sizeVector,dimension, type), dimension);
elseif strcmp(dimension,'2d')
    x = ifft(ifftshift(x),sizeVector,2, type);
    x = fftshift(ifft(x,sizeVector,1, type));
elseif strcmp(dimension,'3d')
    x = ifft(ifftshift(x),sizeVector,3, type);
    x = ifft(x,sizeVector,2, type);
    x = fftshift(ifft(x,sizeVector,1, type));
else
    error('This fourier transform type is not supported.')
end

end