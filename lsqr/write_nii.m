function write_nii(volume2, name, header, dataType, saveType, preview, noGzip)
%WRITE_NII Writes N-D array to disk.
%
%   WRITE_NII writes data array to disk.
%
%
%   Syntax
%
%   WRITE_NII(volume)
%   WRITE_NII(volume, filename)
%   WRITE_NII(volume, filename, header/voxelaspecratio)
%   WRITE_NII(volume, filename, header/voxelaspecratio, dataType)
%   WRITE_NII(volume, filename, .., dataType, saveType)
%   WRITE_NII(volume, filename, .., dataType, saveType, preview)
%   WRITE_NII(volume, filename, .., dataType, saveType, preview,true)
%
%
%   Description
%
%   WRITE_NII(volume) writes volume as analyze-file to disk.
%   Filename is volume.nii. Volume may be img/hdrDcm struct or plain data-
%   matrix.
%
%   ATTENTION: since this is not using a true nifti format which could
%   hold up to 7 dimensions, only 4 dimensions may be written this way using write_nii
%
%   WRITE_NII(volume, filename) specifies filename.
%
%   WRITE_NII(volume, filename, header) uses dcm header information in the header-struct.
%   Currently supported features: voxel-size, qForm (slice position). Note,
%   this does only work with plain matrix data - not with struct-data type.
%
%   WRITE_NII(volume, filename, voxelsize) defines the voxelsize. Note,
%   this does only work with plain matrix data - not with struct-data type.
%
%   WRITE_NII(volume, filename, .., dataType) converts the file into the
%   dataformat specified by dataType. If dataType equals 'orig' everything
%   will be left as it was
%
%   WRITE_NII(volume, filename, .., dataType, saveType) if not specified by
%   the filename-suffix the file will be saved either in the 'nii'
%   (default) or in the 'img'/'hdr' format
%
%   WRITE_NII(volume, filename, .., dataType, saveType, preview) saves an
%   additional 'png' file if set to true.
%
%   WRITE_NII(volume, filename, .., dataType, saveType, preview, true) do
%   not gzip files.

%   2009/04/20 F Schweser and BW Lehr, mail@ferdinand-schweser.de
%
%   Changelog:
%   2009/04/28 - F Schweser: Support for complex valued arrays added.
%   2009/07 -    F Schweser: Support for arbitrary data-type added.
%   2009/02/26 - F Schweser: header-support
%   2010/08/06 - B Lehr: Rearanged function call
%   2010/08/23 - F Schweser: Now supports qForm.
%   2010/08/25 - F Schweser: Now supports struct data type.
%   2010/08/30 - F Schweser: Bugfix: Flipdim removed for hdrDcm (use
%                            load_nii_raw)
%   2010/09/06 - F Schweser: Now supporst mids object + Bugfix: empty
%                            hdrDcm + Bugfix load_nii_raw
%   2011/02/08 - F Schweser: Bugfix: No voxel aspect ratio
%   2011/02/21 - A Deistung: Bugfix: for getdcmdata-mode, first header is
%                            now extracted with spm_dicom_headers
%   2011/03/18 - A Deistung: Bugfix: voxelsize is now extracted using the
%                            getvoxelsize method of the mids object
%   2011/03/29 - A Deistung: Bugfix: now logical arrays can be saved as
%                            nii. These data are converted into uint8 data.
%   2011/04/05 - F Schweser: Bugfix: Minor change due to mids handle
%   2011/04/20 - F Schweser: addpath for fslorient
%   2011/04/21 - F Schweser: Header-management changed for new mids-
%   2011/05/06 - A Deistung: svn conflicts resolved
%   2011/06/07 - A Deistung: system-call of fslorient was replaced by
%                            runfreesurfercommand
%   2011/06/14 - A Deistung: BugFix: if spm_dicom_convert fails data will
%                            be saved with nifti or without header
%   2011/06/14 - A Deistung: BugFix: if spm_dicom_convert fails
%   2011/07/21 - A Deistung: mids objects without header information are
%                            now treated as plain matrices with a certain voxelsize
%   2011/10/20 - M Kr??mer  : BugFix: system command 'touch name' would fail
%                            on complex or long directories because of
%                            missing quotation characters around the param
%   2011/12/13 - F Schweser: No automatically gzipps files.
%   2012/01/04 - F Schweser: Minor Bugfix regarding gzipping + minor code
%                            style optimizations 
%                            + Major Bugfix regarding writing of mids objects
%                            in DICOM corrdinate system (was not converted
%                            to NIFTI prior to saving!!!) 
%                            + Now uses NIFTI header by default (was
%                            converted from DICOM before) plus prints
%                            warning message if DICOM header is transformed
%                            (note that NIFTI must be loaded with
%                            spm_dicom_convert import routine specification
%                            to get proper header!)
%   2012/06/12 - A Deistung: if the savefolder does not exist, than this
%                            folder is now generated automatically
%   2012/08/30 - F Schweser: Bugfix regarding previously added feature:
%                            isempty
%   2012/08/31 - F Schweser: Bugfix gzip
%    2012/09/05 - F Schweser: Now supports Lossless 2000 JPEG DICOM source
%                                           by using modified SPM8
%                                           functions
%    2012/09/06 - F Schweser: Bugfix regarding unvonvertable DICOM header
%    2012/09/06 - F Schweser: Bugfix regarding unvonvertable DICOM header, again
%    2012/09/10 - A Deistung: Bugfix: mri_convert now works with filenames
%                             with spaces
%    2012/09/10 - F Schweser: Major bugfix: FLIPIMPORTEDDATA was never
%                             reached
%    2012/10/12 - F Schweser: Major bugfix: Now uses only one DCM image for
%                             determining the NIFTI-header by using dcm2nii
%                             instead of mri_convert. Previously, a DICOM
%                             series was load even if images were actually
%                             not belonging together!!!
%    2012/10/22 - F Schweser: Bugfix Dicom-mode. Data was flipped (?)
%    2012/12/04 - A Deistung: Bugfix Dicom-mode. dcm2nii was replace by
%                             mri_convert
%    2013/10/02 - F Schweser: Bugfix regarding mri_convert (previous
%                             change). Now, first dcm2nii is used (as
%                             before) and mri_convert is only used as a
%                             fallback solution.
%    2013/10/08 - A Deistng:  the path to dcm2nii was added
%    2013/10/18 - F Schweser: Switched the order of mri_convert and dcm2nii
%                             because of orientation errors in sagittal
%                             data (2D).
%    2013/11/05 - F Schweser: Now catches freesufer errors.
%    2013/11/25 - F Schweser: Now uses exec_system_command instead
%                                    of system()
%    2013/12/02 - F Schweser: Bugfix: mids without header
%    2013/12/03 - F Schweser: Now uses exec_system_command
%    2013/12/05 - A Deistung: error throwing of mri_convert commands is
%                             switched of to enable the error fallback mechanism implemented in
%                             write_nii update of the used mri_convert function
%    2014/03/14 - F Schweser: GLOBAL_PATH_SPM, GLOBAL_PATH_NIFTITOOL_SHEN, GLOBAL_FILE_DCM2NII, GLOBAL_TEMPDIR           
%    2015/07/15 - F Schweser: MAJOR bugfixes regarding writing of Bruker data
%    2015/11/13 - F Schweser: Now correctly writes Toshiba raw images

dispstatus(mfilename(),[],'functioncall')
addpath(fileparts(mfilename('fullpath')))

global GLOBAL_PATH_FREESURFER
global GLOBAL_FILE_DCM2NII
global GLOBAL_PATH_NIFTITOOL_SHEN
global GLOBAL_PATH_SPM

DEFAULT_TMPDIR = tempdir;

if nargin < 2 || isempty(name)
    name = inputname(1);
end
if numel(name) < 1
    error('Not enough input arguments.');
end

if nargin < 7 || isempty(noGzip)
    noGzip = false;
end

[pathstr, ~, ~] = fileparts(name);
if ~isempty(pathstr) && ~isdir(pathstr)
    mkdir(pathstr);
end

if isobject(volume2)
    volume = volume2.clone;
    boolmidsHdrExists = ~(isempty(volume.getHeader('NIFTI')) && isempty(volume.getHeader('DICOM')) && isempty(volume.getHeader('BRUKER'))  && isempty(volume.getHeader('RAW')));
    if ~boolmidsHdrExists
        volume = volume.img;
    end
else
    volume = volume2;
    boolmidsHdrExists = false;
end
isBruker = false;
isUseQform = false;
if (isstruct(volume) || isobject(volume)) && boolmidsHdrExists % input is data struct/object
    if strcmp(volume.getCoordinateSystemType,'reflectionCorrected') 
         volume.switchCoordinateSystemType('original');
    end
    volume.switchCoordinateSystemType('NIFTI');
    if isobject(volume)
        
        if ~isempty(volume.getHeader('NIFTI')) %nifti header (or converted dcm-header) available
            
            voxelSize = volume.getvoxelsize;
            header = volume.getHeader('NIFTI');
            %volume = volume.img;
            
            qform = [header.hist.srow_x header.hist.srow_y header.hist.srow_z 0 0 0 1];
            reshapedqform = reshape(qform', [16,1]);
            qformstring = '';
            for i = 1:16
                qformstring = cat(2, qformstring, num2str(reshapedqform(i)), ' ');
            end
            
            isUseQform = true;
            
            
            
        elseif ~isempty(volume.getHeader('DICOM')) %dcm header available
            %%% convert dcmheader to nifti header
            
            
            currDir = pwd;
            cd(DEFAULT_TMPDIR);
            
            %addpath(GLOBAL_PATH_SPM)
            headerFirst = volume.getHeader('DICOM',1);
            %headerTmp = spm_dicom_headers_medphys(headerFirst.Filename);
            %rmpath(GLOBAL_PATH_SPM)
            
            
            
            % copy one single dicom file to temporary folder
            % this step is very important because if a lot of
            % dicom files are in one folder but do not belong
            % together they may be mixed resulting in wrong
            % meta information
            tempFolderDicom = tempname;
            mkdir(tempFolderDicom)
            tempFileDicom = [tempFolderDicom(numel(tempdir):end) '.dcm'];
            shellCommand = ['cp ' makeunixstring(headerFirst.Filename) ' ' fullfile(tempFolderDicom,tempFileDicom)];
            %shellCommand = ['cp ' makeunixstring(headerTmp{1}.Filename) ' ' fullfile(tempFolderDicom,tempFileDicom)];
            statusCodeCp = exec_system_command(shellCommand);
            if statusCodeCp
                error('Copy of DICOM file for header conversion failed.')
            end
            
            
            
            
            
            %%% header conversion
            volume.img = flipdim(volume.img, 1); % required for dcm2nii AND mri_convert
            
            
            %%% try mri_convert
            dispstatus('Note that if data is loaded with load_nii_raw and needs to be in original matrix orientation, you need to apply flips along x and y axes and mind coordinate system type!',[],'warning');
            
            volume.img = flipdim(volume.img, 2); % seems to be required for mri_convert (additional to flip 1)
            shellcommandSiemens = [fullfile(GLOBAL_PATH_FREESURFER,'bin/mri_convert') ' -it siemens_dicom -ot nii --force_ras_good ' fullfile(tempFolderDicom,tempFileDicom) ' ' fullfile(tempFolderDicom,[tempFileDicom(1:end-3), 'nii.gz'])];
            shellcommandOther   = [fullfile(GLOBAL_PATH_FREESURFER,'bin/mri_convert') ' -it dicom -ot nii --force_ras_good ' fullfile(tempFolderDicom,tempFileDicom) ' ' fullfile(tempFolderDicom,[tempFileDicom(1:end-3), 'nii.gz'])];
            
            opts.noErrorOnShellErrorCode = true; % the error of a failing shell command is switched off for mri_convert. This is required to enable the fallback modes implemented in write_nii
            if strcmpi(volume.getHeader('DICOM',1).Manufacturer, 'siemens')
                statusCode = runfreesurfercommand(shellcommandSiemens, [], opts);
            else
                statusCode = runfreesurfercommand(shellcommandOther, [], opts);          
            end
            
            if statusCode && strcmpi(volume.getHeader('DICOM',1).Manufacturer, 'siemens')
                dispstatus('Mri_convert for SIEMENS-specific tags failed. Now trying general DICOM format mri_convert...',[],'warning');
                statusCode = runfreesurfercommand(shellcommandOther, [], opts);
            end
            
            
            if statusCode % mri_convert failed
                %%% try dcm2nii
                %%% dcm2nii is not the first choice for header conversion
                %%% because we observed some orientation errors when data
                %%% was acquired in sagittal slice orientation.
                
                dispstatus('MRI_convert failed. Now trying fallback solution (mri_convert)... Note that this file has to be flipped along x-axis when it is loaded with load_nii_raw and the data matrix orientation must be in same data matrix orientation as input matrix (irrespective of the header) (mind coordinate system type). The header is, of course in consistence with the obained data matrix orientation.');
                statusCode = exec_system_command([GLOBAL_FILE_DCM2NII ' -f y -d n -p n -e n -i n -r n -n y -x n ' fullfile(tempFolderDicom,tempFileDicom)]);
                
            end
            
            
            if statusCode
                error('Conversion of DICOM header to NIFTI header using mri_convert and dcm2nii not successful.')
            end
            
            
            cd(currDir);
            tmpNifti = load_nii_raw(fullfile(tempFolderDicom,[tempFileDicom(1:end-3) 'nii']));
            
            mydelete(fullfile(tempFolderDicom,tempFileDicom))
            mydelete(fullfile(tempFolderDicom,[tempFileDicom(1:end-3) 'nii.gz']))
            
            volume.setHeader(tmpNifti.getHeader('NIFTI'));
            voxelSize = volume.getvoxelsize;
            header = volume.getHeader('NIFTI');

            
            %%% calculate qform
            qform = [header.hist.srow_x header.hist.srow_y header.hist.srow_z 0 0 0 1];
            reshapedqform = reshape(qform', [16,1]);
            qformstring = '';
            for i = 1:16
                qformstring = cat(2, qformstring, num2str(reshapedqform(i)), ' ');
            end
            
            isUseQform = true;
            rmdir(fullfile(tempFolderDicom));
            %   THIS WAS THE OLD FALLBACK SOLUTION: (seems to be obsolete
            %   now)
            %                     catch
            %                         warning('write_nii:headerConversion',['This nifti file must be loaded with spm_dicom_convert import routine specification if data array orientation in MATLAB shall be maintained! ' ...
            %                             'I tried to convert the header using mri_convert but if failed. This may occur when single-channel data has been loaded...']);
            %                         % at this point usually tmpNiftiFileName(1) does
            %                         % not exist
            %
            %
            %                         volume.img = flipdim(volume.img, 1);
            %                         voxelSize = volume.getvoxelsize;
            %                     end
            
            %                 end
            %             end
            %
            
            
            
            
            
            
        elseif ~isempty(volume.getHeader('BRUKER')) %BRUKER header available
            %volume.switchCoordinateSystemType('BRUKER');
            isBruker = true;
            qform = zeros(4,4);
            voxelSize = volume.getvoxelsize;
            Rotation = rotationfrombrukerheader(volume.getHeader('BRUKER'));
            Rotation.matrix(:,1) = Rotation.matrix(:,1) * voxelSize(1);
            Rotation.matrix(:,2) = Rotation.matrix(:,2) * voxelSize(2);
            Rotation.matrix(:,3) = Rotation.matrix(:,3) * voxelSize(3);
            Rotation.matrix(1,:) = Rotation.matrix(1,:) * (-1);
            Rotation.matrix(2,:) = Rotation.matrix(2,:) * (-1); % this is required because we flipped coordinate system to NIFTI here, which involves x-y flip. 
            
            volume.img = flipdim(volume.img,1);
            volume.img = flipdim(volume.img,2);
            
            if isfield(Rotation,'translationVector')
                qform(1:3,:) = [Rotation.matrix, Rotation.translationVector'];
            else
                % backward compatibility; in case translation vector not
                % available
                qform(1:3,:) = [Rotation.matrix, [0, 0, 0]']; %volume.getHeader('BRUKER').method.PVM_SPackArrReadOffset, volume.getHeader('BRUKER').method.PVM_SPackArrPhase1Offset, volume.getHeader('BRUKER').method.PVM_SPackArrSliceOffset]'];
                warning('Conversion of Bruker header to NIFTI translation information not supported. NIFTI translation information will be wrong!')
                % Bruker header seems to state the slice center, while NIFTI and DICOM want the corner edge voxel...    
            end
            
            %qform(1:3,:) = [squeeze(volume.getHeader('BRUKER').method.PVM_SPackArrGradOrient), [volume.getHeader('BRUKER').method.PVM_SPackArrReadOffset, volume.getHeader('BRUKER').method.PVM_SPackArrPhase1Offset, volume.getHeader('BRUKER').method.PVM_SPackArrSliceOffset]'];
            qform(4,4) = 1;
            isUseQform = true;
            reshapedqform = reshape(qform', [16,1]);
            qformstring = '';
            for i = 1:16
                qformstring = cat(2, qformstring, num2str(reshapedqform(i)), ' ');
            end
            
            %volume.img = flipdim(flipdim(flipdim(volume.img,2),3),1); % header information is in DICOM format (see mids switchCoordinate...)
             %volume.img = flipdim(permute(volume.img,[1 3 2 4 5 6 7]),2);
             
             
%              volume.img = flipdim(permute(volume.img,[2 1 3 4 5 6 7]),3);
%              voxelSizeTmp = volume.getvoxelsize;
%              voxelSize = voxelSizeTmp;
%              voxelSize(1) = voxelSizeTmp(2);
%              voxelSize(2) = voxelSizeTmp(1);
             
             
             %voxelSize(3) = voxelSizeTmp(3);
        elseif ~isempty(volume.getHeader('RAW')) %Toshiba raw file header available
            qform = zeros(4,4);
            voxelSize = volume.getvoxelsize;
            Rotation = volume.getOrientation;
            Rotation.matrix(:,1) = Rotation.matrix(:,1) * voxelSize(1);
            Rotation.matrix(:,2) = Rotation.matrix(:,2) * voxelSize(2);
            Rotation.matrix(:,3) = Rotation.matrix(:,3) * voxelSize(3);
            %Rotation.matrix(1,:) = Rotation.matrix(1,:) * (-1);
            %Rotation.matrix(2,:) = Rotation.matrix(2,:) * (-1); % this is required because we flipped coordinate system to NIFTI here, which involves x-y flip. 
            
            %volume.img = flipdim(volume.img,1);
            %volume.img = flipdim(volume.img,2);
            
            qform(1:3,:) = [Rotation.matrix, Rotation.translationVector']; 
            
            qform(4,4) = 1;
            isUseQform = true;
            reshapedqform = reshape(qform', [16,1]);
            qformstring = '';
            for i = 1:16
                qformstring = cat(2, qformstring, num2str(reshapedqform(i)), ' ');
            end
            
        else
            error('Function currently only supports hdr and hdrDcm, Bruker and Toshiba raw data header information.')
        end
        
%         
%             rotationMatrix = squeeze(volume.getHeader('BRUKER').method.PVM_SPackArrGradOrient);
%             if round(det(rotationMatrix)*1000000)/1000000 ~= 1
%                 error('Calculated rotation matrix is improper. Not implemented yet. Please see qform doc'); 
%             end
%             aSum = 1 + rotationMatrix(1,1) + rotationMatrix(2,2) + rotationMatrix(3,3);
%             if aSum > 0.5
%                 a = 0.5  * sqrt(aSum);
%                 b = 0.25 * (rotationMatrix(3,2) - rotationMatrix(2,3)) / a; % quatern_b
%                 c = 0.25 * (rotationMatrix(1,3) - rotationMatrix(3,1)) / a; % quatern_c
%                 d = 0.25 * (rotationMatrix(2,1) - rotationMatrix(1,2)) / a; % quatern_d
%             else
%                 error('This is a 180 degree rotation. An alternative formula is required, which is not implemented yet.') % see nifti1_io.c function mat44_to_quatern() for an implementation
%             end
%             if a*a+b*b+c*c+d*d ~= 1
%                 error('Something went wrong.')
%             end
%             
%             % Reference: nifti.nimh.nih.gov/nifti-1...
%             
        
    end
    
    
    % (undo) fliping of data according to the applied imput routine
    if isfield(volume,'importRoutine') || isproperty(volume,'importRoutine')
        flipimporteddata(volume,volume.importRoutine);
    end
   
    
    volume = volume.img;
    
    
else % input is plain matrix data
    
    if nargin < 3 || isempty(header)
        if isobject(volume)
            voxelSize = volume.getvoxelsize;
            volume = volume.img;
        else
            voxelSize = [1 1 1];
        end
        
    else
        if isfield(header, 'PixelSpacing') % header supplied
            % compute qform
            qform = computeniftiqfromfromTOFSWIdcmdata(header);
            reshapedqform = reshape(qform', [16,1]);
            qformstring = '';
            for i = 1:16
                qformstring = cat(2, qformstring, num2str(reshapedqform(i)), ' ');
            end
            isUseQform = true;
            voxelSize = voxelaspectratio(header);
        else % voxel aspect ratio directly supplied
            voxelSize = header;
        end
    end
end




if nargin < 4 || isempty(dataType),  dataType = false;                       end
if nargin < 5 || isempty(saveType), saveType = 'nii';       				end
if nargin < 6 || isempty(preview),  preview = false;         				end

if preview && ndims(volume) == 3,   write_png(volume, name, 'z');           end

% if ndims(volume) == 2
%     volume = repmat(volume,[1 1 2]);
%     voxelSize = [voxelSize mean(voxelSize)];
%     warning('This is a 2D array. Converting to 3D because NIFTI does not support 2D.')
% end


[~, ~, ext] = fileparts(name);
if strcmp(ext, '.gz')
    name = name(1:end-3);
end

if numel(name) < 5 || ~strcmp(name(numel(name)-3), '.')
    if strcmp(saveType, 'hdr')
        saveType = 'img';
    end
    name = [name, '.', saveType];
end

[code, msg] = exec_system_command(strcat('touch "', name,'"'));
if code ~= 0
    error('SWI_Toolbox:write_nii:CantWrite', ...
        'Missing privileges: %s', msg);
end

addpath(GLOBAL_PATH_NIFTITOOL_SHEN);
if dataType
    if strcmp(dataType, 'orig')
        save_nii(make_nii(volume), name);
    else
        save_nii(make_nii(volume,voxelSize, [], dataType), name);
    end
else
    if islogical(volume)
        save_nii(make_nii(uint8(volume),voxelSize, [], 2), name);
    elseif isreal(volume)
        save_nii(make_nii(volume,voxelSize, [], 16), name);
    else
        save_nii(make_nii(volume,voxelSize, [], 32), name);
    end
end

if ~noGzip
    mydelete([name '.gz']);
    if exist([name '.gz'], 'file')
        error([name '.gz already exists and I have no rights to overwrite or delete it...'])
    end
end

if isUseQform
    shellStatusCode0 = runfreesurfercommand(cat(2, 'fslorient -setsform ', qformstring , ' ', name));
    shellStatusCode1 = runfreesurfercommand(cat(2, 'fslorient -copysform2qform ', name));
%     if isBruker
%         shellStatusCode0 = runfreesurfercommand(cat(2, 'fslorient -forceradiological ', name));
%     end
    if shellStatusCode0 || shellStatusCode1
        error('Setting of NIFTI header failed.')
    end
    %runfreesurfercommand(cat(2, 'fslorient -forceradiological ', name));
end


if ~noGzip
    dispstatus('Zipping nifti file...')
    gzip(name)
    mydelete(name);
end



    function mydelete(filename)
        if exist(filename, 'file')
            delete(filename);
        end
    end

dispstatus(mfilename(),[],'functionexit')


end
