function [dataArray, IterationDetails, residualArray, problemMaskOne, gradientMaskReturn, anisotropyComponent,dataArrayPreConeProc,ConeMask,GradientMaskResidual_x,phaseDiscrepancy] = computesusceptibility_lsqr(dataArray, Options, problemMask,ConstraintMask,spatialWeightingMatrix,anisotropyAngleArray,anisotropyMask)

%COMPUTESUSCEPTIBILITY Inverts the magnetic field to its sources.
%
%   Computes source distribution from relative residual field.
%
%
%   Syntax
%
%   x = COMPUTESUSCEPTIBILITY(A)
%   x = COMPUTESUSCEPTIBILITY(A,Options)
%   x = COMPUTESUSCEPTIBILITY(A,Options,M)
%   x = COMPUTESUSCEPTIBILITY(A,Options,M,C)
%   x = COMPUTESUSCEPTIBILITY(A,Options,M,C,D)
%   x = COMPUTESUSCEPTIBILITY(A,Options,M,C,D,E,F)
%   [x, Details] = COMPUTESUSCEPTIBILITY(...)
%   [x, Details, y] = COMPUTESUSCEPTIBILITY(...)
%   [x, Details, y, N] = COMPUTESUSCEPTIBILITY(...)
%   [x, Details, y, N, a] = COMPUTESUSCEPTIBILITY(...)
%
%
%
%   Description
%
%   COMPUTESUSCEPTIBILITY(A) computes a susceptibility distribution
%   based on relative difference field A. A must be a real-valued NxMxPxQ
%   data array with Q relative-difference-fields (rdf). If Q > 1
%   specification of corresponding rotational axes and angles
%   (Options-struct)
%   is required.
%
%   COMPUTESUSCEPTIBILITY(A,Options) uses the options that are defined in
%   the struct Options für the algorithm.
%
%   COMPUTESUSCEPTIBILITY(A,Options,M) uses only voxels of A that
%   are defined by the NxMxP logical array M.
%
%   COMPUTESUSCEPTIBILITY(A,Options,M,C) puts constraints regions that
%   are defined by the struct C. C may contain fields 'susceptibility' and
%   'residual'. The fields must be NxMxPxQ logical matrices that define the
%   Q regions to be constrained. Constrained regions will have constant
%   susceptibility and rotation-invariant shift, respectively.
%
%   COMPUTESUSCEPTIBILITY(A,Options,M,C,D) uses the real valued NxMxP spatial
%   weighting matrix D.
%
%   COMPUTESUSCEPTIBILITY(A,Options,M,C,D,E,F) uses anisotropy information for
%   inversion. The real valued NxMxPxQ matrix E contains for each voxel and
%   each orientation the angle of the anisotropy with respect to the
%   magnetic field. The logical mask F specifies the voxels to be used for
%   calculation of the anisotropy component (e.g., FA > 0.6).
%
%   [x, Details] = COMPUTESUSCEPTIBILITY(...) also returns a
%   struct with details about the iteration.
%
%   [x, Details, y] = COMPUTESUSCEPTIBILITY(...) returns the
%   rotation-invariant shift vector y in case residual weighting was activated (see Options).
%
%   [x, Details, y, N] = COMPUTESUSCEPTIBILITY(...) returns also the
%   mask N where susceptibility may be regarded as reliable.
%
%   [x, Details, y, N, G] = COMPUTESUSCEPTIBILITY(...) returns also the
%   gradient masks (cell).
%
%   [x, Details, y, N, G, a] = COMPUTESUSCEPTIBILITY(...) returns also the
%   anisotropy vector a in case anisotropy information was supplied.
%
%   [x, Details, y, N, G, a, P] = COMPUTESUSCEPTIBILITY(...) returns also the
%   computed susceptibility before correction of the cone in k-space.
%
%   [x, Details, y, N, G, a] = COMPUTESUSCEPTIBILITY(...) returns also the
%   mask which defines the region in k-space which is corrected in case of
%   Post-processing of the cone.
%
%
%   The following Option-fields of are supported:
%
%
%       solvingType   - String which specifies type of solver used for calculation of
%                       Susc-Map. The following types are currently
%                       supported:
%                           ''  (default)
%                           Uses LSQR-solver which solves the equation system in the
%                           spatial domain in a least-squares-sense
%
%                           'InverseFiltering'
%                           Solves the problem directly in Fourier-space
%                           and sets voxels in dipole-cone to
%                           zero. See Options-field
%                           DipoleFilter.parameter for details.
%                           his parameter is set to 0.1 by default.
%
%                           'SpatialDomainTV'
%                           the following field are optional for Options.solvingType='SpatialDomainTV':
%                               tol - tolerance for the stopping criteria (default: 1e-5)
%                               muMin     - Minimum value of mu (deviation from perfect
%                                            l1 norm) at the last continuation step.
%                                           A smaller mu leads to higher accuracy. (default: 0.1)
%                               maxitMu   - number of continuation steps (default: 2)
%                               maxitInner - max number of iterations in an inner loop (default: 10)
%                               lambda - regularization parameter; the higher the value, the smoother the solution.
%                                        (default: 0.5; values between 0.1 and 1 make quite a difference)
%                               ResidualGradientWeighting.parameter - determines relative weight of Gradient Weighting of Residual Vector
%                                                                     compared to Gradient Weighting of Susceptibility (default: 1).
%                                                                     Options.residualWeighting must be true.
%
%
%   The following fields are optional:
%
%                           tolerance
%                               convergence tolerance
%                           maxit
%                               maximum number of iterations
%                           offsetUseBool
%                               Boolean for automatic offset determination
%                               Default: true
%                           residualWeighting
%                               factor for rotation-invariant shift
%                           ConvolutionFilterCompensation
%                               Struct with parameters for compensation of
%                               convolution type filters (see documentation of
%                               GENERATEUNITDIPOLERESPONSE).
%                           DipoleFilter
%                               Filter-struct for dipole regularization.
%                               See GENERATEUNITDIPOLERESPONSE for further
%                               details.
%                           boolPreconditioner
%                               Boolean for preconditioning
%                           preconditionerParameter
%                               parameter for preconditioning
%                           PreconditionerFilter
%                               Filter-struct for regularization of
%                               preconditioning matrix
%                           chemicalShiftFactor
%                               factor for proportionality inversion
%                           multiAngleMethod
%                               method for solution using multiple angle
%                               aquisition data. Available methods are
%                               'difference' (default) and 'simultaneous'.
%
%                           PowerRegularization
%                               standard tikhonov regularization: norm of
%                               susceptibility is minimized. The following
%                               fields are supported:
%                                   'parameter' - specifies the regularization parameter
%                           TikhonovRegularizationSusceptibility
%                               This struct specifies parameters for solving with
%                               Tikhonov regularization of the susceptibility.
%                               The following fields are supported:
%                                   'type'
%                                       Specifies the Tikhonov regularization
%                                       type. Currently, the follwing types are supported:
%                                          'laplacian' - Tikhonov-matrix is a
%                                            3-D Laplacian matrix
%                                          'weightedGradient' - Gradient of
%                                            susceptiblity is weighted with
%                                            the inverse of the gradient of the
%                                            input matrix (see below))
%                                          'weightedGradientDirect' - Gradient of
%                                            susceptiblity is weighted with
%                                            the spatialMatrix (see below) directly
%                                          'partial gradient weighting'
%                                            only voxels where the gradient is
%                                            smaller than Options.GradientMask.gradientThreshMin
%                                            are used to generate a weighting mask)
%                                   'parameter'
%                                       Specifies the Tikhonov-parameter (scalar).
%                                   'spatialMatrix'
%                                       3D-data array that is used for weighting,
%                                       e.g. magnitude or phase data
%                                       (only required for 'weightedGradient',
%                                       'partial gradient weighting', or 'weightedGradientDirect').
%
%                           useConstantConstraints  - Defines whether constrained
%                               susceptibility regions are set to constant values.
%
%
%
%
%       PostProcCone - Struct which defines the post-processing type of
%                      the dipole cone structure. The following fields are
%                      required:
%
%                           type - string containing the type of
%                                  post-processing. Currently only 'tv' is
%                                  supported (Total Variation Minimization).
%
%                      The following fields are optional:
%
%                           threshold - scalar defining the maximum singular value of the dipole
%                                       structure to be replaced = NF threshold (default: 0.14)
%                           applyNCFCorrection - boolean which defines if NCF correction is applied
%                                                (Default: false)
%                           outerConeThreshold - defines the size of the outer Cone (= NCF region),
%                                                where a denoising is applied (Default: 0.3).
%                           NCFFilterOptions - defines options for denoising that is applied in the NCF region
%                                              (see diffusionimagefilter.m)
%                           kSpaceUndersamplingMatrixPhase - This binary matrix
%                                  specifies undersampling of phase k-space
%                                  during data acquisition, e.g. due to
%                                  using partial fourier (PF). Using this
%                                  matrix for sparse recovery reduces the
%                                  number of unknowns and, thus, improved
%                                  quality.
%                           kSpaceUndersamplingMatrixPhaseRelaxed - This binary matrix
%                                  specifies undersampling of k-space
%                                  during data acquisition, e.g. due to
%                                  using partial fourier (PF). Using this
%                                  matrix for sparse recovery reduces the
%                                  number of unknowns and, thus, improved
%                                  quality.
%                           tol - tolerance for the stopping criteria L1-norm (default: 1e-5)
%                           tolEnergy - tolerance for the stopping criteria energy (default: 1e-5)
%                           stopEenergy - specification of the stopping
%                                         energy. If this value is 0,
%                                         energy is determined
%                                         automatically. (default: 0)
%                           muMin     - Minimum value of mu (deviation from perfect
%                                       l1 norm) at the last continuation step.
%                                       A smaller mu leads to higher accuracy. (default: 0.1)
%                           maxitMu   - number of continuation steps (default: 5)
%                           maxMaxitMu - value to which maxitMu may be increased in case stop criterion was not fulfilled
%                           maxitInner - max number of iterations in an inner loop (default: 20)
%                           coneType - defines the shape of the cone. Currently only 'Default' is supported.
%
%
%       useHEIDI - boolean which defines if HEIDI is applied (default: true)
%                  if true, the following Options are chosen (if not defined manually):
%                  PostProcCone.type = 'tv';
%                  PostProcCone.applyNCFCorrection = false;
%                  GradientMask.applyLaplacianCorrection = true;
%                  GradientMask.applyDenoising = true;
%                  DipoleFilter.type = truncSingularValues
%                  DipoleFilter.parameter = DEFAULT
%                  residualWeighting = 0 (only if not set manually)
%
%       useHEDI - boolean which defines if HEDI is applied (default: false)
%
%
%
%       useMEDI - boolean which defines if MEDI is applied (default: false)
%                 if true, the following Options are chosen
%                 tolerance = 1e-5;
%                 isFourierDomainFormula = true;
%                 PostProcCone = [];
%                 residualWeighting = [];
%                 DipoleFilter = [];
%                 solvingType = 'SpatialDomainTV';
%                 offsetUseBool = true;
%                 gradientType = 2; % set to two although Tian Liu, 2012, IEEE says cental difference (but this introduces some checkerboard patterns
%
%
%
%
%       Rotation     - struct with position information of supplied rdf-data.
%                      This struct may consist of
%                      * 'axis' and 'angle' to define rotation axis and angle in degrees.
%                      * or 'matrix' to define the rotation matrix (3x3xnumberOfRDF)
%
%       voxelAspectRatio - Aspect ratio of voxels (default = [1 1 1]).
%                          Aspect ratio corresponds to voxel size.
%
%       problemMaskClosed - Specification of the region of internal sources.
%                      This mask is required for the HEIDI mode.
%
%       isProblemMaskClosedFlat - If this boolean is true, gradient-based reconstruction
%                      will be improved. The problemMaskClosed is used  for
%                      constraining voxels with unkown field values to be
%                      spatially flat (avoids spikes in the data).
%
%       GradientMask - generates three weighting masks by applying thresholds on the gradient of an input map.
%                      The following fields are supported:
%                           suppliedMask - in case gradient masks were generated externally,
%                                          they can be supplied in this field
%                           spatialMatrix - specifies the input data (default: input RDF)
%                           gradientThreshMin - threshold for gradient (default see source code)
%                           applyLaplacianCorrection - boolean which defines if Laplacian correction is applied
%                                                      (Default: false)
%                           laplacianThreshMin - threshold for laplacian correction (default: 0.009).
%                                                only makes sense if spatialMatrix is phase data.
%                           MagnitudeForCorrection - specifies magnitude data for correction of gradient mask.
%                           magnitudeThresholdMin - threshold for magnitude data correction (default: 3)
%                           applyDenoising - boolean which defines if denoising is applied to spatialMatrices
%                                            before gradient mask creation (Default: true)
%                           applyDenoisingAfter - boolean which defines if denoising is applied to spatialMatrices
%                                            after gradient mask creation (Default: false)
%                           filterOptions  - struct that defines options for denoising (see diffusionimagefilter.m)
%                                            note: if this struct exists, applyDenoising is automatically set to 'true'.
%
%
%       echoTime     - echo time of supplied data, used for normalization
%                      of phase data if GradientMask is used
%
%       magneticFieldStrength - field strength of supplied data, used for normalization
%                      of phase data if GradientMask is used
%
%       GradientMaskResidual - generates three weighting masks for the Residual.
%                       The following fields are supported:
%                           spatialMatrix - specifies the input data (default: Options.GradientMask.spatialMatrix). if this is not supplied,
%                                           Options.GradientMask must be true.
%                           gradientThreshMin - threshold for gradient (default see source code)
%
%
%
%
%   Notes
%
%   Apply zeropadding and makeodd to rdf before calling this function!





%   F Schweser, ferdinand.schweser@med.uni-jena.de, 2009/07/30
%
%   Changelog:]
%  v12.3 - 2018/10/08 - F Schweser - Bugfix for no zeropadding
%  v12.2 - 2018/10/02 - F Schweser - Bugfix scaling of multiecho maps
%  v12.1 - 2018/09/24 - F Schweser - Bugfixes regarding multiecho data and partial Fourier
%  v12  - 2018/02/27	 - F Schweser - Major improvement of default HEIDI parameters
%  v11.5   - 2016/07/27	 - F Schweser - Major bugfix regarding masks in isNCF
%  v11.4   - 2015/08/29	 - F Schweser - Now allows non-zero residual
%                                       weighting in HEIDI mode
%  v11.4   - 2015/08/27	 - F Schweser - Bugfixes regarding residual regularization
%  v11.3   - 2015/07/15	 - F Schweser - Changed MEDI default threshold parameter to value reflecting changes in MEDI function
%  v11.2   - 2015/06/24	 - F Schweser - Bugfixes regarding MEDI. Changed
%                                       default reg parameter to 0.1 and ermoved magnitude weighting matrix
%  v11.1   - 2014/03/31	 - F Schweser - Check rotation matrix
%  v11.0   - 2012/12/12	 - F Schweser - Default parameters changed!!!!!
%			 		maxit from 200 to 400
%				        dipole formula from spatial to FT
%					in HEIDI now no  dipole filter (was thresholded)
%					These changes were made by comparing with old GRAZ results that were better
%  v10.91   - 2012/12/04 - F Schweser - Removed some old stuff
%  v10.9   - 2012/11/15 - F Schweser - Options.problemMaskClosed now closed
%  v10.8   - 2012/11/16 . F Schweser - Major bugfix regarding all types of
%                                      QSM with offset correction. In the
%                                      transpose matrix the
%                                      offsetcalculation was incorrect
%                                      (mask not accounted for).
%  v10.7   - 2012/11/02 - F Schweser - kSpaceUndersamplingMatrixPhaseRelaxed
%                                      now uncommented again
%				       + DEFAULT_POSTPROC_THRESHOLD changed from 0.09 to 0.14
%				       + DEFAULT_POSTPROC_OUTERCONETHRESHOLD changed from 0.23 to 0.3
%				       These settings produced substantially better results in the GRAZ in situ data (comparable to the results used in the paper)
%  v10.6   - 2012/10/27 - F Schweser - Major bugfix regarding HEIDI. The
%                                      gradient mask was converted to
%                                      logical, although it must contain
%                                      values of 0.1
%  v10.5   - 2012/09/26 - F Schweser - Memory performance opt
%  v10.41   - 2012/07/19 - F Schweser - Bugfix: nans in rdf
%  v10.4   - 2012/06/29 - F Schweser - New mode 'useMEDI'
%  v10.3   - 2012/04/18 - F Schweser - removed log entried <8.0
%                                    + changed DEFAULT values according to
%                                      HEIDI paper:
%                                       DEFAULT_POSTPROC_THRESHOLD from 0.12 to 0.09
%                                       DEFAULT_POSTPROC_OUTERCONETHRESHOLD from 0.3 to 0.23
%                                       DEFAULT_TV_MU from 0.1 to 0.001 (reduced speckles)
%                                       DEFAULT_TV_MAXITER from 300 to 1000
%                                       DEFAULT_TV_MAXINTITER from 4 to 8
%                                       DEFAULT_CONE_STOPTEST from 3 to 1
%                                       DEFAULT_TV_TOLVAR from 1e-5 to 1e-3
%                                       DEFAULT_GRADIENTMASK_THRESHOLD from 0.00106666 to 0.00105
%                                       DEFAULT_GRADIENTMASK_LAPLACIANTRHESHOLD from 0.002 to 0.0159
%                                       DEFAULT_GRADIENTMASK_MAGNITUDETHRESHOLD from 3 to 2.4
%                                       DEFAULT_ISREPLACEWELLPOSEDSUBDOMAIN from true to false
%                                    + changed HEIDI-specific parameters:
%                                       maxit from 500 to 400
%                                       tolerance from 1e-15 to 1e-5 (see COMPUTESUSCWRAPPER for details)
%                                    + some improvements regarding the NCF
%                                      reconstruction
%                                    + some minor code cleanup
%                                    + Bugfix: no undersamplingPhaseRelaxed
%                                    matrix set
%  v10.2   - 2012/04/05 - F Schweser - gradientMaskReturn default value
%  v10.1   - <2012/03/27 - F Schweser - Now returns all gradient masks as cell
%  				      + Denoising of derivative images implemented BEFORE
%					and AFTER the calculation of the derivatives. By default only
%					before.
%				      + Rplacement of zero-values in the gradient masks by fixed value
%					reduced noise in unconstrained regions (default: 0.1)
%				      + Gradient type now piped (default: 2; no checkerboard effect)
%				      + Bugfix: COSMOS mode required problemMaskClosed
%				      + residualWeighting is not 0 in HEIDI mode by default
%				      + Default values for epsilon
%			              + Some code refactoring to reduce redundancy
%				      + Several minor bugfixes that resulted in improved quality during
%					manuscript drafting
%  v10   - 2012/01/10 - K Sommer   - call of estimatetotalenergy was wrong (PfMatrix)
%                        F Schweser - Bugfix: empty Options.kSpaceUndersamplingMatrixPhaseRelaxed
%                                     failed
%                                     + PostProcCone.Correction factor
%                                     default value set to 1.
%                                     + now uses only energy inside of cone
%                                     + new option tolEnergy
%                                     + new option stopEnergy
%                                     + isProblemMaskClosedFlat introduced
%                                     and behaviour of problemMaskClosed
%                                     changed
%                                     + now uses dispstatus
%                                     + Bugfix: magnitude gradient was
%                                     denoised instead of the magnitude
%                                     before calculating the gradient
%                                     + DEFAULT_GRADIENTMASK_APPLYDENOISING
%                                     changed to true
%                                     +
%                                     DEFAULT_POSTPROC_NCFFILTEROPTIONS_NOITERATIONS
%                                     changed to 8 (from 5)
%                                     + normalization of phase wrt
%                                     magneticFieldStrength (new options
%                                     field) + default parameter changed
%                                     correspondingly
%                                     + Bugfix: Filteroptions not always
%                                     set by default
%                                     + default heidi value: residualWeighting = 0
%  v9.51   - 2012/01/23 - K Sommer   - default value for Laplacian threshold updated (0.017)
%  v9.5    - 2012/01/20 - F Schweser - Critical bugfix: Laplacian was divided with its maximum (normalized).
%				       This was inconsistent with paper an produced unforseeable effects.
%				       I have set threshold to 1/eps (large--> no Laplacian reg)
%			               + Laplacian now normalized by echo time.
%				       @KS: Please repeat simulation and change default value.
%  v9.4    - 2012/01/19 - F Schweser - Magnitude gradient threshold
%                                      normalization now with respect to
%                                      average gradient of all three
%                                      directions.
%                                      + code readability improved
%  v9.34    - 2012/01/03 - F Schweser - Memory efficiency improved
%  v9.3     - 2011/12/22 - F Schweser - Now uses the 2-voxel gradient
%                                       instead of the 3-voxel gradient and
%                                       uses the automatic transpose
%                                       gradient of DISCRETEGRADIENT.
%                                       Moiree may now be eliminated.
%  v9.2     - 2011/12/16 - F Schweser - Bugfix: Now HEIDI uses
%                                       threshSingularValues automatically
%   v9.1  2011/12/14 - F Schweser   - Changed transitional-denoising
%                                     parameters to less denoising because
%                                     rio-data lost quality after
%                                     transitional domain replacement
%   v9.0  2011/11/30 - F Schweser   - New mode 'weightedGradientDirect' in
%                                     the hope that this move removes speckles
%                                       + energy correction factor default
%                                       value changed to 1/1.1 because this
%                                       resulted in substantially improved
%                                       solution with fixed post mortem
%                                       brains + uses pfMatrix now (avoids speckles)
%   v8.9  2011/11/21 - F Schweser   - Default NCF and NF parameters changed
%                                     to optimal values in NeuroImage paper
%   v8.8  2011/11/05 - F Schweser   - Performance optimization: Energy estimation improved by outsourcing to new function ESTIMATETOTALIMAGEENERGY
%                                        + several memory optimizations (only startet with this issue... :-/)
%                                   - Bugfix: Voxel aspect ratio now piped to SPARSERECOVERY
%                                   - Bugfix: denoising was only performed
%                                             when NCF was activated
%                                   - Critical Bugfix!! Laplacian correction
%                                     was in fact calculated from the
%                                     Fourier image not the spatial domain
%                                     image!!!! (extremely poor a priori
%                                     information was the result)
%   v8.7  2011/10/27 - F Schweser   - kSpaceUndersamplingMatrix renamed to kSpaceUndersamplingMatrixPhase
%   v8.63 2011/10/25 - F Schweser   - Bugfix: isNCF was not set by default
%   v8.62 2011/10/24 - K Sommer     - minor header update
%   v8.61 2011/10/24 - K Sommer     - applyMagnitudeCorrection field removed (not necessary)
%   v8.6  2011/10/21 - K Sommer     - Options.GradientMask: fields applyLaplacianCorrection, applyMagnitudeCorrection,
%                                                           applyDenoising, filterOptions added
%                                     Options.PostProcCone: fields applyNCFCorrection, outerConeThreshold
%                                                           and NCFFilterOptions added
%                                     header & input options check restructured.
%   v8.53 2011/10/19 - K Sommer     - speed increase: ifftsave(....,'symmetric') -> real(ifftsave(..))
%   v8.52 2011/10/19 - K Sommer     - Default DipoleFilter parameter set to  0.1 again. Query if PostProcCone threshold
%                                     is smaller than this parameter included.
%   v8.51 2011/10/18 - K Sommer     - voxel size piped to diffusionimagefilter
%   v8.5 2011/10/18  - K Sommer     - HEIDI option added: Denoising of input data (phase & magnitude) before gradient mask creation.
%                                     dpMask now named ConeMask. inverseFiltering Option for PostProcCone removed.
%                                     improvedCone Option removed. Default parameter for DipoleFilter set to 0.05.
%   v8.43 2011/10/17 - K Sommer     - bugfix of v8.41: magnitude threshold for gradientmask not normalized
%   v8.42 2011/10/17 - F Schweser   - Bugfix: Now works with MAA mode
%   v8.41 2011/10/17 - K Sommer     - new Options from update to v8.4 removed -> was buggy
%   v8.4 2011/10/12 - K Sommer      - HEIDI algorithm implemented (still called PostProcCone, though).
%                                     New Options-fields: PostProcCone.NFConeThreshold, PostProcCone.NCFConeThreshold, PostProcCone.NCFFilterOptions,
%                                     GradientMask.FilterOptions -> phase and magnitude data is now automatically denoised before gradient mask creation.
%                                     Automatic increase of maxItMu (was defined by maxMaxitMu) removed (too time-consuming).
%                                     Some renaming of variables in PostProcCone-routine.
%   v8.3  2011/09/22 - F Schweser   - feature added: kSpaceUndersamplingMatrix
%   v8.2  2011/09/22 - F Schweser   - Performance improvement: now uses DISCRETEGRADIENT()
%   v8.12 2011/08/02 - K Sommer     - bugfix: operand | instead of || in laplacian correction
%   v8.11 2011/08/02 - K Sommer     - bugfix: now uses suscSpatialWeightingMatrix instead of spatialWeightingMatrix
%                                     (default matrix for spatialWeightingMatrix was not set)
%   v8.1 2011/07/13 - K Sommer      - default value set for Options.PostProcCone.correctionFactor (1)
%   v8.0 2011/07/19 - F Schweser    - Significantly imporved quality of
%                                      cone post-processing with new option: problemMaskClosed. Improves
%                                      gradient-based reconstruction.
%   v8.1 2013/03/04 - A Deistung    - Minor bugfix: gradientMaskReturn is now defined for output
%   v8.2 2013/03/08 - F Schweser    - Minor improvement: removed totalEnergy calculation
%   v8.3 2013/03/31 - F Schweser    - DEFAULT change: Now
%                                       applyNCFCorrection is false by
%                                       default. This was inconsistent
%                                       previosuly!
%   v8.4 2015/07/15 - F Schweser    - Changed default parameters for MEDI
%   v8.5 2017/08/22 - F Schweser    - Now returns phase discrepancy

dispstatus(' ')
residualArray = [];

%% constants

DEFAULT_MAXIT = 100;
DEFAULT_TOLERANCE = 1e-5;
DEFAULT_OFFSETUSEBOOL = true;
DEFAULT_USECONSTANTCONSTRAINTS = true;
DEFAULT_GRADIENTMASK_THRESHOLD = 0.00105;
DEFAULT_GRADIENTMASK_APPLYLAPLACIAN = true; %false
DEFAULT_GRADIENTMASK_LAPLACIANTRHESHOLD = 0.0159;
DEFAULT_GRADIENTMASK_MAGNITUDETHRESHOLD =  2.4;
DEFAULT_GRADIENTMASK_APPLYDENOISING = true;
DEFAULT_GRADIENTMASK_APPLYDENOISINGAFTER = false;
DEFAULT_GRADIENTMASK_FILTEROPTIONS_TIMESTEP = 0.05;
DEFAULT_GRADIENTMASK_FILTEROPTIONS_CONDUCTANCE = 1;
DEFAULT_GRADIENTMASK_FILTEROPTIONS_NOITERATIONS = 5;
DEFAULT_GRADIENTMASKRESIDUAL_THRESHOLD = 0.0032; % 0.04;
DEFAULT_GRADIENTMASKREPLACEVALUE = 0.1;
DEFAULT_ISREPLACEWELLPOSEDSUBDOMAIN = false;

DEFAULT_USEMEDI = false;
DEFAULT_USEHEDI = false;

DEFAULT_SOLVINGTYPE = 'SpatialDomainL2';
DEFAULT_DIPOLEFILTER_PARAMETER = 0.1;
DEFAULT_DIPOLEFILTER_TYPE = 'truncSingularValues';
DEFAULT_VOXELASPECTRATIO = [1 1 1];

DEFAULT_TV_MAXINTITER = 8;%5;
DEFAULT_TV_MAXITER = 500; %20;
DEFAULT_TV_MU = eps; %substantially reduces speckles compared to 0.1
DEFAULT_TV_TOLVAR = 1e-3;
% it looks like the tolerance mainly influences how well the low frequency
% components match. If chosen 1e-2 the resulting error pattern has
% substantial low frequencay components in it. The pattern looks quite well
% when chosen 1e-3 (with perfect mask). --> set to 1e-3



DEFAULT_TV_LAMBDA = 1e-3; % resulted in most appealing images (see test script /synology/projects/methods_development/QSM/reco/MEDI_parameterOptimization/test_various_parameters.m)
DEFAULT_TV_RESIDUALGRADIENTWEIGHTING_PARAMETER = 1;
DEFAULT_TV_STOPTEST = 4;

DEFAULT_POSTPROC_APPLYNCF = false;

DEFAULT_GRADIENTTYPE = 2;

%% Combine RDFs of multiple angle acquisitions
%(Compute difference of several RDFs)



gridDimensionVector = size(dataArray);
gridDimensionVector = gridDimensionVector(1:3);
nRdf = size(dataArray,4);




dispstatus(['Number of RDFs:                       ',num2str(nRdf)]);




if nRdf > 1
    if ~myisfield(Options,'multiAngleMethod')
        Options.multiAngleMethod = 'difference';
        dispstatus('Multi-Angle Solution method:          DEFAULT (difference)');
    else
        dispstatus(['Multi-Angle Solution method:          ',Options.multiAngleMethod]);
    end
    
    
    
    manyFieldsBool = true;
    
    
    
    if strcmp(Options.multiAngleMethod,'difference')
        
        dataArrayDiff=(nRdf-1)*dataArray(:,:,:,1);
        for iRdf=2:nRdf
            dataArrayDiff=dataArrayDiff-dataArray(:,:,:,iRdf);
        end
        dataArray=dataArrayDiff;
        clear dataArrayDiff
        
        nRdfUsed = 1;
    elseif strcmp(Options.multiAngleMethod,'simultaneous')
        nRdfUsed = nRdf;
        
    else
        error('Specified Multi-Angle-Method is not supported.')
        
    end
    
    
else
    manyFieldsBool = false;
    nRdfUsed = 1;
end



%% check input arguments



if nargin < 1 || isempty(dataArray)
    error('Function requires at least one input argument.')
end
if nargin < 2 || isempty(Options)
    dispstatus('Default parameters will be used.')
    Options.defaultParameters = true;
end
if nargin < 3 || isempty(problemMask)
    problemMask = true([gridDimensionVector nRdfUsed]);
end

% generate mask of voxels where at least one field value is available
problemMaskOne = false(gridDimensionVector);
for jRdf = 1:nRdf
    problemMaskOne = problemMaskOne | problemMask(:,:,:,jRdf);
end

if nargin < 4 || isempty(ConstraintMask)
    boolWithConstraintMaskSusceptibility = false;
    boolWithConstraintMaskResidual       = false;
    dispstatus( 'Susceptibility constraints:           DEFAULT (OFF)')
    dispstatus( 'Residual constraints:                 DEFAULT (OFF)')
elseif isstruct(ConstraintMask)
    if myisfield(ConstraintMask,'susceptibility') && ~isempty(ConstraintMask.susceptibility)
        boolWithConstraintMaskSusceptibility = true;
        dispstatus( 'Susceptibility constraints:           ON')
    else
        boolWithConstraintMaskSusceptibility = false;
        dispstatus( 'Susceptibility constraints:           DEFAULT (OFF)')
    end
    
    if myisfield(ConstraintMask,'residual') && ~isempty(ConstraintMask.residual)
        if myisfield(Options,'residualWeighting') && Options.residualWeighting
            boolWithConstraintMaskResidual = true;
            dispstatus( 'Residual constraints:                 ON')
        else
            boolWithConstraintMaskResidual = false;
            dispstatus( 'Residual constraints:                 DEFAULT (OFF)')
        end
    else
        boolWithConstraintMaskResidual = false;
        dispstatus( 'Residual constraints:                 DEFAULT (OFF)')
    end
else
    error('Constraint-matrix argument must be a Struct.')
end


if ~myisfield(Options,'problemMaskClosed') || isempty(Options.problemMaskClosed)
    if nRdf > 1
        Options.problemMaskClosed = problemMaskOne;
    else
        Options.problemMaskClosed = problemMask;
    end
    Options.problemMaskClosed = closemask(Options.problemMaskClosed,3,true); % arbitrary chosen value of 3
    dispstatus( 'Optional closed problem mask supplied:           NO')
elseif islogical(Options.problemMaskClosed)
    dispstatus( 'Optional closed problem mask supplied:           YES')
else
    error('Optional closed problem mask must be logical.')
end

if ~myisfield(Options,'isProblemMaskClosedFlat') || isempty(Options.isProblemMaskClosedFlat)
    Options.isProblemMaskClosedFlat = false;
    dispstatus( 'Force flat susceptibility in unknown-phase regions:           NO')
elseif islogical(Options.isProblemMaskClosedFlat) && Options.isProblemMaskClosedFlat
    dispstatus( 'Force flat susceptibility in unknown-phase regions:           YES')
else
    error('isProblemMaskClosedFlat must be logical.')
end




if nargin < 5 || isempty(spatialWeightingMatrix)
    boolUseSpatialWeightingMatrix = false;
    dispstatus( 'Spatial weighting matrix:             DEFAULT (OFF)')
else
    boolUseSpatialWeightingMatrix = true;
    dispstatus( 'Spatial weighting matrix:             ON')
end



if nargin < 6 || isempty(anisotropyAngleArray)
    anIso = false;
    dispstatus( 'Anisotropy:                           DEFAULT (OFF)')
else
    anIso = true;
    dispstatus( 'Anisotropy:                           ON')
end



% check dataArray
if ~isreal(dataArray)
    error('Relative difference field must be real-valued.')
end
if numel(dataArray(isnan(dataArray)))
    error('Relative difference field must not contain NAN-values!')
end
if numel(dataArray(isinf(dataArray)))
    error('Relative difference field must not contain INF-values!')
end
if sum(mod(gridDimensionVector,2)) ~= 3
    error('Grid dimension size must not be even.')
end




if ~islogical(problemMask)
    error('Array that defines which voxels of RDF shall be used must be logical-valued.')
end

if ~islogical(problemMask)
    error('Array that defines which voxels of RDF shall be used must be logical-valued.')
end

if boolWithConstraintMaskSusceptibility && ~islogical(ConstraintMask.susceptibility)
    error('Arrays that defines regions of constant susceptibility must be logical-valued.')
end

if boolWithConstraintMaskResidual && ~islogical(ConstraintMask.residual)
    error('Arrays that defines regions of constant residual must be logical-valued.')
end


if boolUseSpatialWeightingMatrix && ~isreal(spatialWeightingMatrix)
    error('Spatial weighting matrix must be real-valued.')
end





% set default values for Options struct
if ~myisfield(Options,'solvingType') || isempty(Options.solvingType)
    Options.solvingType = DEFAULT_SOLVINGTYPE;
end

if ~myisfield(Options,'gradientType') || isempty(Options.gradientType)
    Options.gradientType = DEFAULT_GRADIENTTYPE;
end

isResidualGradientWeighting = false;
switch Options.solvingType
    case 'SpatialDomainL2'
        dispstatus('Solving Type:                         Spatial Domain (L2)')
    case 'InverseFiltering'
        dispstatus('Solving Type:                         Inverse Filtering')
        Options.display = false;
        if ~isfield(Options,'DipoleFilter') || isempty(Options.DipoleFilter) ...
                || ~myisfield(Options.DipoleFilter,'parameter') || isempty(Options.DipoleFilter.parameter)
            Options.DipoleFilter.parameter = DEFAULT_DIPOLEFILTER_PARAMETER;
            Options.DipoleFilter.type = DEFAULT_DIPOLEFILTER_TYPE;
            Options.offsetUseBool = false;
            Options.residualWeighting = false;
        end
    case 'SpatialDomainTV'
        dispstatus('Solving Type:                         Spatial Domain (TV)')
        Options.display = false;
        if ~isfield(Options,'GradientMask')
            dispstatus('Gradient Mask for solving type "SpatialDomainTV" automatically set to: RDF.')
            Options.GradientMask.spatialMatrix = dataArray;
        end
        
        if ~myisfield(Options,'SpatialDomainTV')
            Options.SpatialDomainTV = [];
        end
        
        if ~myisfield(Options.SpatialDomainTV,'tol') || isempty(Options.SpatialDomainTV.tol)
            Options.SpatialDomainTV.tol = DEFAULT_TV_TOLVAR;
            dispstatus(['SpatialDomain (TV) ~ tolerance:       DEFAULT (',num2str(Options.SpatialDomainTV.tol),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ tolerance:       ',num2str(Options.SpatialDomainTV.tol),])
        end
        if ~myisfield(Options.SpatialDomainTV,'muMin') || isempty(Options.SpatialDomainTV.muMin)
            Options.SpatialDomainTV.muMin = DEFAULT_TV_MU;
            dispstatus(['SpatialDomain (TV) ~ minimum mu:      DEFAULT (',num2str(Options.SpatialDomainTV.muMin),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ minimum mu:      ',num2str(Options.SpatialDomainTV.muMin),])
        end
        if ~myisfield(Options.SpatialDomainTV,'maxitMu') || isempty(Options.SpatialDomainTV.maxitMu)
            Options.SpatialDomainTV.maxitMu = DEFAULT_TV_MAXINTITER;
            dispstatus(['SpatialDomain (TV) ~ max. iterations mu:          DEFAULT (',num2str(Options.SpatialDomainTV.maxitMu),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ max. iterations mu:          ',num2str(Options.SpatialDomainTV.maxitMu),])
        end
        if ~myisfield(Options.SpatialDomainTV,'maxitInner') || isempty(Options.SpatialDomainTV.maxitInner)
            Options.SpatialDomainTV.maxitInner = DEFAULT_TV_MAXITER;
            dispstatus(['SpatialDomain (TV) ~ max. iterations inner loop:  DEFAULT (',num2str(Options.SpatialDomainTV.maxitInner),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ max. iterations inner loop:  ',num2str(Options.SpatialDomainTV.maxitInner),])
        end
        
        
        
        if ~myisfield(Options.SpatialDomainTV,'lambda') || isempty(Options.SpatialDomainTV.lambda)
            Options.SpatialDomainTV.lambda = DEFAULT_TV_LAMBDA;
            dispstatus(['SpatialDomain (TV) ~ lambda:          DEFAULT (',num2str(Options.SpatialDomainTV.lambda),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ lambda:          ',num2str(Options.SpatialDomainTV.lambda),])
        end
        if ~myisfield(Options.SpatialDomainTV,'stopTest') || isempty(Options.SpatialDomainTV.stopTest)
            Options.SpatialDomainTV.stopTest = DEFAULT_TV_STOPTEST;
            dispstatus(['SpatialDomain (TV) ~ stopTest:        DEFAULT (',num2str(Options.SpatialDomainTV.stopTest),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ stopTest:        ',num2str(Options.SpatialDomainTV.stopTest),])
        end
        if ~myisfield(Options.SpatialDomainTV,'ResidualGradientWeighting') || isempty(Options.SpatialDomainTV.ResidualGradientWeighting)
            dispstatus('SpatialDomain (TV) ~ Residual Gradient Weighting: OFF')
        else
            isResidualGradientWeighting = true;
            if ~myisfield(Options,'residualWeighting') || isempty(Options.residualWeighting)
                error('Residual Gradient Weighting without Residual Weighting not supported.')
            end
            dispstatus('SpatialDomain (TV) ~ Residual Gradient Weighting: ON')
            if ~myisfield(Options.SpatialDomainTV.ResidualGradientWeighting,'parameter') || isempty(Options.SpatialDomainTV.ResidualGradientWeighting.parameter)
                Options.SpatialDomainTV.ResidualGradientWeighting.parameter = DEFAULT_TV_RESIDUALGRADIENTWEIGHTING_PARAMETER;
                dispstatus(['SpatialDomain (TV) ~ Residual Gradient Weighting parameter:  DEFAULT (',num2str(Options.SpatialDomainTV.ResidualGradientWeighting.parameter),')'])
            else
                dispstatus(['SpatialDomain (TV) ~ Residual Gradient Weighting parameter:  ',num2str(Options.SpatialDomainTV.ResidualGradientWeighting.parameter),])
            end
            
            if ~myisfield(Options,'GradientMaskResidual') || isempty(Options.GradientMaskResidual) || ~myisfield(Options.GradientMaskResidual,'spatialMatrix') || isempty(Options.GradientMaskResidual.spatialMatrix)
                isGradientMaskResidualSpatialMatrix = false;
                if ~myisfield(Options,'GradientMask') || isempty(Options.GradientMask)
                    error('If spatialMatrix for GradientMaskResidual is not supplied, GradientMask is needed.')
                end
                dispstatus('Gradient Mask for Residual:           Gradient Mask for Susceptibility (Default)')
            else
                isGradientMaskResidualSpatialMatrix = true;
                dispstatus('Gradient Mask for Residual:           Spatial Matrix supplied')
            end
            if ~myisfield(Options,'GradientMaskResidual') || isempty(Options.GradientMaskResidual) ||~myisfield(Options.GradientMaskResidual,'gradientThreshMin') || isempty(Options.GradientMaskResidual.gradientThreshMin)
                Options.GradientMaskResidual.gradientThreshMin = DEFAULT_GRADIENTMASKRESIDUAL_THRESHOLD;
                dispstatus(['Gradient Mask for Residual Threshold: DEFAULT (',num2str(Options.GradientMaskResidual.gradientThreshMin),')'])
            else
                dispstatus(['Gradient Mask for Residual Threshold: ',num2str(Options.GradientMaskResidual.gradientThreshMin),])
            end
            
        end
        if myisfield(Options.SpatialDomainTV,'Lmu1') && ~isempty(Options.SpatialDomainTV.Lmu1)          % only temporary, will be deleted soon...
            dispstatus(['Lmu1:                                 ',num2str(Options.SpatialDomainTV.Lmu1)])
        end
        
    otherwise
        error('Specified solving type currently not supported.')
end
if ~myisfield(Options,'useHEDI') || isempty(Options.useHEDI) || Options.useHEDI == false;
    dispstatus('HEDI mode:                           DEFAULT (OFF)')
    Options.useHEDI = DEFAULT_USEHEDI;
elseif ~islogical(Options.useHEDI)
    error('Field Options.useHEDI must be boolean!')
end


if ~myisfield(Options,'useHEIDI') || isempty(Options.useHEIDI) || (Options.useHEIDI == false && ~Options.useHEDI);
    Options.useHEIDI = false;
    dispstatus('HEIDI mode:                           DEFAULT (OFF)')
elseif Options.useHEIDI && nRdf > 1
    error('You cannot use HEIDI with COSMOS.')
elseif ~Options.useHEDI && ~islogical(Options.useHEIDI)
    error('Field Options.useHEIDI must be boolean!')
else
    dispstatus('HEIDI mode:                           ON! Go Heidi!')
    Options.PostProcCone.type = 'tv';
    if ~myisfield(Options.PostProcCone,'applyNCFCorrection') || isempty(Options.PostProcCone.applyNCFCorrection)
        Options.PostProcCone.applyNCFCorrection = DEFAULT_POSTPROC_APPLYNCF;
    end
    if ~myisfield(Options,'GradientMask') || isempty(Options.GradientMask) || ~myisfield(Options.GradientMask,'applyLaplacianCorrection') || isempty(Options.GradientMask.applyLaplacianCorrection)
        Options.GradientMask.applyLaplacianCorrection = true;
    end
    if ~myisfield(Options.GradientMask,'MagnitudeForCorrection') || isempty(Options.GradientMask.MagnitudeForCorrection)
        dispstatus('WARNING: Magnitude Data should be supplied for HEIDI mode!!')
    end
    if ~myisfield(Options,'GradientMask') || isempty(Options.GradientMask) || ~myisfield(Options.GradientMask,'applyDenoising') || isempty(Options.GradientMask.applyDenoising)
        Options.GradientMask.applyDenoising = true;
    end
    
    if ~myisfield(Options,'problemMaskClosed') || isempty(Options.problemMaskClosed)
        error('HEIDI requires problemMaskClosed.')
    end
    
    if Options.useHEIDI
        dispstatus('Setting HEIDI default parameters...')
        Options.maxit = 400;
        %Options.maxit = 200;
        Options.tolerance = 1e-5;%1e-5;
        Options.isFourierDomainFormula = false;
        %Options.isFourierDomainFormula = false;
        Options.offsetUseBool = true;
        %Options.DipoleFilter.type =  'threshSingularValues';
        %Options.DipoleFilter.parameter =  DEFAULT_POSTPROC_THRESHOLD-0.05; % 0.05 difference to avoid edge issues in k-space between regions
        % all the parameters commented above were changed to fit old graz
        % result
        Options.DipoleFilter = [];
        if ~isfield(Options,'residualWeighting') || isempty(Options.residualWeighting)
            Options.residualWeighting = 0;
        end
    end
end

if ~myisfield(Options,'useMEDI') || isempty(Options.useMEDI) || Options.useMEDI == false;
    dispstatus('MEDI mode:                           DEFAULT (OFF)')
    Options.useMEDI = DEFAULT_USEMEDI;
elseif ~islogical(Options.useMEDI)
    error('Field Options.useMEDI must be boolean!')
end

if ~Options.useHEIDI
    if Options.useMEDI || Options.useHEDI
        dispstatus('MEDI/HEDI mode:                           ON!')
        Options.PostProcCone = [];
        if Options.useMEDI
            if ~myisfield(Options,'GradientMask') || isempty(Options.GradientMask)
                error('MEDI needs GradientMask-field to be magnitude image.')
                %             elseif ~myisfield(Options.GradientMask,'gradientThreshMin') || isempty(Options.GradientMask.gradientThreshMin)
                %                 error('MEDI needs GradientMask.gradientThreshMin field (e.g. 1.2; ideally 70% edges).')
            end
        end
        
        dispstatus('Setting MEDI/HEDI default parameters...')
        Options.tolerance = 1e-5
        Options.isFourierDomainFormula = false;
        Options.PostProcCone = [];
        Options.residualWeighting = [];
        Options.DipoleFilter = [];
        Options.solvingType = 'SpatialDomainTV';
        %Options.SpatialDomainTV.maxitMu = 4;
        %Options.SpatialDomainTV.maxitInner = 2000;
        %Options.SpatialDomainTV.muMin = 0.1;
        Options.offsetUseBool = false;
        Options.gradientType = 2; % set to two although Tian Liu, 2012, IEEE says cental difference (but this introduces some checkerboard patterns
        Options.SpatialDomainTV.stopTest = 1;
        Options.SpatialDomainTV.maxitMu = DEFAULT_TV_MAXINTITER;
        Options.SpatialDomainTV.maxitInner = DEFAULT_TV_MAXITER;
        if ~myisfield(Options.SpatialDomainTV,'lambda') || isempty(Options.SpatialDomainTV.lambda)
            Options.SpatialDomainTV.lambda = DEFAULT_TV_LAMBDA;
            disp('DEFAULT MEDI/HEDI regularization parameter chosen! This may result in oversmoothing...')
        end
        Options.SpatialDomainTV.muMin = DEFAULT_TV_MU;
        Options.SpatialDomainTV.tol = DEFAULT_TV_TOLVAR;
        Options.SpatialDomainTV.tolerance = DEFAULT_TV_TOLVAR;
        
    end
end









isPreConeSuscSupplied = false;


if ~myisfield(Options,'voxelAspectRatio') || isempty(Options.voxelAspectRatio)
    Options.voxelAspectRatio = DEFAULT_VOXELASPECTRATIO;
end
if ~myisfield(Options,'epsilon') || isempty(Options.epsilon)
    Options.epsilon = 0;
end

if ~myisfield(Options,'gradientMaskReplaceValue') || isempty(Options.gradientMaskReplaceValue)
    Options.gradientMaskReplaceValue = DEFAULT_GRADIENTMASKREPLACEVALUE;
end


if ~isfield(Options,'tolerance') || isempty(Options.tolerance)
    Options.tolerance = DEFAULT_TOLERANCE;
    dispstatus(['Tolerance:                            DEFAULT (',num2str(Options.tolerance),')'])
else
    dispstatus(['Tolerance:                            ',num2str(Options.tolerance),''])
end

if ~isfield(Options,'isReplaceWellPosedDomain') || isempty(Options.isReplaceWellPosedDomain)
    Options.isReplaceWellPosedDomain = DEFAULT_ISREPLACEWELLPOSEDSUBDOMAIN;
    dispstatus(['Replace well-posed domain:                 DEFAULT (',num2str(Options.isReplaceWellPosedDomain),')'])
end


if ~myisfield(Options,'useConstantConstraints')
    Options.useConstantConstraints = DEFAULT_USECONSTANTCONSTRAINTS;
    dispstatus(['Constant constraints:                 DEFAULT (',num2str(Options.useConstantConstraints),')'])
else
    dispstatus(['Constant constraints:                 ',num2str(Options.useConstantConstraints),''])
end

if ~myisfield(Options,'maxit')
    Options.maxit = DEFAULT_MAXIT;
    dispstatus(['Maximum no of cycles:                 DEFAULT (',num2str(Options.maxit),')'])
else
    dispstatus(['Maximum no of cycles:                 ',num2str(Options.maxit)])
end

if ~myisfield(Options,'offsetUseBool')
    Options.offsetUseBool = DEFAULT_OFFSETUSEBOOL;
    dispstatus( 'Automatic offset determination: DEFAULT (YES)')
else
    if Options.offsetUseBool
        dispstatus('Automatic offset determination:       YES')
    else
        dispstatus('Automatic offset determination:       OFF')
    end
end

if ~myisfield(Options,'display')
    Options.display = true;
end

if ~myisfield(Options,'residualWeighting') || isempty(Options.residualWeighting) || Options.residualWeighting == 0
    Options.residualWeighting = 0;
    dispstatus( 'Residual regularization:              DEFAULT (OFF)');
    residualArray = zeros(gridDimensionVector);
else
    dispstatus(['Residual regularization:              ON (',num2str(Options.residualWeighting),')'])
end



if Options.residualWeighting && nargout < 3
    error('Residual-Weighting requires three output variables.')
end


if ~myisfield(Options,'TikhonovRegularizationSusceptibility') || isempty(Options.TikhonovRegularizationSusceptibility)
    isTikhonovRegularizationSusceptibility = false;
    dispstatus( 'Tikhonov Regularization Susc.:        DEFAULT (OFF)')
else
    isTikhonovRegularizationSusceptibility = true;
    if strcmp(Options.TikhonovRegularizationSusceptibility.type,'partial gradient weighting')   % GradientMask is used for Option 'partial gradient weighting'. For Option 'weightedGradient',
        if ~myisfield(Options,'GradientMask')                                                    %   GradientMask is not activated to ensure compatibility with older versions
            Options.GradientMask = [];    % GradientMask activated, default values are set below
        end
    end
end

    isGradientMask = false;
    dispstatus('Gradient Mask:                                  DEFAULT (OFF)')



if myisfield(Options,'ConvolutionFilterCompensation')  && ~isempty(Options.ConvolutionFilterCompensation)
    % if ~myisfield(Options,'dipoleCroppingRadius')
    %     Options.dipoleCroppingRadius = [];
    %     dispstatus( 'Cropped dipole response:        DEFAULT (OFF)')
    % elseif ~isempty(Options.dipoleCroppingRadius)
    %     dispstatus(['Cropped dipole response:        ',num2str(Options.dipoleCroppingRadius),'px'])
    % end
    dispstatus(['Conv. filter compensation:            ',Options.ConvolutionFilterCompensation.type]);
else
    dispstatus( 'Conv. filter compensation:            DEFAULT (OFF)')
    Options.ConvolutionFilterCompensation = [];
end

if ~myisfield(Options,'DipoleFilter') || isempty(Options.DipoleFilter)
    Options.DipoleFilter = [];
    dispstatus( 'Dipole response filter:               DEFAULT (OFF)')
else
    if ~isstruct(Options.DipoleFilter) || ~myisfield(Options.DipoleFilter,'type') || ~myisfield(Options.DipoleFilter,'parameter')
        error('Specification of parameter and type field of dipole-filter struct required!')
    end
    dispstatus(['Dipole response filter:               ',Options.DipoleFilter.type,' (',num2str(Options.DipoleFilter.parameter),')']);
end

if myisfield(Options,'smvErrorTermThickness')
    error('smvErrorTermThickness field is deprecated!');
    %     Options.smvErrorTermThickness = false;
    %     dispstatus( 'SMV-Surfaceterm:                DEFAULT (OFF)')
    % elseif Options.smvErrorTermThickness
    %     dispstatus( 'SMV-Surfaceterm:                ON')
end

if ~myisfield(Options,'boolPreconditioner')
    Options.boolPreconditioner = false;
    dispstatus( 'Preconditioning:                      DEFAULT (OFF)')
elseif Options.boolPreconditioner
    dispstatus( 'Preconditioning:                      ON')
    
    if  ~myisfield(Options,'preconditionerParameter')
        error('No parameter for preconditioning specified.')
    elseif  ~myisfield(Options,'PreconditionerFilter')
        Options.PreconditionerFilter = [];
        dispstatus(['Preconditioning filter:               ',Options.PreconditionerFilter.type,' (',num2str(Options.PreconditionerFilter.parameter),')']);
        
    end
end

if ~myisfield(Options,'chemicalShiftFactor')
    Options.chemicalShiftFactor = false;
    dispstatus( 'Chemical Shift Factor:                DEFAULT (OFF)')
elseif Options.chemicalShiftFactor
    dispstatus(['Chemical Shift Factor:                ',num2str(Options.chemicalShiftFactor)])
end

nProblemMaskVoxel = numel(problemMask(problemMask == true));
dispstatus(['Available data samples (total): ',num2str(nProblemMaskVoxel)])

nProblemMaskVoxelIndividual = zeros(nRdfUsed,1);
for jRdf = 1:nRdfUsed
    tmp = problemMask(:,:,:,jRdf);
    nProblemMaskVoxelIndividual(jRdf) = numel(tmp(tmp == true));
    
    dispstatus(['Available data samples RDF',num2str(jRdf),': ',num2str(nProblemMaskVoxelIndividual(jRdf))])
    clear tmp
end

nProblemMaskVoxelOne = numel(problemMaskOne(problemMaskOne == true));




% determine number of constrained regions
if boolWithConstraintMaskSusceptibility
    if ndims(ConstraintMask.susceptibility) == 3 && sum(ConstraintMask.susceptibility(:))
        nConstrainedRegionSusceptibility = 1;
    else
        
        % remove all empty regions
        nConstrainedRegionSusceptibility = 0;
        for jDim = 1:size(ConstraintMask.susceptibility,4)
            if sum(sum(sum(ConstraintMask.susceptibility(:,:,:,jDim))))
                nConstrainedRegionSusceptibility = nConstrainedRegionSusceptibility + 1;
                ConstraintMask.susceptibility(:,:,:,nConstrainedRegionSusceptibility) = ConstraintMask.susceptibility(:,:,:,jDim);
            end
        end
        if nConstrainedRegionSusceptibility
            ConstraintMask.susceptibility = ConstraintMask.susceptibility(:,:,:,1:nConstrainedRegionSusceptibility);
        else
            boolWithConstraintMaskSusceptibility = false;
            nConstrainedRegionSusceptibility = 0;
        end
    end
    dispstatus(['Number of constrained regions (S):',num2str(nConstrainedRegionSusceptibility)])
end

if boolWithConstraintMaskResidual
    if ndims(ConstraintMask.residual) == 3 %%&& sum(ConstraintMask.susceptibility(:))
        nConstrainedRegionResidual = 1;
    else
        
        % remove all empty regions
        nConstrainedRegionResidual = 0;
        for jDim = 1:size(ConstraintMask.residual,4)
            if sum(sum(sum(ConstraintMask.residual(:,:,:,jDim))))
                nConstrainedRegionResidual = nConstrainedRegionResidual + 1;
                ConstraintMask.residual(:,:,:,nConstrainedRegionResidual) = ConstraintMask.residual(:,:,:,jDim);
            end
        end
        if nConstrainedRegionResidual
            ConstraintMask.residual = ConstraintMask.residual(:,:,:,1:nConstrainedRegionResidual);
        else
            boolWithConstraintMaskResidual = false;
            nConstrainedRegionResidual = 0;
        end
    end
    dispstatus(['Number of constrained regions (R):',num2str(nConstrainedRegionResidual)])
end




% check that constrained regions are disjoint
if boolWithConstraintMaskSusceptibility
    tmp = false(gridDimensionVector);
    for jConstrainedRegionSusceptibility = 1:nConstrainedRegionSusceptibility
        tmp = tmp & ConstraintMask.susceptibility(:,:,:,jConstrainedRegionSusceptibility);
    end
    if sum(tmp(:))
        error('Regions of constrained susceptibility must be disjoint!')
    end
    clear tmp
end
if boolWithConstraintMaskResidual
    tmp = false(gridDimensionVector);
    for jConstrainedRegionResidual = 1:nConstrainedRegionResidual
        tmp = tmp & ConstraintMask.residual(:,:,:,jConstrainedRegionResidual);
    end
    if sum(tmp(:))
        error('Regions of constrained residual must be disjoint!')
    end
    clear tmp
end





% determine vector and matrix sizes

if boolWithConstraintMaskSusceptibility
    
    freeRegion = zeros(gridDimensionVector);
    if Options.useConstantConstraints
        for jConstrainedRegion = 1:nConstrainedRegionSusceptibility
            freeRegion = freeRegion + ConstraintMask.susceptibility(:,:,:,jConstrainedRegion);
        end
    end
    freeRegion = freeRegion == 0;
    nVoxelFreeRegion = sum(freeRegion(:));
    
    %nDegreeFreedomSuscept = numel(ConstraintMask.susceptibility(ConstraintMask.susceptibility == false)) + 1;
    nDegreeFreedomSuscept = nVoxelFreeRegion + nConstrainedRegionSusceptibility;
else
    nDegreeFreedomSuscept = prod(gridDimensionVector);
    freeRegion = true(gridDimensionVector);
end

% if Options.offsetUseBool
%     offsetFreedom = nRdf;
% else
%     offsetFreedom = 0;
% end


if boolWithConstraintMaskResidual
    
    freeRegionResidual = zeros([gridDimensionVector nRdfUsed]);
    for jConstrainedRegion = 1:nConstrainedRegionResidual
        freeRegionResidual = freeRegionResidual + ConstraintMask.residual(:,:,:,jConstrainedRegion);
    end
    freeRegionResidual = freeRegionResidual == 0 & problemMaskOne;
    nVoxelFreeRegionResidual = sum(freeRegionResidual(:) & problemMaskOne(:));
    nDegreeFreedomResidual = nVoxelFreeRegionResidual + nConstrainedRegionResidual;
elseif Options.residualWeighting
    nDegreeFreedomResidual = sum(problemMaskOne(:));
    nVoxelFreeRegionResidual = sum(problemMaskOne(:));
    freeRegionResidual = problemMaskOne;
else
    nDegreeFreedomResidual = 0;
end

dispstatus(['Degrees of Freedom Susceptibility: ',num2str(nDegreeFreedomSuscept)])
dispstatus(['Degrees of Freedom Residual: ',num2str(nDegreeFreedomResidual)])

dispstatus(['Percent determined (optimal): ',num2str(floor(nProblemMaskVoxel/(nProblemMaskVoxelOne+nDegreeFreedomResidual)*100)),'%'])
dispstatus(['Percent determined (bad background correction): ',num2str(floor(nProblemMaskVoxel/(nDegreeFreedomSuscept+nDegreeFreedomResidual)*100)),'%'])
dispstatus(['Virtual percent determined (susceptibility only): ',num2str(floor(nProblemMaskVoxel/(nProblemMaskVoxelOne)*100)),'%'])

if Options.isProblemMaskClosedFlat
    forceSmoothSusceptibilityMask = Options.problemMaskClosed & ~problemMask;
else
    forceSmoothSusceptibilityMask = false(size(problemMask));
end

if isGradientMask
    
    if isGradientMaskSupplied
        GradientMask_x = Options.GradientMask.suppliedMask(:,:,:,1);
        GradientMask_y = Options.GradientMask.suppliedMask(:,:,:,2);
        GradientMask_z = Options.GradientMask.suppliedMask(:,:,:,3);
    else
        if ~Options.useMEDI
            normalizationFactor = 1/Options.echoTime ./ Options.magneticFieldStrength;
        else
            normalizationFactor = 1;
        end
        
        if isDenoisingBeforeGradientCalc
            Options.GradientMask.filterOptions.voxelsize = Options.voxelAspectRatio;
            PhaseDenoised = diffusionimagefilter(Options.GradientMask.spatialMatrix.*problemMask,Options.GradientMask.filterOptions);
            
            
            Phase_Gradient_x = abs(discretegradient(PhaseDenoised .* normalizationFactor,'x',Options.voxelAspectRatio(1),DEFAULT_GRADIENTTYPE));
            Phase_Gradient_y = abs(discretegradient(PhaseDenoised .* normalizationFactor,'y',Options.voxelAspectRatio(2),DEFAULT_GRADIENTTYPE));
            Phase_Gradient_z = abs(discretegradient(PhaseDenoised .* normalizationFactor,'z',Options.voxelAspectRatio(3),DEFAULT_GRADIENTTYPE));
        else
            Phase_Gradient_x = abs(discretegradient(Options.GradientMask.spatialMatrix  .* normalizationFactor,'x',Options.voxelAspectRatio(1),DEFAULT_GRADIENTTYPE));
            Phase_Gradient_y = abs(discretegradient(Options.GradientMask.spatialMatrix  .* normalizationFactor,'y',Options.voxelAspectRatio(2),DEFAULT_GRADIENTTYPE));
            Phase_Gradient_z = abs(discretegradient(Options.GradientMask.spatialMatrix  .* normalizationFactor,'z',Options.voxelAspectRatio(3),DEFAULT_GRADIENTTYPE));
        end
        
        if isDenoisingAfterGradientCalc
            Phase_Gradient_x = diffusionimagefilter(Phase_Gradient_x,Options.GradientMask.filterOptions);
            Phase_Gradient_y = diffusionimagefilter(Phase_Gradient_y,Options.GradientMask.filterOptions);
            Phase_Gradient_z = diffusionimagefilter(Phase_Gradient_z,Options.GradientMask.filterOptions);
        end
        
        % calculate normalization of magnitude in case of MEDI
        if Options.useMEDI
            Phase_Gradient_x_Mean = mean(Phase_Gradient_x(problemMask));
            Phase_Gradient_y_Mean = mean(Phase_Gradient_y(problemMask));
            Phase_Gradient_z_Mean = mean(Phase_Gradient_z(problemMask));
            normalizationFactor2 = mean([Phase_Gradient_x_Mean,Phase_Gradient_y_Mean,Phase_Gradient_z_Mean]);
        else
            normalizationFactor2 = 1;
        end
        
        
        
        if Options.useMEDI
            tmp.sortedValues = sort(Phase_Gradient_x(problemMask),'descend');
            GradientMask_x = ~(Phase_Gradient_x > tmp.sortedValues(round(numel(Phase_Gradient_x(problemMask))*0.1)));
            
            tmp.sortedValues = sort(Phase_Gradient_y(problemMask),'descend');
            GradientMask_y = ~(Phase_Gradient_y > tmp.sortedValues(round(numel(Phase_Gradient_y(problemMask))*0.1)));
            
            tmp.sortedValues = sort(Phase_Gradient_z(problemMask),'descend');
            GradientMask_z = ~(Phase_Gradient_z > tmp.sortedValues(round(numel(Phase_Gradient_z(problemMask))*0.1)));
            %
        else
            GradientMask_x = Phase_Gradient_x < Options.GradientMask.gradientThreshMin * normalizationFactor2;
            GradientMask_y = Phase_Gradient_y < Options.GradientMask.gradientThreshMin * normalizationFactor2;
            GradientMask_z = Phase_Gradient_z < Options.GradientMask.gradientThreshMin * normalizationFactor2;
        end
        
        if isLaplacianCorrection
            Options.Laplacian = '2';
            
            if isDenoisingBeforeGradientCalc
                Phase_Laplacian = abs(ifftsave(fftsave(PhaseDenoised) .* fftsave(generatelaplacian3d(gridDimensionVector,Options))));
            else
                Phase_Laplacian = abs(ifftsave(fftsave(Options.GradientMask.spatialMatrix) .* fftsave(generatelaplacian3d(gridDimensionVector,Options))));
            end
            
            Phase_Laplacian = Phase_Laplacian ./ Options.echoTime ./ Options.magneticFieldStrength;
            PhaseMask_Laplacian = Phase_Laplacian > Options.GradientMask.laplacianThreshMin;
        end
        
        if isMagnitudeCorrection
            if ndims(Options.GradientMask.MagnitudeForCorrection) == 4 % --> relaxation array (already denoised)
                relax_Gradient_x = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection(:,:,:,1),'x',Options.voxelAspectRatio(1),DEFAULT_GRADIENTTYPE));
                relax_Gradient_y = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection(:,:,:,1),'y',Options.voxelAspectRatio(2),DEFAULT_GRADIENTTYPE));
                relax_Gradient_z = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection(:,:,:,1),'z',Options.voxelAspectRatio(3),DEFAULT_GRADIENTTYPE));
                
                s0_Gradient_x = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection(:,:,:,2),'x',Options.voxelAspectRatio(1),DEFAULT_GRADIENTTYPE));
                s0_Gradient_y = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection(:,:,:,2),'y',Options.voxelAspectRatio(2),DEFAULT_GRADIENTTYPE));
                s0_Gradient_z = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection(:,:,:,2),'z',Options.voxelAspectRatio(3),DEFAULT_GRADIENTTYPE));
                
                relax_Gradient_x_Mean = mean(relax_Gradient_x(problemMask));
                relax_Gradient_y_Mean = mean(relax_Gradient_y(problemMask));
                relax_Gradient_z_Mean = mean(relax_Gradient_z(problemMask));
                relax_Gradient_Mean = mean([relax_Gradient_x_Mean,relax_Gradient_y_Mean,relax_Gradient_z_Mean]);
                
                s0_Gradient_x_Mean = mean(s0_Gradient_x(problemMask));
                s0_Gradient_y_Mean = mean(s0_Gradient_y(problemMask));
                s0_Gradient_z_Mean = mean(s0_Gradient_z(problemMask));
                s0_Gradient_Mean = mean([s0_Gradient_x_Mean,s0_Gradient_y_Mean,s0_Gradient_z_Mean]);
                
                
                MagnitudeGradientMask_x = relax_Gradient_x > Options.GradientMask.magnitudeThresholdMin * relax_Gradient_Mean;  % threshold normalized to average of gradient
                MagnitudeGradientMask_y = relax_Gradient_y > Options.GradientMask.magnitudeThresholdMin * relax_Gradient_Mean;
                MagnitudeGradientMask_z = relax_Gradient_z > Options.GradientMask.magnitudeThresholdMin * relax_Gradient_Mean;
                
                MagnitudeGradientMask_x = MagnitudeGradientMask_x | (s0_Gradient_x > Options.GradientMask.magnitudeThresholdMin * s0_Gradient_Mean);  % threshold normalized to average of gradient
                MagnitudeGradientMask_y = MagnitudeGradientMask_y | (s0_Gradient_y > Options.GradientMask.magnitudeThresholdMin * s0_Gradient_Mean);
                MagnitudeGradientMask_z = MagnitudeGradientMask_z | (s0_Gradient_z > Options.GradientMask.magnitudeThresholdMin * s0_Gradient_Mean);
                
                
                warning('In principle here should be denoising of gradients as well!')
            else
                if isDenoisingBeforeGradientCalc
                    Options.GradientMask.MagnitudeForCorrection = diffusionimagefilter(Options.GradientMask.MagnitudeForCorrection,Options.GradientMask.filterOptions);
                end
                Magn_Gradient_x = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection,'x',Options.voxelAspectRatio(1),DEFAULT_GRADIENTTYPE));
                Magn_Gradient_y = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection,'y',Options.voxelAspectRatio(2),DEFAULT_GRADIENTTYPE));
                Magn_Gradient_z = abs(discretegradient(Options.GradientMask.MagnitudeForCorrection,'z',Options.voxelAspectRatio(3),DEFAULT_GRADIENTTYPE));
                
                if isDenoisingAfterGradientCalc
                    Magn_Gradient_x = diffusionimagefilter(Magn_Gradient_x,Options.GradientMask.filterOptions);
                    Magn_Gradient_y = diffusionimagefilter(Magn_Gradient_y,Options.GradientMask.filterOptions);
                    Magn_Gradient_z = diffusionimagefilter(Magn_Gradient_z,Options.GradientMask.filterOptions);
                end
                
                Magn_Gradient_x_Mean = mean(Magn_Gradient_x(problemMask));
                Magn_Gradient_y_Mean = mean(Magn_Gradient_y(problemMask));
                Magn_Gradient_z_Mean = mean(Magn_Gradient_z(problemMask));
                Magn_Gradient_Mean = mean([Magn_Gradient_x_Mean,Magn_Gradient_y_Mean,Magn_Gradient_z_Mean]);
                MagnitudeGradientMask_x = Magn_Gradient_x > Options.GradientMask.magnitudeThresholdMin * Magn_Gradient_Mean;  % threshold normalized to average of gradient
                MagnitudeGradientMask_y = Magn_Gradient_y > Options.GradientMask.magnitudeThresholdMin * Magn_Gradient_Mean;
                MagnitudeGradientMask_z = Magn_Gradient_z > Options.GradientMask.magnitudeThresholdMin * Magn_Gradient_Mean;
            end
            
            
        end
        
        clear PhaseDenoised Magn_Gradient_x Magn_Gradient_y Magn_Gradient_z
        
        if isMagnitudeCorrection
            MagnitudeGradientMask_x = (MagnitudeGradientMask_x.*problemMask) | forceSmoothSusceptibilityMask;
            MagnitudeGradientMask_y = (MagnitudeGradientMask_y.*problemMask) | forceSmoothSusceptibilityMask;
            MagnitudeGradientMask_z = (MagnitudeGradientMask_z.*problemMask) | forceSmoothSusceptibilityMask;
            GradientMask_x = GradientMask_x .* ~MagnitudeGradientMask_x ;   % where Gradient of Magnitude is very large, Gradient Mask is set to zero
            GradientMask_y = GradientMask_y .* ~MagnitudeGradientMask_y ;
            GradientMask_z = GradientMask_z .* ~MagnitudeGradientMask_z ;
        end
        clear MagnitudeGradientMask_x MagnitudeGradientMask_y MagnitudeGradientMask_z
        if isLaplacianCorrection
            PhaseMask_Laplacian = PhaseMask_Laplacian | forceSmoothSusceptibilityMask; % to avoid (wrong) non-zero values in PhaseMask_LaplacianSmall due to sharp edges in problemMask
            
            GradientMask_x = GradientMask_x .* ~PhaseMask_Laplacian ;   % where Laplacian of Phase is very large, Gradient Mask is set to zero
            GradientMask_y = GradientMask_y .* ~PhaseMask_Laplacian ;
            GradientMask_z = GradientMask_z .* ~PhaseMask_Laplacian ;
        end
        clear PhaseMask_Laplacian
        
        if myisfield(Options.GradientMask,'additionalCorrectionMask_x') && ~isempty(Options.GradientMask.additionalCorrectionMask_x)
            GradientMask_x = GradientMask_x & Options.GradientMask.additionalCorrectionMask_x;
            GradientMask_y = GradientMask_y & Options.GradientMask.additionalCorrectionMask_y;
            GradientMask_z = GradientMask_z & Options.GradientMask.additionalCorrectionMask_z;
        end
        
        GradientMask_x = (GradientMask_x.*problemMask) | forceSmoothSusceptibilityMask;
        GradientMask_y = (GradientMask_y.*problemMask) | forceSmoothSusceptibilityMask;
        GradientMask_z = (GradientMask_z.*problemMask) | forceSmoothSusceptibilityMask;
        
        clear Phase_Laplacian PhaseMask_LaplacianBig Phase_Gradient_x Phase_Gradient_y Phase_Gradient_z
    end
    
else
    GradientMask_x = false(gridDimensionVector);
    GradientMask_y = false(gridDimensionVector);
    GradientMask_z = false(gridDimensionVector);
end



PercentGradientMask = (sum(GradientMask_x(:))/nProblemMaskVoxelOne) * 100;
dispstatus(['Percentage of Gradient Mask: ',num2str(floor(PercentGradientMask)),'%'])


if isResidualGradientWeighting
    if isGradientMaskResidualSpatialMatrix
        Phase_Gradient_x = discretegradient(Options.GradientMaskResidual.spatialMatrix ./ Options.echoTime ./ Options.magneticFieldStrength,'x',Options.voxelAspectRatio(1),Options.gradientType);
        Phase_Gradient_y = discretegradient(Options.GradientMaskResidual.spatialMatrix ./ Options.echoTime ./ Options.magneticFieldStrength,'y',Options.voxelAspectRatio(2),Options.gradientType);
        Phase_Gradient_z = discretegradient(Options.GradientMaskResidual.spatialMatrix ./ Options.echoTime ./ Options.magneticFieldStrength,'z',Options.voxelAspectRatio(3),Options.gradientType);
        
        PhaseGradientThreshMin = Options.GradientMaskResidual.gradientThreshMin;
        
        GradientMaskResidual_x = abs(Phase_Gradient_x) < PhaseGradientThreshMin;
        GradientMaskResidual_y = abs(Phase_Gradient_y) < PhaseGradientThreshMin;
        GradientMaskResidual_z = abs(Phase_Gradient_z) < PhaseGradientThreshMin;
        
        %         GradientMaskResidual_x = GradientMaskResidual_x .* problemMask_eroded;
        %         GradientMaskResidual_y = GradientMaskResidual_y .* problemMask_eroded;
        %         GradientMaskResidual_z = GradientMaskResidual_z .* problemMask_eroded;
        %
        GradientMaskResidual_x = (GradientMaskResidual_x.*problemMask) | forceSmoothSusceptibilityMask;
        GradientMaskResidual_y = (GradientMaskResidual_y.*problemMask) | forceSmoothSusceptibilityMask;
        GradientMaskResidual_z = (GradientMaskResidual_z.*problemMask) | forceSmoothSusceptibilityMask;
    else
        GradientMaskResidual_x = GradientMask_x;    % if spatial Matrix for GradientMaskResidual is not supplied, use GradientMask
        GradientMaskResidual_y = GradientMask_y;
        GradientMaskResidual_z = GradientMask_z;
        
    end
else
    GradientMaskResidual_x = [];
end
clear forceSmoothSusceptibilityMask





nPowerRegularizationVoxel = 0;
if ~myisfield(Options,'PowerRegularization') || isempty(Options.PowerRegularization)
    isPowerRegularization = false;
    dispstatus( 'Tikhonov Regularization Susc. power-term: DEFAULT (OFF)')
else
    isPowerRegularization = true;
    dispstatus(['Tikhonov Regularization Susc. power-term: (',num2str(Options.PowerRegularization.parameter),')'])
    nPowerRegularizationVoxel = nProblemMaskVoxelOne;
    if ~myisfield(Options.PowerRegularization,'spatialMatrix') || ~isempty(Options.PowerRegularization.spatialMatrix)
        PowerTermWeightingMatrix = problemMaskOne;
    else
        PowerTermWeightingMatrix = Options.PowerRegularization.spatialMatrix;
        PowerTermWeightingMatrix = 1./PowerTermWeightingMatrix;
        PowerTermWeightingMatrix(isinf(PowerTermWeightingMatrix)) = 0;
        PowerTermWeightingMatrix = PowerTermWeightingMatrix ./ max(PowerTermWeightingMatrix(:));
    end
end




nTikhonovTermSusceptibilityVoxel = 0;

if isTikhonovRegularizationSusceptibility
    if myisfield(Options.TikhonovRegularizationSusceptibility,'type')
        tikhonovVoxelMask_x = problemMaskOne;
        tikhonovVoxelMask_y = problemMaskOne;
        tikhonovVoxelMask_z = problemMaskOne;
        switch Options.TikhonovRegularizationSusceptibility.type
            case 'laplacian'
                dispstatus(['Tikhonov Regularization Susc.:  laplacian (',num2str(Options.TikhonovRegularizationSusceptibility.parameter),')'])
                tikhonovSusceptibilityMatrixType = 'fourier';
                nTikhonovTermSusceptibilityVoxel = nProblemMaskVoxelOne;
                tikhonovMatrixSusceptibilityFt = fftsave(generatelaplacian3d(gridDimensionVector));
            case 'weightedGradient'
                dispstatus(['Tikhonov Regularization Susc.:  weighted gradient (',num2str(Options.TikhonovRegularizationSusceptibility.parameter),')'])
                tikhonovSusceptibilityMatrixType = 'mixed';
                nTikhonovTermSusceptibilityVoxel = nProblemMaskVoxelOne;
                [gX gY gZ] = gradient(Options.TikhonovRegularizationSusceptibility.spatialMatrix);
                %                 gNorm = sqrt(gX.^2 +gY.^2+gZ.^2);
                GradientMask_x = 1./abs(gX);
                GradientMask_y = 1./abs(gY);
                GradientMask_z = 1./abs(gZ);
                GradientMask_x(isinf(GradientMask_x)) = 0;
                GradientMask_y(isinf(GradientMask_y)) = 0;
                GradientMask_z(isinf(GradientMask_z)) = 0;
                
                %                 problemMask_eroded = erodemask(problemMask,1);
                GradientMask_x = GradientMask_x .* problemMask;
                GradientMask_y = GradientMask_y .* problemMask;
                GradientMask_z = GradientMask_z .* problemMask;
                
                GradientMask_x(abs(GradientMask_x) > 5*mean(GradientMask_x(:))) = 5*mean(GradientMask_x(:));        % to eliminate extremely high values
                GradientMask_y(abs(GradientMask_y) > 5*mean(GradientMask_y(:))) = 5*mean(GradientMask_y(:));
                GradientMask_z(abs(GradientMask_z) > 5*mean(GradientMask_z(:))) = 5*mean(GradientMask_z(:));
                
                GradientMask_x = GradientMask_x ./ max(GradientMask_x(:));
                GradientMask_y = GradientMask_y ./ max(GradientMask_y(:));
                GradientMask_z = GradientMask_z ./ max(GradientMask_z(:));
                clear gNorm gX gY gZ
            case 'weightedGradientDirect'
                dispstatus(['Tikhonov Regularization Susc.:  weighted gradient direct (',num2str(Options.TikhonovRegularizationSusceptibility.parameter),')'])
                tikhonovSusceptibilityMatrixType = 'mixed';
                nTikhonovTermSusceptibilityVoxel = nnz(Options.TikhonovRegularizationSusceptibility.spatialMatrix);
                
                GradientMask_x = Options.TikhonovRegularizationSusceptibility.spatialMatrix;
                GradientMask_y = Options.TikhonovRegularizationSusceptibility.spatialMatrix;
                GradientMask_z = Options.TikhonovRegularizationSusceptibility.spatialMatrix;
                tikhonovVoxelMask_x = Options.TikhonovRegularizationSusceptibility.spatialMatrix;
                tikhonovVoxelMask_y = Options.TikhonovRegularizationSusceptibility.spatialMatrix;
                tikhonovVoxelMask_z = Options.TikhonovRegularizationSusceptibility.spatialMatrix;
            case 'partial gradient weighting'
                dispstatus(['Tikhonov Regularization Susc.: partial gradient weighting (',num2str(Options.TikhonovRegularizationSusceptibility.parameter),')']);
                tikhonovSusceptibilityMatrixType = 'mixed';
                nTikhonovTermSusceptibilityVoxel = nProblemMaskVoxelOne;
                Options.Laplacian = '2';
            otherwise
                error('Unsupported Tikhonov regularization type.')
        end
    else
        error('Tikhonov regularization type not specified.')
    end
end





% Moritz 21.8.:

if manyFieldsBool
    if  ~myisfield(Options, 'Rotation') || ((~myisfield(Options.Rotation, 'angle') || ~myisfield(Options.Rotation,'axis')) && ~myisfield(Options.Rotation,'matrix'))
        error('Rotation axis and/or rotation angles not specified.')
    end
    
    
    % convert Rotation matrix into the angle and axis representation
    if ((~myisfield(Options.Rotation, 'angle') || ~myisfield(Options.Rotation,'axis')) && ~myisfield(Options.Rotation,'matrix'))
        
        nRotMats = size(Options.Rotation.matrix, 3);
        for iRotMat=1:nRotMats
            if det(Options.Rotation.matrix(:,:,iRotMat)) ~= 1
                error(['Calculated rotation matrix ' num2str(iRotMat) ' is improper, i.e. it contains reflections. This would result in improper angle calculations in the following (e.g. using vrrotmat2vec). For this reason I am aborting here.'])
            end
            vectorExpressionTmp = vrrotmat2vec(Options.Rotation.matrix(:,:,iRotMat));
            Options.Rotation.axis(iRotMat,:) = vectorExpressionTmp(1:3);
            Options.Rotation.angle(iRotMat)  = rad2deg(vectorExpressionTmp(4));
        end
    end
    
    if size(Options.Rotation.angle) ~= size(Options.Rotation.axis,1)
        error('Number of input rotation axis and input rotation angles must agree.')
    elseif size(Options.Rotation.axis,1)~=nRdf
        error('Number of input fields and specified rotations disagree.')
    end
    
    % Check angles and make positive
    for iRdf=1:nRdf
        
        if abs(Options.Rotation.angle(iRdf))>360
            error('Angles must not be larger than 360 degrees.')
        end
        
        if Options.Rotation.angle(iRdf)<0
            Options.Rotation.angle(iRdf)=360+Options.Rotation.angle(iRdf);
        end
        
    end
    
    
    % Check axes
    axisNorms = sqrt(Options.Rotation.axis(:,1).^2+Options.Rotation.axis(:,2).^2+Options.Rotation.axis(:,3).^2); % norms of rotation axis
    for iRdf=1:nRdf
        Options.Rotation.axis(iRdf,:)=Options.Rotation.axis(iRdf,:)/axisNorms(iRdf);
    end
    
    for iRdf=1:nRdf-1
        for jRdf=iRdf+1:nRdf
            if (abs(Options.Rotation.axis(iRdf,1)-Options.Rotation.axis(jRdf,1))<DEFAULT_TOLERANCE && abs(Options.Rotation.axis(iRdf,2)-Options.Rotation.axis(jRdf,2))<DEFAULT_TOLERANCE && abs(Options.Rotation.axis(iRdf,3)-Options.Rotation.axis(jRdf,3))<DEFAULT_TOLERANCE && Options.Rotation.angle(iRdf)==Options.Rotation.angle(jRdf))
                error('Two times same rotation.')
            end
        end
    end
    
    for iRdf=1:nRdf-1
        for jRdf=iRdf+1:nRdf
            if (abs(Options.Rotation.axis(iRdf,1)+Options.Rotation.axis(jRdf,1))<DEFAULT_TOLERANCE && abs(Options.Rotation.axis(iRdf,2)+Options.Rotation.axis(jRdf,2))<DEFAULT_TOLERANCE && abs(Options.Rotation.axis(iRdf,3)+Options.Rotation.axis(jRdf,3))<DEFAULT_TOLERANCE && Options.Rotation.angle(iRdf)==360-Options.Rotation.angle(jRdf))
                error('Two times same rotation.')
            end
        end
    end
    
    
end


%% initialization

jIterationCycle = 0;




%dataArray = mask4ddata(dataArray,problemMask);
dataArray = dataArray .* problemMask;

if nargout > 9 && ~manyFieldsBool
    % in this case we need the RDF below
    rdf = dataArray;
end



% preprocessing: throw away bad voxels and align matrix as vector
% concatenate rdf-data as vector
%rdfVectorGoodVoxels = zeros(nProblemMaskVoxel * nRdfUsed,1);
%rdfVectorGoodVoxels = zeros(nProblemMaskVoxel,1);
%for jRdf = 1:nRdfUsed
%tmp = dataArray(:,:,:,jRdf);
%rdfVectorGoodVoxels((jRdf-1) * nProblemMaskVoxel + 1 : jRdf * nProblemMaskVoxel) = tmp(problemMask(:,:,:,jRdf));
%tmp = dataArray(:,:,:,jRdf);
rdfVectorGoodVoxels = dataArray(problemMask);
%   clear tmp
%end


% add tikhonov-zeros
if isPowerRegularization
    rdfVectorGoodVoxels = cat(1,rdfVectorGoodVoxels,zeros(nPowerRegularizationVoxel,1));
end
if isTikhonovRegularizationSusceptibility
    rdfVectorGoodVoxels = cat(1,rdfVectorGoodVoxels,zeros(3*nTikhonovTermSusceptibilityVoxel,1));
end



% generate dipole response to be used for inversion
boolFourierDomain = true;
if ~manyFieldsBool
    
    dataArray = generateunitdipoleresponse(gridDimensionVector, boolFourierDomain,Options,true);
    
    
    % note: dataArray is now dipole response! (before: RDF)
    
    dataArrayOrig = dataArray;
    if isfield(Options,'kSpaceUndersamplingMatrixPhaseRelaxed')
        dataArray = dataArray .* Options.kSpaceUndersamplingMatrixPhaseRelaxed;
    end
    
    %    warning('kSpaceUndersamplingMatrixPhaseRelaxed removed')
    
    
else
    if strcmp(Options.multiAngleMethod,'difference')
        OptionsmanyFieldsBool=Options;
        
        OptionsmanyFieldsBool.Rotation.angle=Options.Rotation.angle(1);
        OptionsmanyFieldsBool.Rotation.axis=Options.Rotation.axis(1,:);
        if ~isempty(OptionsmanyFieldsBool.DipoleFilter)
            dispstatus('WARNING: Dipole Filter currently not supported for multiple angle acqusitions.')
        end
        OptionsmanyFieldsBool.DipoleFilter = [];
        dataArray=generateunitdipoleresponse(gridDimensionVector, boolFourierDomain,OptionsmanyFieldsBool,true);
        dataArray=(nRdf-1)*dataArray;
        % note: dataArray is now dipole response! (before: RDF)
        
        for iRdf=2:nRdf
            OptionsmanyFieldsBool.Rotation.angle=Options.Rotation.angle(iRdf);
            OptionsmanyFieldsBool.Rotation.axis=Options.Rotation.axis(iRdf,:);
            dataArray=dataArray-generateunitdipoleresponse(gridDimensionVector, boolFourierDomain,OptionsmanyFieldsBool,true);
        end
        
        if isfield(Options,'kSpaceUndersamplingMatrixPhaseRelaxed')
            dataArrayOrig = dataArray;
            dataArray = dataArray .* Options.kSpaceUndersamplingMatrixPhaseRelaxed;
        end
        %warning('kSpaceUndersamplingMatrixPhaseRelaxed removed')
        
    else strcmp(Options.multiAngleMethod,'simultaneous')
        
        dataArray = zeros([gridDimensionVector nRdfUsed]);
        dataArrayOrig = dataArray;
        OptionsTmp = Options;
        for jRdf = 1:nRdf
            OptionsTmp.Rotation.angle=Options.Rotation.angle(jRdf);
            OptionsTmp.Rotation.axis =Options.Rotation.axis(jRdf,:);
            
            dataArray(:,:,:,jRdf) = generateunitdipoleresponse(gridDimensionVector, boolFourierDomain,OptionsTmp,true);
            
            if isfield(Options,'kSpaceUndersamplingMatrixPhaseRelaxed')
                dataArrayOrig(:,:,:,jRdf) = dataArray(:,:,:,jRdf);
                dataArray(:,:,:,jRdf) = dataArray(:,:,:,jRdf) .* Options.kSpaceUndersamplingMatrixPhaseRelaxed;
            end
            %warning('kSpaceUndersamplingMatrixPhaseRelaxed removed')
            
        end
    end
end



clear dataArrayOrig


% include linear term into dipole-response function (if chemical-shift factor is used)
if Options.chemicalShiftFactor
    error('This method may not work any more... (deprecated/development stopped)')
    %dataArray = dataArray + Options.chemicalShiftFactor * (1-fftsave(discretizesphereshell(gridDimensionVector, Options.dipoleCroppingRadius, Options.smvErrorTermThickness)));
end



% calculate RDF of constant susceptibility region (constraint)
if boolWithConstraintMaskSusceptibility
    rdfSusceptibilityConstraint = zeros([gridDimensionVector,nConstrainedRegionSusceptibility,nRdf]);
    for jConstrainedRegion = 1:nConstrainedRegionSusceptibility
        for jRdf = 1:nRdf
            rdfSusceptibilityConstraint(:,:,:,jConstrainedRegion,jRdf) = real(ifftsave(fftsave(ConstraintMask.susceptibility(:,:,:,jConstrainedRegion)) .* dataArray(:,:,:,jRdf)));
        end
    end
end

% calculate RDF of constant susceptibility region (constraint)
if isTikhonovRegularizationSusceptibility
    if tikhonovSusceptibilityMatrixType
        if boolWithConstraintMaskSusceptibility
            gradientSusceptibilityConstraint = zeros([gridDimensionVector,nConstrainedRegionSusceptibility,nRdf]);
            for jConstrainedRegion = 1:nConstrainedRegionSusceptibility
                for jRdf = 1:nRdf
                    gradientSusceptibilityConstraint(:,:,:,jConstrainedRegion,jRdf) = Options.TikhonovRegularizationSusceptibility.parameter .* tikhonovMatrixSusceptibility .* real(ifftsave(tikhonovMatrixSusceptibilityFt .* dataArray(:,:,:,jRdf)));
                end
            end
        end
    end
end


% initialize preconditioner
if Options.boolPreconditioner
    preconditioner = generateunitdipoleresponse(gridDimensionVector,boolFourierDomain,Options,false);
    preconditionerLarge = logical(abs(preconditioner) > Options.preconditionerParameter);
    preconditioner(preconditionerLarge) = preconditioner(preconditionerLarge) ./ abs(preconditioner(preconditionerLarge)) * Options.preconditionerParameter;
    preconditioner(preconditioner == 0) = 1;
    preconditioner = 1./ preconditioner;
    computematrixvectorproductpreconditioner = @(inputVector, matrixTypeFlag)(computePrecondVectorProduct(inputVector, matrixTypeFlag));
    
end

% initialize replacement function
computematrixvectorproduct = @(inputVector, matrixTypeFlag)(computeMatrixVectorProduct(inputVector, matrixTypeFlag));


% generate anisotropy-matrix
if anIso
    nDegreeFreedomAnisotropy = sum(anisotropyMask(:));
    
    anisotropyShiftArray = zeros([gridDimensionVector,nRdf]);
    for jRdf=1:nRdf
        anisotropyShiftArray(:,:,:,jRdf) = -((cos(anisotropyAngleArray(:,:,:,jRdf))).^2-1/3);
        anisotropyShiftArray(:,:,:,jRdf) = anisotropyShiftArray(:,:,:,jRdf) .* anisotropyMask;
    end
end


% preprocessing for spatial weighting
if boolUseSpatialWeightingMatrix
    suscSpatialWeightingMatrix = zeros(size(problemMask));
    suscSpatialWeightingMatrix(logical(problemMask)) = spatialWeightingMatrix(logical(problemMask));
else
    suscSpatialWeightingMatrix = ones(size(problemMask));
end
clear spatialWeightingMatrix

%% main part (iterative solving)


gradientMaskReturn = [];
switch Options.solvingType
    
    case 'SpatialDomainL2'
        
        if isPreConeSuscSupplied
            resultVector = Options.PostProcCone.PreConeSuscSupplied(:);
        else
            
            tic
            %switch Options.algorithm
            %case 'LeastSquaresLsqr'
            rdfVectorGoodVoxels(1:nnz(problemMask)) = rdfVectorGoodVoxels(1:nnz(problemMask)).*suscSpatialWeightingMatrix(problemMask);
            rdfVectorGoodVoxels(isnan(rdfVectorGoodVoxels)) = 0;% don't understand why, but sometimes rdf has nans
            rdfVectorGoodVoxels(isinf(rdfVectorGoodVoxels)) = 0;
            if Options.boolPreconditioner
                [resultVector, IterationDetails.flag, IterationDetails.residualNorm, IterationDetails.nCycles, IterationDetails.residualVector] = lsqr(computematrixvectorproduct, rdfVectorGoodVoxels, Options.tolerance, Options.maxit,computematrixvectorproductpreconditioner);
            else
                [resultVector, IterationDetails.flag, IterationDetails.residualNorm, IterationDetails.nCycles, IterationDetails.residualVector] = lsqr(computematrixvectorproduct, rdfVectorGoodVoxels, Options.tolerance, Options.maxit);
            end
            %case 'LeastSquaresL1'
            %    [x,status]=l1_ls(A,At,numel(phase),numel(phase),rdfVectorGoodVoxels,lambda,rel_tol);
            %otherwise
            %    error('Choosen algorithm not supported')
            %end
            
            IterationDetails.computationTime = toc;
            indicateprogress(); %reset progress indicator
        end
        clear suscSpatialWeightingMatrix
    case 'SpatialDomainTV'
        
        computematrixvectorproduct_normal = @(inputVector)(computeMatrixVectorProduct_Normal(inputVector));
        computematrixvectorproduct_transpose = @(inputVector)(computeMatrixVectorProduct_Transpose(inputVector));
        
        La = 0.7629^2;    % largest singular value (squared) of A = largest value of dataArray     % correct for Residual Weighting?
        % this could be wrong! Shouldn't this be 2/3?
        %         La = max(abs(dataArray(:)));
        
        Opts.MaxIntIter = Options.SpatialDomainTV.maxitMu;
        Opts.maxiter = Options.SpatialDomainTV.maxitInner;
        Opts.TolVar = Options.SpatialDomainTV.tol;
        Opts.TypeMin = 'tv';
        
        Opts.GradientMask_x = GradientMask_x;
        Opts.GradientMask_y = GradientMask_y;
        Opts.GradientMask_z = GradientMask_z;
        gradientMaskReturn = GradientMask_x + GradientMask_y + GradientMask_z;
        
        if isResidualGradientWeighting
            Opts.GradientMaskResidual_x = GradientMaskResidual_x;
            Opts.GradientMaskResidual_y = GradientMaskResidual_y;
            Opts.GradientMaskResidual_z = GradientMaskResidual_z;
            Opts.ResidualGradientWeighting = Options.SpatialDomainTV.ResidualGradientWeighting;  % to pass parameter
        end
        
        Opts.gridDimensionVector = gridDimensionVector;
        
        
        TikhonovGradientFt_x = fftsave((generategradientmatrix1d(gridDimensionVector,'x'))/(Options.voxelAspectRatio(1)));  % voxelsize in meter
        TikhonovGradientFt_y = fftsave((generategradientmatrix1d(gridDimensionVector,'y'))/(Options.voxelAspectRatio(2)));
        TikhonovGradientFt_z = fftsave((generategradientmatrix1d(gridDimensionVector,'z'))/(Options.voxelAspectRatio(3)));
        %
        Opts.grad1 = TikhonovGradientFt_x;
        Opts.grad2 = TikhonovGradientFt_y;
        Opts.grad3 = TikhonovGradientFt_z;
        
        Opts.residualWeighting = Options.residualWeighting;
        Opts.isResidualGradientWeighting = isResidualGradientWeighting;
        
        Opts.nDegreeFreedomSuscept = nDegreeFreedomSuscept;
        Opts.nDegreeFreedomResidual = nDegreeFreedomResidual;
        Opts.problemMask = problemMask;
        
        %        Opts.Lmu1 = Options.SpatialDomainTV.Lmu1;
        Opts.stopTest = Options.SpatialDomainTV.stopTest;
        Opts.voxelAspectRatio = Options.voxelAspectRatio;
        Opts.tolvar = Options.SpatialDomainTV.tolerance;
        
        %rdfVectorGoodVoxels(1:nnz(problemMask)) = rdfVectorGoodVoxels(1:nnz(problemMask)).*suscSpatialWeightingMatrix(problemMask);
        rdfVectorGoodVoxels(isnan(rdfVectorGoodVoxels)) = 0;% don't understand why, but sometimes rdf has nans
        rdfVectorGoodVoxels(isinf(rdfVectorGoodVoxels)) = 0;
        
        
        boolUseSpatialWeightingMatrix = 0;
        tic
        [resultVector,~,~,~,~] = NESTA_UP_3d(computematrixvectorproduct_normal, computematrixvectorproduct_transpose, rdfVectorGoodVoxels, Options.SpatialDomainTV.lambda, La, Options.SpatialDomainTV.muMin, Opts);
        
        dispstatus(['Computation time: ',num2str(IterationDetails.computationTime),' seconds.']);
        Options.display = false;
        
    case 'InverseFiltering'
        
        tmp = zeros(gridDimensionVector);
        tmp(problemMask) = rdfVectorGoodVoxels(1:nProblemMaskVoxel);
        tmp = fftsave(tmp) ./ dataArray;
        tmp(isinf(tmp)) = 0;
        tmp(abs(dataArray) == 0) = 0;
        tmp = real(ifftsave(tmp));
        resultVector = reshape(tmp(1:nDegreeFreedomSuscept), [nDegreeFreedomSuscept,1]);
        
end
clear tmp

if Options.offsetUseBool
    IterationDetails.globalOffset = zeros(nRdf,1);
    for jRdf = 1:nRdfUsed
        IterationDetails.globalOffset(jRdf) = resultVector(nDegreeFreedomSuscept+nDegreeFreedomResidual+jRdf);
        display(['Global offset >',num2str(jRdf),'< was set to ',num2str( IterationDetails.globalOffset(jRdf)),'.']);
    end
else
    IterationDetails.globalOffset = 0;
end






%% display iteration details

if Options.display
    if IterationDetails.flag == 0
        dispstatus('Algorithm converged to the desired tolerance.')
    elseif IterationDetails.flag == 1
        dispstatus('Algorithm did not converge to the desired tolerance within specified maximum number of iterations.')
    else
        dispstatus('Algorithm error. Please check iteration details.')
    end
    
    dispstatus(['Normalized Residualnorm: ',num2str(IterationDetails.residualNorm),'.']);
    dispstatus(['Number of Iterations: ',num2str(IterationDetails.nCycles),'.']);
    dispstatus(['Computation time: ',num2str(IterationDetails.computationTime),' seconds.']);
    
    figure; plot(IterationDetails.residualVector ./ (norm(rdfVectorGoodVoxels)/nRdfUsed) );
    xlabel('cycle number')
    ylabel('normalized residual norm')
end
clear rdfVectorGoodVoxels


if isTikhonovRegularizationSusceptibility
    IterationDetails.GradientMask_x = GradientMask_x;
    IterationDetails.GradientMask_y = GradientMask_y;
    IterationDetails.GradientMask_z = GradientMask_z;
end
%% postprocess results



clear phaseNoisePattern
% get susceptibility distribution
if boolWithConstraintMaskSusceptibility
    dataArray = zeros(gridDimensionVector);
    dataArray(freeRegion) = resultVector(1:nDegreeFreedomSuscept-nConstrainedRegionSusceptibility);
    
    for jConstrainedRegion = 1:nConstrainedRegionSusceptibility
        if Options.useConstantConstraints
            dataArray(ConstraintMask.susceptibility(:,:,:,jConstrainedRegion)) = resultVector(nDegreeFreedomSuscept-nConstrainedRegionSusceptibility+jConstrainedRegion);
        else
            dataArray(ConstraintMask.susceptibility(:,:,:,jConstrainedRegion)) = dataArray(ConstraintMask.susceptibility(:,:,:,jConstrainedRegion)) + resultVector(nDegreeFreedomSuscept-nConstrainedRegionSusceptibility+jConstrainedRegion);
        end
    end
    %dataArray(~ConstraintMask.susceptibility) = resultVector(1:nDegreeFreedomSuscept-1);
else
    dataArray = reshape(resultVector(1:nDegreeFreedomSuscept), gridDimensionVector);
end
clear freeRegion

% get residual vector
if Options.residualWeighting && nargin > 2
    residualArray = zeros(gridDimensionVector);
    %residualArray(problemMask) = resultVector(nDegreeFreedomSuscept+1:numel(resultVector)-offsetFreedom);
    
    if boolWithConstraintMaskResidual
        if nVoxelFreeRegionResidual
            residualArray(freeRegionResidual) = resultVector(nDegreeFreedomSuscept+1:nDegreeFreedomSuscept+nVoxelFreeRegionResidual);
        end
        
        for jConstrainedRegion = 1:nConstrainedRegionResidual
            residualArray(ConstraintMask.residual(:,:,:,jConstrainedRegion)) = resultVector(nDegreeFreedomSuscept+nVoxelFreeRegionResidual+jConstrainedRegion);
        end
        
    else
        residualArray(problemMaskOne) = resultVector(nDegreeFreedomSuscept+1:nDegreeFreedomSuscept+nProblemMaskVoxelOne);
    end
    
    
    
    residualArray = residualArray .* Options.residualWeighting;
else
    residualArray = [];
end

if anIso
    anisotropyComponent = zeros(gridDimensionVector);
    anisotropyComponent(anisotropyMask) = resultVector(nDegreeFreedomSuscept+nDegreeFreedomResidual+1:nDegreeFreedomSuscept+nDegreeFreedomResidual+nDegreeFreedomAnisotropy);
else
    anisotropyComponent = [];
end
clear resultVector






ConeMask = false(gridDimensionVector);
if nargout > 6
    dataArrayPreConeProc = false(gridDimensionVector);
end

if nargout > 9 && ~manyFieldsBool
    dipole = generateunitdipoleresponse(gridDimensionVector, boolFourierDomain,Options,true);
    phaseDiscrepancy = real(ifftsave(fftsave(dataArray) .* dipole)) - rdf;
end


%% member-functions


    function outputVector = computeMatrixVectorProduct_Normal(inputVector)
        
        outputVector = computeMatrixVectorProduct(inputVector, 'normal');
    end

    function outputVector = computeMatrixVectorProduct_Transpose(inputVector)
        
        outputVector = computeMatrixVectorProduct(inputVector, 'transp');
    end




% function that replaces standard matrix-vector product and transpose
% matrix-vector product
    function outputVector = computeMatrixVectorProduct(inputVector, matrixTypeFlag)
        
        %%%%%%%%%%%%%%%%%%%%%
        % transpose matrix
        %%%%%%%%%%%%%%%%%%%%%
        if (strcmp(matrixTypeFlag,'transp'))
            
            %tmp = zeros(gridDimensionVector);
            tmp = zeros([gridDimensionVector,nRdfUsed]);
            %tmp(problemMask) = inputVector;
            
            
            
            
            % construct nRdf 3-D arrays from input vector
            %tmp2 = zeros(gridDimensionVector);
            %             for kRdf = 1:nRdfUsed
            %                 tmp2(problemMask(:,:,:,kRdf)) = inputVector((kRdf-1)*nProblemMaskVoxel+1:nProblemMaskVoxel*kRdf);
            %                 tmp(:,:,:,kRdf) = tmp2;
            %             end
            tmp(problemMask) = inputVector(1:nProblemMaskVoxel);
            
            
            if boolUseSpatialWeightingMatrix
                %tmp(problemMask) = tmp(problemMask) .* suscSpatialWeightingMatrix(problemMask);
                %                 for kRdf = 1:nRdfUsed
                %                     tmp2 = tmp(:,:,:,kRdf);
                %                     tmp2(problemMask) = tmp2(problemMask) .* suscSpatialWeightingMatrix(problemMask);
                %                     tmp(:,:,:,kRdf) = tmp2;
                %                 end
                tmp(problemMask) = tmp(problemMask) .* suscSpatialWeightingMatrix(problemMask);
                
            end
            
            
            
            if Options.offsetUseBool
                tmpOffset = zeros(nRdfUsed,1);
                %tmpOffset = sum(tmp(:));
                for kRdf = 1:nRdfUsed
                    tmp2 = tmp(:,:,:,kRdf);
                    tmpOffset(kRdf) = sum(tmp2(problemMask(:,:,:,kRdf))); % sum must be calculated only over the voxels inside the brain, because field is masked in forward direction
                end
            end
            
            
            
            if boolWithConstraintMaskSusceptibility
                % With susceptibility constraints
                
                % compute actual matrix-vector product
                %                 outputVector = real(ifftsave(fftsave(tmp) .* dataArray));
                %                 outputVector = outputVector(freeRegion);
                %
                outputVector = zeros(nDegreeFreedomSuscept-nConstrainedRegionSusceptibility,1);
                
                % multiplication of RDF-vectors with transpose matrix
                if nVoxelFreeRegion
                    for kRdf = 1:nRdfUsed
                        tmp2 = real(ifftsave(fftsave(tmp(:,:,:,kRdf)) .* dataArray(:,:,:,kRdf)));
                        outputVector(1:nVoxelFreeRegion) = outputVector(1:nVoxelFreeRegion) + tmp2(freeRegion);
                    end
                end
                
                %
                for kConstrainedRegion = 1:nConstrainedRegionSusceptibility
                    
                    %outputVector = cat(1,outputVector,tmp(problemMask).' * inputVector);
                    
                    tmp2 = 0;
                    for kRdf = 1:nRdfUsed
                        tmp = rdfSusceptibilityConstraint(:,:,:,kConstrainedRegion,kRdf);
                        if boolUseSpatialWeightingMatrix
                            tmp(problemMask) = tmp(problemMask) .* suscSpatialWeightingMatrix(problemMask);
                        end
                        tmp2 = tmp2 + tmp(problemMask(:,:,:,kRdf)).' * inputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf)));
                    end
                    outputVector = cat(1,outputVector,tmp2);
                end
                
            else
                % without susceptibility constraints
                
                outputVector = zeros(nDegreeFreedomSuscept,1);
                
                %outputVector = reshape(real(ifftsave(fftsave(tmp) .* dataArray)), [nDegreeFreedomSuscept, 1]);
                
                for kRdf = 1:nRdfUsed
                    tmp2 = reshape(real(ifftsave(fftsave(tmp(:,:,:,kRdf)) .* dataArray(:,:,:,kRdf))), [nDegreeFreedomSuscept, 1]);
                    if isfield(Options,'susceptibilityWeightingMatrix')
                        tmp2 = tmp2 .* reshape(Options.susceptibilityWeightingMatrix, [nDegreeFreedomSuscept, 1]);
                    end
                    outputVector = outputVector + tmp2;
                end
            end
            
            
            % compute Regularization term
            
            if isPowerRegularization
                tmp2 = zeros(gridDimensionVector);
                tmp2(problemMask) = inputVector(nProblemMaskVoxel+1:nProblemMaskVoxel+nProblemMaskVoxelOne);
                PowerRegularizationTerm = Options.PowerRegularization.parameter .* PowerTermWeightingMatrix .* tmp2;
                outputVector = outputVector + PowerRegularizationTerm(freeRegion);
            end
            
            
            
            
            % compute tikhonov term for susceptibility
            if isTikhonovRegularizationSusceptibility
                tmp2_X = zeros(gridDimensionVector);
                tmp2_Y = zeros(gridDimensionVector);
                tmp2_Z = zeros(gridDimensionVector);
                tmp2_X(tikhonovVoxelMask_x) = inputVector(nProblemMaskVoxel+1:nProblemMaskVoxel+nTikhonovTermSusceptibilityVoxel);
                tmp2_Y(tikhonovVoxelMask_y) = inputVector(nProblemMaskVoxel+nTikhonovTermSusceptibilityVoxel+1:nProblemMaskVoxel+2*nTikhonovTermSusceptibilityVoxel);
                tmp2_Z(tikhonovVoxelMask_z) = inputVector(nProblemMaskVoxel+2*nTikhonovTermSusceptibilityVoxel+1:nProblemMaskVoxel+3*nTikhonovTermSusceptibilityVoxel);
                
                switch tikhonovSusceptibilityMatrixType
                    case 'fourier'
                        tikhonovTerm = Options.TikhonovRegularizationSusceptibility.parameter .* real(ifftsave(tikhonovMatrixSusceptibilityFt .* fftsave(tmp2)));
                    case 'spatial'
                        tikhonovTerm = Options.TikhonovRegularizationSusceptibility.parameter .* tikhonovMatrixSusceptibility .* tmp2;
                    case 'mixed'
                        %                         tmp2=fftsave(tikhonovMatrixSusceptibility .* tmp2);
                        tikhonovTermX = Options.TikhonovRegularizationSusceptibility.parameter .* ( discretegradient(GradientMask_x .* tmp2_X,'x',Options.voxelAspectRatio(1),Options.gradientType,true));
                        tikhonovTermY = Options.TikhonovRegularizationSusceptibility.parameter .* ( discretegradient(GradientMask_y .* tmp2_Y,'y',Options.voxelAspectRatio(2),Options.gradientType,true));
                        tikhonovTermZ = Options.TikhonovRegularizationSusceptibility.parameter .* ( discretegradient(GradientMask_z .* tmp2_Z,'z',Options.voxelAspectRatio(3),Options.gradientType,true));
                        
                        tikhonovTerm = tikhonovTermX + tikhonovTermY + tikhonovTermZ;
                end
                
                if boolWithConstraintMaskSusceptibility
                    tmp3 = tikhonovTerm(freeRegion);
                    for kConstrainedRegion = 1:nConstrainedRegionSusceptibility
                        
                        %outputVector = cat(1,outputVector,tmp(problemMask).' * inputVector);
                        
                        tmp2 = 0;
                        for kRdf = 1:nRdfUsed
                            tmp = rdfSusceptibilityConstraint(:,:,:,kConstrainedRegion,kRdf);
                            tmp2 = tmp2 + tmp(problemMask(:,:,:,kRdf)).' * inputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf)));
                        end
                        tmp3 = cat(1,tmp3,tmp2);
                    end
                    outputVector = outputVector + tmp3;
                else
                    outputVector = outputVector + tikhonovTerm(freeRegion);
                end
            end
            
            
            %outputVector = cat(1,outputVector,inputVector .* Options.residualWeighting);
            % residual weighting part
            if Options.residualWeighting
                
                
                %tmp2 = zeros(nProblemMaskVoxel,1);
                tmp2 = zeros(nVoxelFreeRegionResidual,1);
                
                % tmp3 = zeros([gridDimensionVector nRdfUsed]);
                for kRdf = 1:nRdfUsed
                    tmp3 = zeros(gridDimensionVector);
                    if boolUseSpatialWeightingMatrix
                        tmp3(problemMask(:,:,:,kRdf)) = inputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) .* Options.residualWeighting .* suscSpatialWeightingMatrix(problemMask(:,:,:,kRdf));
                    else
                        tmp3(problemMask(:,:,:,kRdf)) = inputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) .* Options.residualWeighting;
                    end
                    tmp2 = tmp2 + tmp3(freeRegionResidual);
                    %tmp2 = tmp2 + inputVector((kRdf-1)*nProblemMaskVoxel+1:nProblemMaskVoxel*kRdf) .* Options.residualWeighting;
                end
                
                
                
                if boolWithConstraintMaskResidual
                    %outputVector = cat(1,outputVector,tmp2 .* freeRegionResidual(problemMask));
                    %outputVector = cat(1,outputVector,tmp2(freeRegionResidual(problemMask)));
                    outputVector = cat(1,outputVector,tmp2);
                    
                    for kConstrainedRegion = 1:nConstrainedRegionResidual
                        if boolUseSpatialWeightingMatrix
                            tmp2 = ConstraintMask.residual(:,:,:,kConstrainedRegion) .* suscSpatialWeightingMatrix;
                        else
                            tmp2 = ConstraintMask.residual(:,:,:,kConstrainedRegion);
                        end
                        tmp3 = 0;
                        for kRdf = 1:nRdfUsed
                            tmp3 = tmp3 + inputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf)))' * tmp2(problemMaskOne);
                        end
                        outputVector = cat(1,outputVector,tmp3 .* Options.residualWeighting);
                    end
                else
                    
                    outputVector = cat(1,outputVector,tmp2);
                    
                end
                
                
            end
            
            
            % anisotropy part
            if anIso
                tmp4 = zeros(nDegreeFreedomAnisotropy,1);
                tmp5 = zeros(gridDimensionVector);
                
                for kRdf = 1:nRdfUsed
                    tmp6 = anisotropyShiftArray(:,:,:,kRdf);
                    tmp5(problemMask(:,:,:,kRdf)) = inputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) .* tmp6(problemMask(:,:,:,kRdf));
                    tmp4 = tmp4 + tmp5(anisotropyMask);
                end
                outputVector = cat(1,outputVector,tmp4);
            end
            
            
            
            
            
            
            % global-offset part
            if Options.offsetUseBool
                %outputVector = cat(1,outputVector,tmpOffset);
                for kRdf = 1:nRdfUsed
                    outputVector = cat(1,outputVector,tmpOffset(kRdf));
                end
            end;
            
            
            
            
            
        else
            %%%%%%%%%%%%%%%%%%%%%
            % normal matrix
            %%%%%%%%%%%%%%%%%%%%%
            
            
            
            tmp = zeros([gridDimensionVector nRdfUsed]);
            if boolWithConstraintMaskSusceptibility
                % With susceptibility constraints
                
                % construct susceptibility array from vector
                tmp2 = zeros(gridDimensionVector);
                if nVoxelFreeRegion
                    tmp2(freeRegion) = inputVector(1:nDegreeFreedomSuscept-nConstrainedRegionSusceptibility);
                end
                
                for kConstrainedRegion = 1:nConstrainedRegionSusceptibility
                    if Options.useConstantConstraints
                        tmp2(ConstraintMask.susceptibility(:,:,:,kConstrainedRegion))  = inputVector(nDegreeFreedomSuscept-nConstrainedRegionSusceptibility+kConstrainedRegion);
                    else
                        tmp2(ConstraintMask.susceptibility(:,:,:,kConstrainedRegion))  = tmp2(ConstraintMask.susceptibility(:,:,:,kConstrainedRegion)) + inputVector(nDegreeFreedomSuscept-nConstrainedRegionSusceptibility+kConstrainedRegion);
                    end
                    % potential improvement! Precalculate the field!
                end
                
                if isfield(Options,'susceptibilityWeightingMatrix')
                    tmp2 = tmp2 .* Options.susceptibilityWeightingMatrix;
                end
                
                for kRdf = 1:nRdfUsed
                    % compute RDFs
                    tmp(:,:,:,kRdf) = real(ifftsave(fftsave(tmp2).* dataArray(:,:,:,kRdf)));
                end
                
                % compute RDFs
                %tmp = real(ifftsave(fftsave(tmp2).* dataArray));
                
                % compute regularization (Power) term
                if isPowerRegularization
                    tmp0 = inputVector(1:nDegreeFreedomSuscept-nConstrainedRegionSusceptibility);
                    PowerRegularizationTerm = Options.PowerRegularization.parameter .*tmp0  ;
                    PowerRegularizationTerm = PowerRegularizationTerm(problemMaskOne);
                    PowerRegularizationTerm = PowerRegularizationTerm .* PowerTermWeightingMatrix(problemMaskOne);
                end
                
                % compute tikhonov term
                if isTikhonovRegularizationSusceptibility
                    switch tikhonovSusceptibilityMatrixType
                        case 'fourier'
                            tikhonovTerm = Options.TikhonovRegularizationSusceptibility.parameter .* real(ifftsave(tikhonovMatrixSusceptibilityFt .* fftsave(tmp2)));
                            tikhonovTerm = tikhonovTerm(problemMaskOne);
                        case 'spatial'
                            tikhonovTerm = Options.TikhonovRegularizationSusceptibility.parameter .* GradientMask .* tmp2;
                            tikhonovTerm = tikhonovTerm(problemMaskOne);
                        case 'mixed'
                            tikhonovTermX = Options.TikhonovRegularizationSusceptibility.parameter .* (  GradientMask_x .* discretegradient(tmp2,'x',Options.voxelAspectRatio(1),Options.gradientType));
                            tikhonovTermY = Options.TikhonovRegularizationSusceptibility.parameter .* (  GradientMask_y .* discretegradient(tmp2,'y',Options.voxelAspectRatio(2),Options.gradientType));
                            tikhonovTermZ = Options.TikhonovRegularizationSusceptibility.parameter .* (  GradientMask_z .* discretegradient(tmp2,'z',Options.voxelAspectRatio(3),Options.gradientType));
                            
                            tikhonovTermX = tikhonovTermX(tikhonovVoxelMask_x);
                            tikhonovTermY = tikhonovTermY(tikhonovVoxelMask_y);
                            tikhonovTermZ = tikhonovTermZ(tikhonovVoxelMask_z);
                    end
                end
                
                
            else
                % without susceptibility constraints
                
                % construct susceptibility array from vector
                tmp0Spatial = reshape(inputVector(1:nDegreeFreedomSuscept), gridDimensionVector);
                tmp0 = fftsave(tmp0Spatial);
                
                % compute RDFs
                for kRdf = 1:nRdfUsed
                    tmp(:,:,:,kRdf) = real(ifftsave(tmp0.* dataArray(:,:,:,kRdf)));
                end
                
                % compute tikhonov term
                if isTikhonovRegularizationSusceptibility
                    switch tikhonovSusceptibilityMatrixType
                        case 'fourier'
                            tikhonovTerm = Options.TikhonovRegularizationSusceptibility.parameter .* real(ifftsave(tikhonovMatrixSusceptibilityFt .* tmp0));
                            tikhonovTerm = tikhonovTerm(problemMaskOne);
                        case 'spatial'
                            tikhonovTerm = Options.TikhonovRegularizationSusceptibility.parameter .* tikhonovMatrixSusceptibility .* reshape(inputVector(1:nDegreeFreedomSuscept), gridDimensionVector);
                            tikhonovTerm = tikhonovTerm(problemMaskOne);
                        case 'mixed'
                            tikhonovTermX = Options.TikhonovRegularizationSusceptibility.parameter .* (  GradientMask_x .* discretegradient(tmp0Spatial,'x',Options.voxelAspectRatio(1),Options.gradientType));
                            tikhonovTermY = Options.TikhonovRegularizationSusceptibility.parameter .* (  GradientMask_y .* discretegradient(tmp0Spatial,'y',Options.voxelAspectRatio(2),Options.gradientType));
                            tikhonovTermZ = Options.TikhonovRegularizationSusceptibility.parameter .* (  GradientMask_z .* discretegradient(tmp0Spatial,'z',Options.voxelAspectRatio(3),Options.gradientType));
                            
                            tikhonovTermX = tikhonovTermX(tikhonovVoxelMask_x);
                            tikhonovTermY = tikhonovTermY(tikhonovVoxelMask_y);
                            tikhonovTermZ = tikhonovTermZ(tikhonovVoxelMask_z);
                    end
                end
                
                
                % compute regularization (Power) term
                if isPowerRegularization
                    tmp0 = inputVector(1:nDegreeFreedomSuscept);
                    PowerRegularizationTerm = Options.PowerRegularization.parameter .*tmp0;
                    PowerRegularizationTerm = PowerRegularizationTerm(problemMaskOne);
                    PowerRegularizationTerm = PowerRegularizationTerm .* PowerTermWeightingMatrix(problemMaskOne);
                end
                
                
            end
            
            
            
            % Add global offset to RDF
            if Options.offsetUseBool
                for kRdf = 1:nRdfUsed
                    tmp(:,:,:,kRdf) = tmp(:,:,:,kRdf) + inputVector(numel(inputVector)-nRdfUsed+kRdf);
                end
                %tmp = tmp + inputVector(numel(inputVector));
            end
            
            
            % update timer
            IterationDetails.computationTime = toc;
            jIterationCycle = jIterationCycle + 1;
            if Options.display
                remainingTime = round(10*((Options.maxit - (jIterationCycle-1)) * (IterationDetails.computationTime/(jIterationCycle-1)) / 60 / 60))/10;
                indicateprogress([num2str(jIterationCycle-1),'. cycle, current computation time: ',num2str(round(IterationDetails.computationTime)),'s (speed: ',num2str(round(IterationDetails.computationTime/(jIterationCycle-1))),' s/cycle, max. remain: ',num2str(remainingTime),' h)'])
            end
            
            %outputVector =                tmp(problemMask);
            %outputVector = outputVector +
            %inputVector(nDegreeFreedomSuscept+1:nDegreeFreedomSuscept+nProblemMaskVoxel)
            %.* Options.residualWeighting;
            
            
            % initialize output vector (matrix-vector product)
            %outputVector = zeros(nRdfUsed .* nProblemMaskVoxel + nTikhonovTermSusceptibilityVoxel,1);
            
            
            outputVector = zeros(nProblemMaskVoxel + nPowerRegularizationVoxel + 3*nTikhonovTermSusceptibilityVoxel,1);
            
            
            % add regularization term
            if isPowerRegularization
                outputVector(nProblemMaskVoxel+1:nProblemMaskVoxel+nPowerRegularizationVoxel) = PowerRegularizationTerm;
            end
            
            
            % add thikonov-term of susceptibility
            if isTikhonovRegularizationSusceptibility
                outputVector(nProblemMaskVoxel+nPowerRegularizationVoxel+1:nProblemMaskVoxel+nPowerRegularizationVoxel+nTikhonovTermSusceptibilityVoxel) = tikhonovTermX;
                outputVector(nProblemMaskVoxel+nPowerRegularizationVoxel+nTikhonovTermSusceptibilityVoxel+1:nProblemMaskVoxel+nPowerRegularizationVoxel+2*nTikhonovTermSusceptibilityVoxel) = tikhonovTermY;
                outputVector(nProblemMaskVoxel+nPowerRegularizationVoxel+(2*nTikhonovTermSusceptibilityVoxel)+1:nProblemMaskVoxel+nPowerRegularizationVoxel+3*nTikhonovTermSusceptibilityVoxel) = tikhonovTermZ;
            end
            
            
            outputVector(1:nProblemMaskVoxel) = tmp(problemMask);
            
            
            for kRdf = 1:nRdfUsed
                % select voxels of RDF
                %dummy = tmp(:,:,:,kRdf);
                
                % concatenate RDF voxels in output vector
                %outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) = dummy(problemMask(:,:,:,jRdf));
                
                % add residual
                if boolWithConstraintMaskResidual
                    
                    % add free residual voxels to RDF
                    if nVoxelFreeRegionResidual
                        %outputVector((kRdf-1)*nProblemMaskVoxel+1:nProblemMaskVoxel*kRdf) = outputVector((kRdf-1)*nProblemMaskVoxel+1:nProblemMaskVoxel*kRdf) + inputVector(nDegreeFreedomSuscept+1:nDegreeFreedomSuscept+nVoxelFreeRegionResidual) .* (freeRegionResidual & problemMask) .* Options.residualWeighting;
                        tmp = zeros(gridDimensionVector);
                        tmp(problemMask(:,:,:,kRdf)) = outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf)));
                        tmp(freeRegionResidual) = tmp(freeRegionResidual) + inputVector(nDegreeFreedomSuscept+1:nDegreeFreedomSuscept+nVoxelFreeRegionResidual) .* Options.residualWeighting;
                        outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) = tmp(problemMask(:,:,:,kRdf));
                    end
                    
                    % add constrained residual regions to RDF
                    tmp2 = zeros(gridDimensionVector);
                    for kConstrainedRegion = 1:nConstrainedRegionResidual
                        tmp2(ConstraintMask.residual(:,:,:,kConstrainedRegion))  = inputVector(nDegreeFreedomSuscept+nVoxelFreeRegionResidual+kConstrainedRegion);
                    end
                    outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) = outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) + tmp2(problemMask(:,:,:,kRdf)) .* Options.residualWeighting;
                    
                    
                    
                else
                    if Options.residualWeighting
                        tmpX = zeros(gridDimensionVector);
                        tmpX(problemMaskOne) = inputVector(nDegreeFreedomSuscept+1:nDegreeFreedomSuscept+nDegreeFreedomResidual) .* Options.residualWeighting;
                        outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) = outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) ...
                            + tmpX(problemMask(:,:,:,kRdf));
                    end
                    
                end
                
            end
            
            
            
            
            if boolUseSpatialWeightingMatrix
                %outputVector = outputVector .* suscSpatialWeightingMatrix(problemMask);
                for kRdf = 1:nRdfUsed
                    tmpWeight = suscSpatialWeightingMatrix(:,:,:,kRdf);
                    outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) = outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) .* tmpWeight(problemMask(:,:,:,kRdf));
                end
            end
            
            
            % anisotropy part
            if anIso
                tmp3 = zeros(gridDimensionVector);
                tmp3(anisotropyMask) = inputVector(nDegreeFreedomSuscept+nDegreeFreedomResidual+1:nDegreeFreedomSuscept+nDegreeFreedomResidual+nDegreeFreedomAnisotropy);
                
                for kRdf = 1:nRdfUsed
                    tmp2 = anisotropyShiftArray(:,:,:,kRdf);
                    outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) = outputVector(sum(nProblemMaskVoxelIndividual(1:kRdf-1))+1:sum(nProblemMaskVoxelIndividual(1:kRdf))) + tmp3(problemMask(:,:,:,kRdf)) .* tmp2(problemMask(:,:,:,kRdf));
                end
            end
            
        end
    end




% function that replaces preconditioning matrix-vector product and transpose
% preconditioning matrix-vector product
%
% note: this function uses inplace-call (input variable = output
% variable)
    function inputVector = computePrecondVectorProduct(inputVector, matrixTypeFlag)
        
        chemShift = inputVector(nDegreeFreedomSuscept+1:nDegreeFreedomSuscept+nProblemMaskVoxel);
        inputVector = reshape(inputVector(1:nDegreeFreedomSuscept), gridDimensionVector);
        
        %%%%%%%%%%%%%%%%%%%%%
        % transpose matrix
        %%%%%%%%%%%%%%%%%%%%%
        if (strcmp(matrixTypeFlag,'transp'))
            if (~Options.offsetUseBool)
                inputVector =        real(ifftsave(fftsave(inputVector) .* preconditioner));
            end;
            
            
            %%%%%%%%%%%%%%%%%%%%%
            % normal matrix
            %%%%%%%%%%%%%%%%%%%%%
        else
            if (~Options.offsetUseBool)
                inputVector = real(ifftsave(fftsave(inputVector).* preconditioner));
            end
            t=toc;
            
            display(sprintf('Precond step in %7.3fs', t));
            
        end
        inputVector = reshape(inputVector, [nDegreeFreedomSuscept, 1]);
        inputVector  = cat(1,inputVector,chemShift);
    end





end
