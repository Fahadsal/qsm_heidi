function [inputArray,cropVector] = makeodd(inputArray,unDoVector,dimensions)

%MAKEODD Makes size of array odd.
%
%   Syntax
%
%   MAKEODD(A)
%   [x,u] = MAKEODD(A)
%   MAKEODD(A,u)
%
%
%
%   Description
%
%   MAKEODD(A) croppes rows/columns from 4/3/2-D array A so that size of A
%   becomes odd.
%
%   [x,u] = MAKEODD(A) also returns an vector u that contains information
%   for un-doing this function.
%
%   MAKEODD(A,u) un-does the operation based on information in u. Appended
%   rows/colums will be filled with zeros.
%
%   MAKEODD(A,[],dims) applies operation only on dimensions specfied in
%   dims vector, e.g. [1 1 1 0], i.e. only 3 first dimensions.
%
%
%   See also: MAKEEVEN

%
%   2009/07/8 F Schweser, mail@ferdinand-schweser.de
%
%   v1.1 - 2009/11/06 - F Schweser - UnDo-Feature added
%   v1.2 - 2010/07/18 - B Lehr     - Added support for 2D objects
%   v1.2 - 2011/09/15 - F Schweser - Added support for 4D objects
%   v1.3 - 2013/07/17 - F Schweser - New feature: dims


if nargin == 2 && ~isempty(unDoVector)
    
    % un-do
    
    outputArray = zeros(size(inputArray)+abs(unDoVector-1));
    
    outputArray(1:size(inputArray,1),1:size(inputArray,2),1:size(inputArray,3),1:size(inputArray,4)) = inputArray;
    
    
    if ~unDoVector(1)
        outputArray(size(inputArray,1)+1,:,:,:) = 0;
    end
    if ~unDoVector(2)
        outputArray(:,size(inputArray,2)+1,:,:) = 0;
    end
    if ndims(unDoVector) > 2
        if ~unDoVector(3)
            outputArray(:,:,size(inputArray,3)+1,:) = 0;
        end
        if ndims(unDoVector) > 3
            if ~unDoVector(4)
                outputArray(:,:,:,size(inputArray,4)+1) = 0;
            end
        end
    end    
    
    inputArray = outputArray;
    
else
    
    
    % even --> odd
    cropVector = mod(size(inputArray),2);
    
    if nargin > 2 && ~isempty(dimensions)
        cropVector(~logical(dimensions)) = 1;
    end
    
    if ~cropVector(1)
        inputArray = inputArray(1:size(inputArray,1)-1,:,:,:);
    end
    if ~cropVector(2)
        inputArray = inputArray(:,1:size(inputArray,2)-1,:,:);
    end
    
    if ndims(inputArray) > 2
        if ~cropVector(3)
            inputArray = inputArray(:,:,1:size(inputArray,3)-1,:);
        end
        if ndims(inputArray) > 3
            if ~cropVector(4)
                inputArray = inputArray(:,:,:,1:size(inputArray,4)-1);
            end
        end
    end    
    
end



end
