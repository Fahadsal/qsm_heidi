function retVal = isproperty(obj,propertyName)


%ISPROPERTY True if property is in object.
%   ISPROPERTY(O,PROPERTY) returns true if the string FIELD is the name of a
%   property in the object O.
%
%
%
%   See also ISFIELD. 

% F Schweser, mail@ferdinand-schweser.de, 2010/09/06
%
%   Changelog:
%   
%   2010/09/06 - v1.0 - F Schweser - new

retVal = any(strcmp(properties(obj),propertyName));
