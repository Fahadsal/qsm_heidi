function retVal = myisfield(inStruct, fieldName)

%MYISFIELD Checks if field exists in a structure.
%
%   MYISFIELD Checks if field in structure exists.
%
%   
%   Syntax
%
%   MYISFIELD(A,b)
%
%
%   Description
%
%   MYISFIELD(A,b) is true if field b (string) does exist in structure A.
%
%
%   Example
%
%   TestStructure.testField = 15;
%   myisfield(TestStructure,'testField')
%   myisfield(TestStructure,'testField2')
%
%   The fist command returns true, the second command returns false.
%
 

% Was adapted from The MATHWORKS Technical Solutions Website.
%
% 2009/05/04 F Schweser, mail@ferdinand-schweser.de
% 2010/08/12 A Deistung - BugFix for empty struct 


retVal = 0;
if isempty(inStruct)
      return;
end

f = fieldnames(inStruct(1));

for i=1:length(f)
  if (strcmp(f(i),strtrim(fieldName))) % changed by FS
      retVal = 1;
      return;
  elseif isstruct(inStruct(1).(f{i}))
      retVal = myisfield(inStruct(1).(f{i}), fieldName);
      if retVal
          return;
      end
  end
end



end