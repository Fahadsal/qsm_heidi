function [ outputstring ] = makeunixstring( inputstring )
%MAKEUNIXSTRING replacement of special characters in strings
%   
%[ outputfilename ] = makeunixfilename( inputstring )
%
% Purpose: This function replaces special characters in strings that shall be
%          executed with the function "unix" at the unix shell. 
%          Currently, the following special cases '(', ')', ' ' are replaced
%          by '\(', '\)', and '\ '. 
%
% Input:  inputstring     - string that should be modified.
%
% Output: outputstring    - modidified string
%
% This function was written by Andreas Deistung
% (andreas.deistung@med.uni-jena.de). 
%
% ChangeLog:
% v 1.0  - 20110208 - A Deistung    - initial version
% v 1.1  - 20120910 - A Deistung    - BugFix: ' '
%
%% Define your symbols that should be replaced by \symbol
replacementsymbols = char('(', ')', ' ');


%% replace the symbols
outputstring = inputstring;
for i = 1:size(replacementsymbols, 1)
    outputstring = strrep(outputstring, replacementsymbols(i,:), ['\', replacementsymbols(i,:)]);
end

end

