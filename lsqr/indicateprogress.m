function indicateprogress(message)
% INDICATEPROGRESS displays progress message in the MATLAB
% command window. The message is displayed as text that can be updated with
% subsequent calls.
%
%   Syntax
%
%   INDICATEPROGRESS()
%   INDICATEPROGRESS(message)
%
%
%   Description
%
%   INDICATEPROGRESS(message) shows/overwrites current message in command
%   window. message is a string.
%
%   INDICATEPROGRESS() resets the function.
%
%
%   See also: WAITBAR

%   F Schweser, 2009/09/01, mail@ferdinand-schweser.de
%   Function was adapted from PROGMETER (Matlab File Exchange)
%
%   Changelog:
%
%   v1.2 - F Schweser - 2009/12/17 - output of "s" omitted.
%   v1.1 - F Schweser - 2009/12/01 - Reset added.

persistent clearlen messlen

if nargin < 2
    x = [];
end

if nargin == 1

    
    if isempty(messlen)
        messlen = 0;
        clearlen = 0;
    end

    % Remove previous meter text
    if clearlen ~= 0 && ~(ischar(x) && strncmpi(x,'r',1)) % dont erase if a 'reset' is issued
        fprintf(repmat('\b', 1, clearlen));
    end

    %if nargin > 1 % A new message has been input
    if messlen > 0
        % erase previous message
        fprintf(repmat('\b', 1, messlen));
    end
    fprintf('%s  ',message);
    %fprintf('%ss ',message);
    messlen = length(message) + 2;
    %end

    if ischar(x) && ~isempty(x) % reset, clear or done
        switch lower(x(1))
            case 'c'
                fprintf(repmat('\b', 1, messlen));
            case 'd'
                fprintf('Done\n');
            case 'r'
                fprintf('\n');
            otherwise
                error('The second input must be a numeric scalar between 0 and 1 or the strings ''done'', ''clear'' or ''reset''');
        end
        clear clearlen
        clear messlen
    elseif isnumeric(x) && ~isempty(x)
        progress = int2str(round(x*100));
        clearlen = length(progress) + 1;
        fprintf('%s%%', progress);
    else
        progress = '';
        clearlen =0;
        fprintf('%s%', progress);
        %else

        %error('The first input must be a numeric scalar between 0 and 1 or the strings ''done'', ''clear'' or ''reset''');
    end
    
else
    clear messlen;
    clear clearlen;

end

end
