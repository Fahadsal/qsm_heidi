function [inputArrayObj,appendVector] = makeeven(inputArrayObj,paddingValue,unDoVector)

%MAKEEVENY Makes size of array even.
%
%   MAKEEVEN(A) appends rows/columns of zeros to 2-D/3-D array A so that size of A
%   becomes even. If A is 4-D data, then each 4th dimension will be treated
%   individually like 3D.
%
%   MAKEEVEN(A,v) appends rows/columns of v to 2-D/3-D A so that size of A
%   becomes even.
%
%   [x,u] = MAKEEVEN(A) also returns an vector u that contains information
%   for un-doing this function.
%
%   MAKEODD(A,[],u) undoes operation with u.
%
%
%   See also: MAKEODD

%   2009/06/10 F Schweser, mail@ferdinand-schweser.de
%
%   Changelog:
%   2009/08/25 - F Schweser - Now supports 2D-data.
%   2011/07/06 - F Schweser - Now works with mids data.
%   2011/07/06 - F Schweser - Improved error handling
%   2012/03/21 - F Schweser - Now supports 4D time series data
%   2018/09/21 - F Schweser - UnDo-Feature added

if nargin < 2 || isempty(paddingValue)
    paddingValue = 0;
end


if isobject(inputArrayObj)
  inputArray = inputArrayObj.img;
else
  inputArray = inputArrayObj;
end


if nargin == 3 && ~isempty(unDoVector)

    
    % un-do
    
    outputArray = zeros(size(inputArray)-abs(unDoVector));
    
    outputArray(:,:,:,:) = inputArray(1:size(inputArray,1)-abs(unDoVector(1)),1:size(inputArray,2)-abs(unDoVector(2)),1:size(inputArray,3)-abs(unDoVector(3)),:);
    
%     
%     if ~unDoVector(1)
%         outputArray(size(inputArray,1)+1,:,:,:) = 0;
%     end
%     if ~unDoVector(2)
%         outputArray(:,size(inputArray,2)+1,:,:) = 0;
%     end
%     if ndims(unDoVector) > 2
%         if ~unDoVector(3)
%             outputArray(:,:,size(inputArray,3)+1,:) = 0;
%         end
%         if ndims(unDoVector) > 3
%             if ~unDoVector(4)
%                 outputArray(:,:,:,size(inputArray,4)+1) = 0;
%             end
%         end
%     end    
%     
    inputArray = outputArray;
    
    
    
else

    appendVector = mod(size(inputArray),2);

    if ndims(inputArray) == 3 || ndims(inputArray) == 4

        % 3-D data or time series (4D) data

        for jVol = 1:size(inputArray,4)
            if appendVector(1)
                inputArray(size(inputArray,1)+1,:,:,jVol) = ones(size(inputArray,2),size(inputArray,3))*paddingValue;
            end
            if appendVector(2)
                inputArray(:,size(inputArray,2)+1,:,jVol) = ones(size(inputArray,1),size(inputArray,3))*paddingValue;
            end
            if appendVector(3)
                inputArray(:,:,size(inputArray,3)+1,jVol) = ones(size(inputArray,1),size(inputArray,2))*paddingValue;
            end
        end    


    elseif ndims(inputArray) == 2

        % 2-D data

        if appendVector(1)
            inputArray(size(inputArray,1)+1,:) = ones(size(inputArray,2),1)*paddingValue;
        end
        if appendVector(2)
            inputArray(:,size(inputArray,2)+1) = ones(size(inputArray,1),1)*paddingValue;
        end

    else
          error('This function supports only 2D, 3D, and 4D data.')
    end

end

if isobject(inputArrayObj)
  inputArrayObj.img = inputArray;
else
  inputArrayObj = inputArray;
end

end
