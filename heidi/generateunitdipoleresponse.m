function returnArray = generateunitdipoleresponse(dims, boolFourierDomain, Options,displayLevel)


%GENERATEUNITDIPOLERESPONSE Unit-dipole response.
%
%   GENERATEUNITDIPOLERESPONSE Generates uni-dipole response.
%
%
%   Syntax
%
%   GENERATEUNITDIPOLERESPONSE(d)
%   GENERATEUNITDIPOLERESPONSE(d,f)
%   GENERATEUNITDIPOLERESPONSE(d,f,Options)
%   GENERATEUNITDIPOLERESPONSE(d,f,Options,l)
%
%
%   Description
%
%   GENERATEUNITDIPOLERESPONSE(d) generates unit dipole response with grid-size
%   specified by vector d.
%
%   GENERATEUNITDIPOLERESPONSE(d,true) generates Fourier-Domain representation of dipole response.
%
%   GENERATEUNITDIPOLERESPONSE(d,true,Options) uses options specified in struct Options
%   generation of the dipole response.
%
%   GENERATEUNITDIPOLERESPONSE(d,true,Options,false) omitts outputs to
%   shell (display).
%
%
%
%   Following Option-fields are supported:
%       ConvolutionFilterCompensation - Struct of parameters for
%       compensation of artifacts induced by convolution-type filters for
%       estimation of external field contributions (see Diploma-thesis of
%       BW Lehr, 2009). Follwing fields are currently supported:
%           Spherical Mean Value Estimation: type = 'smv'
%               parameterRadius    - radius of spherical shell
%               parameterThickness - thickness of spherical shell
%
%           Gaussian High-Pass Filtering: type = 'gaussian'
%               parameterWidth     - spatial domain width of kernel
%
%           Hanning Filter:               type = 'hanning'
%               parameterWidth     - fourier domain width of kernel, e.g.
%                                    [64 64]
%
%       Rotation - Struct for positioning of data with respect to scanner
%       system.
%
%       DipoleFilter - Struct of parameters for regularization of the
%       dipole response. Following fields are supported:
%           type      - Type of regularization:
%                       'ParamWiener'         - parametric Wiener Filter
%                       'Wiener'              - Wiener Filter
%                       'threshParamWiener'   - Thresholded (min) parametric Wiener Filter
%                       'truncParamWiener'    - Truncated parametric Wiener
%                       Filter
%                       'threshSingularValues'- Thresholded Singular Values Spectrum
%                       'truncSingularValues' - Truncated Singular Values Spectrum
%                       'SDI'                 - Extreme Thresholding (no .parameter needed)
%           parameter - Parameter corresponding to the chosen type.
%
%       term - 3: chi33, 2: chi23, 1: chi13 (default: 3)
%
%       voxelAspectRatio - Aspect ratio of voxels (default = [1 1 1]).
%       Aspect ratio corresponds to voxel size.
%
%       isFourierDomainFormula - Boolean for using Fourier-Domain
%       formula of the unit dipole response.
%
%       spatialOriginValue - value in the spatial origin of the dipole
%       (default:0)


%
%
%   First release of this function was coded by BW Lehr.
%   F Schweser, 06/2009, mail@ferdinand-schweser.de
%
%   Changelog:
%   v3.1 - 20201027 - F Schweser - Major bugfox regarding angle=0 data that
%   had B0 in non-z direction
%   v3 - 2019/07/08 - E Baader - added Rakshits code regarding chi31 and
%   chi32 components
%   
%   v2.2 - 2018/10/29 - F Schweser - added spatialOriginValue
%   v2.2 - 2018/03/07 - F Schweser - New mode SDI
%   v2.1 - 2014/03/31 - F Schweser - Check of rotation mateix
%   v2 - 2011/01/20 - F Schweser - Hanning compensation added
%   v1.93 - 2010/09/23 - F Schweser - added 'symmetric' flag for Fourier
%                                     domain formula mode (ifftsave)
%   v1.92 , 2010/09/01 A Deistung   - BugFix: rotation matrix 
%   v1.91 - 2010/08/11 A Deistung   - BugFix: rotation matrix 
%
%   v1.9 - 2010/08/11 A Deistung   - rotation matrix as input for Rotation
%                                    is now also possible
%
%   v1.8 - 2009/10/05 (F Schweser) - Critical smv-thickness bug resolved.
%   v1.71 - 2009/10/03 - F Schweser: Bugfix if Rotation angle was 0.
%   v1.7 - 2009/09/16 - F Schweser: New Option isFourierDomainFormula
%   added. This Option enables usage of Fourier domain representation
%   formula.
%   v1.61 - 2009/09/08 - F Schweser: Minor Bugfixes concerning aspect
%   ratio.
%   v1.6 - 2009/09/04 - F Schweser: Added support for Gaussian Filter
%   Compensation; Changed call of filter compensation; documentation added;
%   variable voxel-aspect ratio added.
%   v1.5 - 2009/08/21 - F Schweser: fieldball variable name changed to
%   returnArray
%
%   v1.4 - 2009/08/20 - M Hütten: Rotation added, some clean-ups (F
%   Schweser)
%
%   v1.3 - 2009/08/18 - F Schweser: Changed function name and call of
%   SMV-function (name changed)
%
%   v1.2 - 2009/08/11 - F Schweser: (Bugfix) Dipole-Cropping withoput
%   SMV-errorterm not working. --> resolved.
%
%   v1.1 - 2009/08/06 - F Schweser: (Bugfix) Removed explicit cropping of dipole
%   field before substraction of SMV.




if nargin < 1,	error('Not enough input arguments.');               end

if nargin < 2 || isempty(boolFourierDomain)
    boolFourierDomain = false;
end

if nargin < 3 || isempty(Options)
    % default Options
    Options.dummy = [];
end

if nargin < 4 || isempty(displayLevel)
    displayLevel = true;
end

if ~mod(dims,2)
    warning('MATLAB:evenGrid', 'Please use grid with odd size to avoid half-pixel-shift errors.');
    disp('Please press Enter to continue...')
    pause
end

if ~isfield(Options,'spatialOriginValue') || isempty(Options.spatialOriginValue)
    spatialOriginValue = 0;
end
if myisfield(Options,'Rotation') && ~isempty(Options.Rotation)
    
    
    if ~isfield(Options,'term') || isempty(Options.term)
        Options.term = 3;
    end
    
    
    if (~myisfield(Options.Rotation, 'angle') || ~myisfield(Options.Rotation,'axis'))  && ~myisfield(Options.Rotation,'matrix')
        error('Rotation axis and rotation angles or rotation matrix not specified.')
    else
        % convert Rotation matrix into the angle and axis representation
        if myisfield(Options, 'Rotation') && (~myisfield(Options.Rotation, 'angle') && ~myisfield(Options.Rotation,'axis') && myisfield(Options.Rotation,'matrix')            )
            if det(Options.Rotation.matrix) ~= 1
                error('Rotation matrix is improper, i.e. it contains reflections. This would result in improper angle calculations in the following (e.g. using vrrotmat2vec). For this reason I am aborting here.')
            end
            vectorExpressionTmp = vrrotmat2vec(Options.Rotation.matrix);
            Options.Rotation.axis  = vectorExpressionTmp(1:3);
            Options.Rotation.angle = rad2deg(vectorExpressionTmp(4));
        end
        
        if Options.Rotation.angle == 0
            useRotationBool = false;
        else
            useRotationBool = true;
        end
    end
else
    useRotationBool = false;
end


if ~myisfield(Options,'ConvolutionFilterCompensation') || isempty(Options.ConvolutionFilterCompensation)
    convolutionFilterCompensationBool = false;
elseif ~myisfield(Options.ConvolutionFilterCompensation,'type')
    error('Filter compensation type must be specified!')
elseif ~isempty(Options.ConvolutionFilterCompensation.type)
    convolutionFilterCompensationBool = true;
else
    convolutionFilterCompensationBool = false;
end


if ~myisfield(Options,'voxelAspectRatio') || isempty(Options.voxelAspectRatio)
    Options.voxelAspectRatio = [1 1 1];
    isIsotropicVoxel = true;
else
    isIsotropicVoxel = false;
    Options.voxelAspectRatio = Options.voxelAspectRatio ./ min(Options.voxelAspectRatio);
    disp(['Voxel Aspect Ratio is ',num2str(Options.voxelAspectRatio(1)),':',num2str(Options.voxelAspectRatio(2)),':',num2str(Options.voxelAspectRatio(3)),' (x:y:z)'])
end

if ~myisfield(Options,'isFourierDomainFormula') || isempty(Options.isFourierDomainFormula) || ~Options.isFourierDomainFormula
    Options.isFourierDomainFormula = false;
    disp('Formula:                        SPATIAL DOMAIN');
end

%% main part

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate coordinate system
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isIsotropicVoxel
    if ~Options.isFourierDomainFormula %spatial domain formula
        [x,y,z,r] = mesh3d(dims);
    else                               %fourier domain formula
        tmpAspectRatio = 1./dims;
        tmpAspectRatio = tmpAspectRatio ./ min(tmpAspectRatio);
        [x,y,z,r] = mesh3d(dims,tmpAspectRatio);
    end
    origin = r == 0;
    
else
    if ~Options.isFourierDomainFormula
        dimsNonIsotropicFull = dims .* Options.voxelAspectRatio - (mod(dims .* Options.voxelAspectRatio,2) - 1);
        [x,y,z,r] = mesh3d(dimsNonIsotropicFull);
        rTmp = mesh3d(dims);
        origin = rTmp == 0;
        clear rTmp;
        
    else % Fourier domain formula
        tmpAspectRatio = 1./dims;
        if nnz(size(tmpAspectRatio) - size(Options.voxelAspectRatio)) ~= 0
            tmpAspectRatio = tmpAspectRatio ./ Options.voxelAspectRatio';
        else
            tmpAspectRatio = tmpAspectRatio ./ Options.voxelAspectRatio;
        end
        tmpAspectRatio = tmpAspectRatio ./ min(tmpAspectRatio);
        [x,y,z,r] = mesh3d(dims,tmpAspectRatio);
        origin = r == 0;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate rotation matrix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~useRotationBool
    if displayLevel
        disp('Rotation:                       DEFAULT (transversal)');
    end
else
    angle=Options.Rotation.angle*pi/180; % degree2radians
    axisNormalized =Options.Rotation.axis/sqrt(Options.Rotation.axis(1)^2+Options.Rotation.axis(2)^2+Options.Rotation.axis(3)^2); % axis is normalized
    if displayLevel
        disp(['Rotation axis:                  ',num2str(Options.Rotation.axis(1)),',',num2str(Options.Rotation.axis(2)),',',num2str(Options.Rotation.axis(3))]);
        disp(['Rotation angle:                 ',num2str(Options.Rotation.angle),'°']);
    end
    x_new=   (axisNormalized(1)^(2)               .*(1-cos(angle))+cos(angle))                   .*x ...
        +(axisNormalized(1).*axisNormalized(2).*(1-cos(angle))-axisNormalized(3).*sin(angle)).*y ...
        +(axisNormalized(1).*axisNormalized(3).*(1-cos(angle))+axisNormalized(2).*sin(angle)).*z;
    
    y_new=   (axisNormalized(2).*axisNormalized(1).*(1-cos(angle))+axisNormalized(3).*sin(angle)).*x ...
        +(axisNormalized(2)^(2)               .*(1-cos(angle))+cos(angle))                   .*y ...
        +(axisNormalized(2).*axisNormalized(3).*(1-cos(angle))-axisNormalized(1).*sin(angle)).*z;
     
    z_new =  (axisNormalized(3).*axisNormalized(1).*(1-cos(angle))-axisNormalized(2).*sin(angle)).*x ...
        +(axisNormalized(3).*axisNormalized(2).*(1-cos(angle))+axisNormalized(1).*sin(angle)).*y ...
        +(axisNormalized(3)^(2)               .*(1-cos(angle))+cos(angle))                   .*z;
    x=x_new;
    y=y_new;
    z=z_new;
    clear x_new y_new z_new
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% generate unit dipole response
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~Options.isFourierDomainFormula
    
    % spatial domain
    if Options.term == 3
        returnArray = (3 * (z./r).^2 - 1) ./ (abs(r).^3 * 4*pi);  %1/3 - (z./r).^2;
    elseif Options.term == 1
        returnArray= 3 * (((z.*x)./r.^2)) ./ (abs(r).^3 * 4*pi);
    else
        returnArray=3 * (((z.*y)./r.^2)) ./ (abs(r).^3 * 4*pi);
    end
    %returnArray = (3 * (z./r).^2 - 1) ./ (abs(r).^3 * 4*pi);
    returnArray(r==0) = spatialOriginValue;
    %returnArray(isnan(returnArray)) = 0;
    %returnArray(isnan(returnArray)) = 0;
    clear x y z
    
    
    if ~isIsotropicVoxel
        returnArray = fftsave(returnArray);
        frameSizeToBeCropped = (dimsNonIsotropicFull - dims)/2;
        returnArray = returnArray(frameSizeToBeCropped(1)+1:dimsNonIsotropicFull(1)-frameSizeToBeCropped(1),...
            frameSizeToBeCropped(2)+1:dimsNonIsotropicFull(2)-frameSizeToBeCropped(2),...
            frameSizeToBeCropped(3)+1:dimsNonIsotropicFull(3)-frameSizeToBeCropped(3));
        
        returnArray = ifftsave(returnArray);
    end
    
    
else
    % Fourier domain
    if Options.term == 3
        returnArray = 1/3 - (z./r).^2;
        returnArray(r==0) = spatialOriginValue;
        clear x y z

        returnArray = ifftsave(returnArray,[],'symmetric');
        %returnArray(r==0) = 0;
    elseif Options.term == 1
        returnArray= (-x.*z)./(r.^2);
        returnArray(r==0) = spatialOriginValue;
        clear x y z
        returnArray=ifftsave(returnArray, [],'symmetric');
    else
       returnArray=(-y.*z)./(r.^2);
       returnArray(r==0) = spatialOriginValue;
       clear x y z
       returnArray=ifftsave(returnArray, [], 'symmetric');
    end
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% perform convolution filter artifact compensation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if convolutionFilterCompensationBool
    switch Options.ConvolutionFilterCompensation.type
        case 'smv'
            if  Options.ConvolutionFilterCompensation.parameterThickness
                returnArray = returnArray - computesphericalmeanvalue(returnArray,[],Options.ConvolutionFilterCompensation.parameterRadius, Options.ConvolutionFilterCompensation.parameterThickness,Options.voxelAspectRatio);
            else
                warnHandle = warndlg('No shell-thickness parameter specified! Only cropping of uni-dipole response will be performed! (This message will close in 30s)','SMVE Warning!');
                uiwait(warnHandle,30)
                % no SMV-errorterm but dipole-cropping
                if Options.ConvolutionFilterCompensation.parameterRadius
                    returnArray(r > Options.ConvolutionFilterCompensation.parameterRadius) = 0;
                end
            end
        case 'gaussian'
            returnArray = filtergaussian(returnArray,Options.ConvolutionFilterCompensation.parameterWidth);
        case 'hanning'
            [~,kernel] = homodynefilter(returnArray,Options.ConvolutionFilterCompensation.parameterWidth);
            kernel = repmat(kernel,[1 1 size(returnArray,3)]);
            returnArray = real(ifftsave(fftsave(returnArray) .* (1-kernel)));
        otherwise
            error('Unsupported filter compensation type.')
    end
end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% apply regularization
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if myisfield(Options,'DipoleFilter') && ~isempty(Options.DipoleFilter)
    
    % calculate Fourier domain representation and set center to zero
    % (numerical limits)
    returnArray = real(fftsave(returnArray));
    returnArray(origin) = 0;
    
    if strcmp(Options.DipoleFilter.type,'ParamWiener') % parametric wiener filter
        
        disp('Using parametric Wiener Filter...')
        
        zeroElements = (returnArray == 0);
        
        returnArray = (abs(returnArray).^2 + Options.DipoleFilter.parameter)./ returnArray;
        returnArray(logical(zeroElements)) = 0;
        
    elseif strcmp(Options.DipoleFilter.type,'Wiener') % parametric wiener filter
        
        disp('Using Wiener Filter...')
        
        zeroElements = (returnArray == 0);
        
        returnArray = (abs(returnArray).^2 + Options.DipoleFilter.noisePowerSpectrum./Options.DipoleFilter.originalPowerSpectrum)./ returnArray;
        returnArray(logical(zeroElements)) = 0;
        
        
    elseif strcmp(Options.DipoleFilter.type,'threshParamWiener') % thresholded parametric wiener filter
        % this filter is thresholded at the minimum of the response
        % function
        
        disp('Using parametric Wiener Filter with thresholded singular value spectrum...')
        
        zeroElements = logical(returnArray == 0);
        
        % determine Fourier coefficients that correspond to minimum of
        % Wiener filter
        dipoleResponseCorrespFilterMin = logical(abs(returnArray) < sqrt(Options.DipoleFilter.parameter));
        
        % determine minimum of Wiener filter
        wienerFilterMin = 2*sqrt(Options.DipoleFilter.parameter);
        
        % calculate filter
        returnArray = (abs(returnArray).^2 + Options.DipoleFilter.parameter)./ returnArray;
        returnArray(dipoleResponseCorrespFilterMin) = returnArray(dipoleResponseCorrespFilterMin) ./ abs(returnArray(dipoleResponseCorrespFilterMin)) * wienerFilterMin;
        returnArray(zeroElements) = 0;
        
        
    elseif strcmp(Options.DipoleFilter.type,'truncParamWiener') % truncated parametric wiener filter (setting increasing values to zero)
        
        disp('Using parametric Wiener Filter with truncated singular value spectrum...')
        
        zeroElements = logical(returnArray == 0);
        
        % determine Fourier coefficients that correspond to minimum of
        % Wiener filter
        dipoleResponseCorrespFilterMin = logical(abs(returnArray) < sqrt(Options.DipoleFilter.parameter));
        
        
        % calculate filter
        returnArray = (abs(returnArray).^2 + Options.DipoleFilter.parameter)./ returnArray;
        returnArray(dipoleResponseCorrespFilterMin) = 0;
        returnArray(zeroElements) = 0;
        
    
    elseif strcmp(Options.DipoleFilter.type,'SDI') % thresholded singular value spectrum
        
        disp('Using extreme thresholding...')
        
        returnArray(returnArray < 0) = min(returnArray(:));
        returnArray(returnArray >= 0) = max(returnArray(:));
        
    elseif strcmp(Options.DipoleFilter.type,'threshSingularValues') % thresholded singular value spectrum
        
        disp('Using thresholded Singular Value spectrum...')
        
        returnArray(returnArray == 0) = Options.DipoleFilter.parameter;
        returnArray(abs(returnArray) < Options.DipoleFilter.parameter) = returnArray(abs(returnArray) < Options.DipoleFilter.parameter) ./ abs(returnArray(abs(returnArray) < Options.DipoleFilter.parameter)) * Options.DipoleFilter.parameter;
        
        
    elseif strcmp(Options.DipoleFilter.type,'truncSingularValues') % truncate singular value spectrum
        
        disp('Using truncated Singular Value spectrum...')
        
        returnArray(abs(returnArray) < Options.DipoleFilter.parameter) = 0;
        
    end
    
    if ~boolFourierDomain
        returnArray = real(ifftsave(returnArray));
        returnArray(origin) = 0;
    end
    
    
    
    
else
    if boolFourierDomain
        returnArray = real(fftsave(returnArray));
        returnArray(origin) = 0;
    elseif ~Options.isFourierDomainFormula
        returnArray = real(fftsave(returnArray));
        returnArray(origin) = 0;
        returnArray = real(ifftsave(returnArray));
        returnArray(origin) = 0;
        %         if convolutionFilterCompensationBool
        %             if strcmp(Options.ConvolutionFilterCompensation.type,'smv')
        %                 returnArray(r > Options.ConvolutionFilterCompensation.parameterRadius) = 0;
        %             end
        %         end
        
    end
end




end