function outMatrix = generatelaplacian3d(sizeVector, Options, spacing_x, spacing_y, spacing_z)
 
%GENERATELAPLACIAN3D Generates 3-D laplacian.
%
%
%   GENERATELAPLACIAN3D(s) generates 3-D laplacian on grid of size s
%   (vector with odd elements).
%  
%   GENERATELAPLACIAN3D(s, Options, x, y, z) generates a 3-D laplacian on
%   grid of size s with spacings x,y,z (= voxel dimensions) in meter.
%   (default: x=y=z=1) - only inplemented for second laplacian!
%
%
% 
%   The following Option-fields of are supported:
%
%       Laplacian                 - defines which Laplacian shall be used,
%                                   '1' or '2' or '4d' - see below for explicit
%                                   form of these laplacians
%                                   best regularization Parameters for sharp are 0.1 and 3e-5, respectively
%                             
% 
%   F Schweser, mail@ferdinand-schweser.de, 2009/09/22
%
%   Changelog:
% 
%   2011/09/22 - F Schweser - 4d version added
%   2011/01/20 - K Sommer - header updated, standard Laplacian changed to '1'
%   2011/01/06 - K Sommer - input arguments "spacings" added   
%   2010/12/21 - A Deistung - bugfix (input arguments) 
%   2010/12/20 - F Schweser - bugfix (input arguments) 
%   2010/11/05 - K Sommer - Second Laplacian added 
 
 
 
if nargin < 2 || ~myisfield(Options,'Laplacian') || isempty(Options.Laplacian)
        Options.Laplacian = '1';   
end

if nargin < 3 || isempty(spacing_x) || isempty(spacing_y) || isempty(spacing_z)
        spacing_x = 1;
        spacing_y = 1;
        spacing_z = 1;
end
 
outMatrix = zeros(3,3,3);
% outMatrix(:,:,1) = [0 3 0;3 10 3;0 3 0];
% outMatrix(:,:,3) = outMatrix(:,:,1);
% outMatrix(:,:,2) = [3 10 3;10 -96 10;3 10 3];
 
switch Options.Laplacian
    
    case '1'
        
        outMatrix(:,:,1) = [0  3 0;
            3 10 3;
            0  3 0];
        outMatrix(:,:,3) = outMatrix(:,:,1);
        outMatrix(:,:,2) = [ 3  10  3;
            10 -96 10;
            3  10  3];
        
    case '2'
        
        outMatrix(:,:,1) = [0 0 0;
                      0  1/spacing_z^2  0;
                            0 0 0];
           
        outMatrix(:,:,3) = outMatrix(:,:,1);
        outMatrix(:,:,2) = [ 0              1/spacing_y^2           0;
                            1/spacing_x^2          0          1/spacing_x^2;
                             0              1/spacing_y^2            0];
        outMatrix(2,2,2) = -2/spacing_x^2 - 2/spacing_y^2 - 2/spacing_z^2;
        
        
     case '4d'
        outMatrix = zeros(3,3,3,3);
        spacing_t = 1;
        
        outMatrix(:,:,1,2) = [0 0 0;
                      0  1/spacing_z^2  0;
                            0 0 0];
           
        outMatrix(:,:,3,2) = outMatrix(:,:,1,2);
        outMatrix(:,:,2,2) = [ 0              1/spacing_y^2           0;
                            1/spacing_x^2          0          1/spacing_x^2;
                             0              1/spacing_y^2            0];
        outMatrix(2,2,2,2) = -2/spacing_x^2 - 2/spacing_y^2 - 2/spacing_z^2 - 2/spacing_t^2;
        outMatrix(2,2,2,1) = 1/spacing_t^2;
        outMatrix(2,2,2,3) = 1/spacing_t^2;
end

                 

outMatrix = zeropadding(outMatrix,sizeVector);
 
 