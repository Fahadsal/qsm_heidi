function returnBool = parity(inputValue)

%PARITY Returns parity of input value.
%
%   Syntax
%
%   PARITY(x)
%
%
%   Description
%
%   PARITY(x) returns 1 if scalar x is odd and false if the value is
%   even.
%

% F Schweser, mail@ferdinand-schweser.de, 2009/11/18
%
%   Changelog:
%
%   v1.0 - 2009/11/19 - F Schweser


returnBool = ~(abs(inputValue/2) - round(inputValue/2)) == 0;



end