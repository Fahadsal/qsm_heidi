function dataArrayObj = erodemask(dataArrayObj,nErode,boolUseFrame,type, erodeKernel,erodeKernelThresh)

%ERODEMASK Erodes mask.
%
%   ERODEMASK Erodes a mask.
%
%   Syntax
%
%   ERODEMASK(A,n)
%   ERODEMASK(A,n,true)
%   ERODEMASK(A,n,[],type)
%   ERODEMASK(A,n,true, type, erodeKernel)
%   ERODEMASK(A,n,true, type, erodeKernel,erodeKernelThresh)
%   ERODEMASK(A,K)
%
%
%
%   Description
%
%   ERODEMASK(A,n) erodes n times the N-D array A.
%
%   ERODEMASK(A,n,true) avoids artifacts at the edges (if trues in mask extend to
%   the edge of the matrix).
%
%   ERODEMASK(A,n,true, type) If type is equal to '2d' performs a 2d erosion if not it will do it in 3D
%
%   ERODEMASK(A,n,true, type, erodeKernel) applies erosion with the kernel
%                                         passed by erodeKernel using Matlab's erode-function. Default =
%                                         ones(3,3,3);
%                                         Alternatively, erodeKernel may be
%                                         voxelsize. In this case
%                                         anisotropic erosion is performed.
%
%   ERODEMASK(A,n,true, type, erodeKernel,erodeKernelThresh) uses the
%                                         specified threshold
%                                         erodeKernelThresh for
%                                         thresholding the mask
%                                         (default:0.99)
%
%   ERODEMASK(A,K) erodes the N-D array A using the kernel in K. 
%
%   
%
%   (Function supports in-place call using A.)
%
%
%   See also: DILATEMASK, IMERODE

% F Schweser, 2009/07/23, mail@ferdinand-schweser.de
%
%   v1.1 - 2009/12/15 - F Schweser - anti-aliasing frame 
%   v1.2 - 2009/02/15 - F Schweser - Now returns logical.
%   v1.3 - 2010/10/04 - F Schweser - Now works with mids object.
%   v1.4 - 2011/02/22 - A Deistung - BugFix: mids-output
%   v1.5 - 2011/02/28 - F Schweser - Kernel functionality added
%   v1.6 - 2011/05/31 - A Deistung - data input is now also supported if 
%                                    input is a struct with img field
%   v1.7 - 2011/06/30 - D Lopez    - If type is equal to 2d it erodes the mask in x
%                                    and y coordinates slice by slice. 2D
%                                    erosion.
%   v1.7.1 2011/06/30 - D Lopez    - Description updated
%   v1.7.2 2011/07/12 - D Lopez    - Type default value set up to '3d'
%   v1.8   2011/09/26 - A Deistung - kernel for matlab-function erosion can
%                                    now be passed as input to the function 
%   v1.9   2012/02/04 - F Schweser - Now supports anisotropic erosion
%   v2.0   2012/06/29 - F Schweser - New parameter erodeKernelThresh.
%                                    Default value of 0.95 was used before
%                                    and has now been changed to 0.999999
%                                    because of substantial errors in the
%                                    SHARP.
%                                    + Debugging related to v1.8
%                                    + replaced symm. FFT by real(FFT)
%   v2.1   2012/07/12 - F Schweser - Bugfix: Now avoids artifacts at the edges
%                                    works even with supplied kerneöl
%   v2.2   2013/02/21 - F Schweser - Bugfix: Kernel was not zeropadded
%   v2.3   2013/09/30 - F Schweser - Bugfix: boolUseFrame mode failed due
%                                    to "bugfix" in reslice.

if nargin < 2
    error('Function requires two input arguments.')
end

DEFAULT_ERODEKERNELTHRESH = 0.999999;


if nargin > 2 && ~isempty(boolUseFrame)
    if ~islogical(boolUseFrame)
        error('Third parameter must be logical.')
    end
else
    boolUseFrame = false;
end

if nargin < 4 || isempty(type)
    %default mode set up to brain
    type = '3d';
end




if isobject(dataArrayObj)
    dataArray = dataArrayObj.img;
 
elseif isstruct(dataArrayObj)
    if isfield(dataArrayObj, 'img')
        dataArray = dataArrayObj.img;
    else
        error('The img field is not available. Please pass a mids-object, struct with the field img, or a data array to erodemask!')
    end
else
    dataArray = dataArrayObj;
end



if nargin < 5 || isempty(erodeKernel)
    %define default erode kernel
    if ~strcmp(type,'2d')
        erodeKernel = ones(3,3,3);
    else
        erodeKernel = ones(3,3);
    end
    erodeKernelThresh = DEFAULT_ERODEKERNELTHRESH;
    isReslicing = false;
elseif size(erodeKernel,1) == 1 % vector
    isReslicing = true;
    inputArraySize = size(dataArray);
    dataArray = reslice(dataArray,erodeKernel);
    erodeKernel = ones(3,3,3);
    erodeKernelThresh = DEFAULT_ERODEKERNELTHRESH;
else
    isReslicing = false;
end
if nargin < 6 || isempty(erodeKernelThresh)
	erodeKernelThresh = DEFAULT_ERODEKERNELTHRESH;
end

if numel(nErode) == 1
    isKernel = false;
else
    isKernel = true;
    erodeKernel = abs(erodeKernel) / max(abs(erodeKernel(:))); % normalization
    nErode = ceil(nthroot(sum(erodeKernel(:)>0),3)); % approximate kernel as cube and estimate kernel width
end
    



%%%%%%%%%%%%%%%%%%%%%%%%%
% Add frame (if desired)
%%%%%%%%%%%%%%%%%%%%%%%%%
if boolUseFrame
    [dataArray unDovector] = prepareforconvolution(dataArray,[],2*(nErode+2));
    if isKernel
        erodeKernel = zeropadding(erodeKernel,size(dataArray));
        %erodeKernel = prepareforconvolution(erodeKernel,[],2*(nErode+2));
    end
end



if isKernel
    dataArray = real(ifftsave(fftsave(dataArray) .* fftsave(erodeKernel)));
    dataArray = dataArray ./ max(dataArray(:)); % normalize to 1
    dataArray = dataArray > erodeKernelThresh;
else
    
    for jErode = 1:nErode
        if ~strcmp(type,'2d')
            dataArray = imerode(dataArray,erodeKernel);
        else
            for jSlice = 1:size(dataArray,3)
                dataArray(:,:,jSlice) = imerode(dataArray(:,:,jSlice),erodeKernel);
            end
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove frame
%%%%%%%%%%%%%%%%%%%%%%%%%
if boolUseFrame
    dataArray = prepareforconvolution(dataArray,[],[],unDovector);
end


dataArray = logical(dataArray);


if isReslicing
    dataArray = reslice(dataArray,[],[], inputArraySize);
end


if isobject(dataArrayObj) || isstruct(dataArrayObj)
    dataArrayObj.img = dataArray;
else 
    dataArrayObj = dataArray;
end


end