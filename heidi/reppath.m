function Path = reppath()
%REPPATH returns path of repository without subsequent slash
%
%   REPPATH returns path of repository
%
%
%   Syntax
%
%   reppath()
%
%
%   Description
%
%   REPPATH Returns path of repository

% 2010/04/20 B W Lehr, p2lebe@uni-jena.de
    myPath = which('reppath.m');
    repPath = 'RepPath.m';
    Path = myPath(1:numel(myPath) - numel(repPath));
return
