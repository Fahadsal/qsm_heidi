function pattern = discretegradient(pattern,direction,voxelSize,spacing,isTranspose)
%DISCRETEGRADIENT fast calculation of gradient
%
%   DISCRETEGRADIENT returns gradient
%
%
%   Syntax
%
%   DISCRETEGRADIENT(A,d)
%   DISCRETEGRADIENT(A,d,s)
%
%
%   Description
%
%   DISCRETEGRADIENT(A,d) returns gradient of 3D pattern A in direction d.
%   d is a string: 'x', 'y' or 'z'.
%
%   DISCRETEGRADIENT(A,d,s) considers anisotropic voxel size. s is the voxel
%   spacing in direction of the gradient. Default: 1
%
%   DISCRETEGRADIENT(A,d,s,c) calculates the gradient using every c-th
%   voxel. Supported values: 2 (adjacent voxels) and any other positive integer. (default: 3)
%
%   DISCRETEGRADIENT(A,d,s,c,true) calculated the transpose of the sgradient.
%
%
%   This function is 2-3 times faster than Fourier based convolution and
%   much faster than matlabs gradient function.

% 2011/09/19 F Schweser
%
%   Changelog:
%
%   v1.0 - 2011-09-19 - F Schweser
%   v1.1 - 2011-12-22 - F Schweser - Now supports transpose and different
%                                    voxelspacing.
%   v1.2 - 2012-02-01 - F Schweser - Bugfix: rescaling of 3-spacing gradient
%   v1.3 - 2012-02-06 - F Schweser - Bugfix: rescaling of 3-spacing gradient
%   v1.4 - 2012-08-09 - F Schweser - Now supports arbitrary spacing
%   v1.5 - 2012-12-04 - F Schweser - Critical Bugfix: Calculation of
%                                    odd-spaced y and z gradients was
%                                    wrong.


if nargin < 3 || isempty(voxelSize)
    voxelSize = 1;
end
if nargin < 4 || isempty(spacing)
    spacing = 3;
end
if nargin < 5 || isempty(isTranspose)
    isTranspose = false;
end


if parity(spacing)
    %if spacing == 3
    centerVoxel = (spacing + 1)/2;
    switch direction
        case 'x'
            pattern(centerVoxel:end-(spacing-centerVoxel),:,:) = pattern(spacing:end,:,:) - pattern(1:end-spacing+1,:,:);
            pattern(1:centerVoxel-1,:,:) = 0;
            pattern(end-(spacing-centerVoxel)+1:end,:,:) = 0;
        case 'y'
            pattern(:,centerVoxel:end-(spacing-centerVoxel),:) = pattern(:,spacing:end,:) - pattern(:,1:end-spacing+1,:);
            pattern(:,1:centerVoxel-1,:) = 0;
            pattern(:,end-(spacing-centerVoxel)+1:end,:) = 0;
        case 'z'
            pattern(:,:,centerVoxel:end-(spacing-centerVoxel)) = pattern(:,:,spacing:end) - pattern(:,:,1:end-spacing+1);
            pattern(:,:,1:centerVoxel-1) = 0;
            pattern(:,:,end-(spacing-centerVoxel)+1:end) = 0;
    end
    pattern = pattern / (spacing-1); %because calculated from -1 to 1 in case of 3 (nenner)
    if isTranspose
        pattern = -pattern;
    end
    
else %if spacing == 2
    if ~isTranspose
        centerVoxel = spacing/2 + 1;
        subtractionOrder = -1;
    else
        centerVoxel = spacing/2;
        subtractionOrder = 1;
    end
    
    switch direction
        case 'x'
            pattern(centerVoxel:end-(spacing-centerVoxel),:,:) = subtractionOrder*(pattern(1:end-spacing+1,:,:) - pattern(spacing:end,:,:));
            pattern(1:centerVoxel-1,:,:) = 0;
            pattern(end-(spacing-centerVoxel)+1:end,:,:) = 0;
        case 'y'
            pattern(:,centerVoxel:end-(spacing-centerVoxel),:) = subtractionOrder*(pattern(:,1:end-spacing+1,:) - pattern(:,spacing:end,:));
            pattern(:,1:centerVoxel-1,:) = 0;
            pattern(:,end-(spacing-centerVoxel)+1:end,:) = 0;
        case 'z'
            pattern(:,:,centerVoxel:end-(spacing-centerVoxel)) = subtractionOrder*(pattern(:,:,1:end-spacing+1) - pattern(:,:,spacing:end));
            pattern(:,:,1:centerVoxel-1) = 0;
            pattern(:,:,end-(spacing-centerVoxel)+1:end) = 0;
    end
    
    pattern = pattern / (spacing-1); %because calculated from -1 to 1 in case of 3 (nenner)
    
    %else
    %    error('Voxel spacing for gradient not supported.')
end

pattern = pattern / voxelSize; % because calculated from -1 to 1 in 3


return
