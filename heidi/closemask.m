function dataArrayObj = closemask(dataArrayObj,nErode,boolUseFrame,type)

%CLOSEMASK Closes mask.
%
%   CLOSEMASK Closes a mask.
%
%   Syntax
%
%   CLOSEMASK(A,n)
%   CLOSEMASK(A,n,true)
%
%
%
%   Description
%
%   CLOSEMASK(A,n) closes a mask by first dilating and after that eroding 
%   n times the (real or logical) N-D array A.
%
%   CLOSEMASK(A,n,true) avoids artifacts at the edges (if trues in mask extend to
%   the edge of the matrix).
%
%   CLOSEMASK(A,n,true,type) If type is equal to '2d' mask will be closed
%   in 2d, otherwise it will be done in 3D
%
%
%   (Function supports in-place call using A.)
%
%
%   See also: DILATEMASK, IMERODE, ERODEMASK

% F Schweser, 2009/11/19, mail@ferdinand-schweser.de
%
%   Changelog:
%
%   v1.0 - 2009/11/19 - F Schweser
%   v1.1 - 2011/03/11 - ADeistung   - BugFix: dilate and erode will be
%                                     performed iteratively
%   v1.2 - 2011/03/11 - ADeistung   - no BugFix: reverted to v1.0
%   v1.3 - 2011/06/30 - DLopez      - Function call now includes type which will
%                                     be used in case we are working with 2D data. type is now included in
%                                     calls to dilatemask and erodemask
%   v1.3.1-2011/07/12 - D Lopez      - Description update for case type
%   v1.3.2-2011/07/12 - D Lopez       Type default value set up to '3d'
%   v.1.4 2011/07/01  - A Deistung - now also works with mids-objects



%%%%%%%%%%%%%%%%%%%%%%%%%
% Check arguments
%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin < 2
    error('Function requires two input arguments.')
end

if nargin > 2 && ~isempty(boolUseFrame)
    if ~islogical(boolUseFrame)
        error('Third parameter must be logical.')
    end
else
    boolUseFrame = false;
end

if nargin < 4
    %If no mode has been specified use brain as a default for 3D procedure
    type = '3d';
end




if isobject(dataArrayObj)
    dataArray = dataArrayObj.img;
elseif isstruct(dataArrayObj)
    if isfield(dataArrayObj, 'img')
        dataArray = dataArrayObj.img;
    else
        error('The img field is not available. Please pass a mids-object, struct with the field img, or a data array to erodemask!')
    end
else
    dataArray = dataArrayObj;
end


%%%%%%%%%%%%%%%%%%%%%%%%%
% Add frame (if desired)
%%%%%%%%%%%%%%%%%%%%%%%%%
if boolUseFrame
    [dataArray unDovector] = prepareforconvolution(dataArray,[],2*(nErode+2));
end





%%%%%%%%%%%%%%%%%%%%%%%%%
% Close mask
%%%%%%%%%%%%%%%%%%%%%%%%%

dataArray = dilatemask(dataArray,nErode,type);
dataArray = erodemask(dataArray,nErode,false,type);


%%%%%%%%%%%%%%%%%%%%%%%%%
% Remove frame
%%%%%%%%%%%%%%%%%%%%%%%%%
if boolUseFrame
    dataArray = prepareforconvolution(dataArray,[],[],unDovector);
end



if isobject(dataArrayObj) || isstruct(dataArrayObj)
    dataArrayObj.img = dataArray;
else 
    dataArrayObj = dataArray;
end


end