function ratioVector = voxelaspectratio(dcmHeader)

%VOXELASPECTRATIO Extracts voxel size from DCM header.
%
%   Syntax
%
%   VOXELASPECTRATIO(h)
%   VOXELASPECTRATIO(A)
%
%
%   Description
%
%   VOXELASPECTRATIO(h) extracts the voxel size from the dicom
%   header h and returns it as a 3-D real vector.
%
%   VOXELASPECTRATIO(A) extracts the voxel size ratio from the hdrDcm
%   or, if no hdrDcm field is available, from the hdr field of the data struct A.
%
%

%   F Schweser, mail@ferdinand-schweser.de, 2009/11/17
%
%   Changelog:
%
%   2009/11/17 - v1.0
%   2010/08/25 - v1.1 - F Schweser - Now supports img/hdrDcm/hdr struct data.
%   2010/09/06 - v1.2 - F Schweser - Now supports mids object
%   2010/11/03 - v1.3 - F Schweser - Now supports xray data
%   2011/04/28 - v1.4 - F Schweser - New: Error message if DICOM header wrong
%   2012/11/05 - v1.5 - F Schweser - Now supports cell-type DCM header

if ~isfield(dcmHeader,'SliceThickness') 
    % no dcm header! --> mids struct?
    
    if isobject(dcmHeader) && ~isempty(dcmHeader.getHeader('DICOM'))
        dcmHeader = dcmHeader.getHeader('DICOM');
        ratioVector = zeros(3,1);
        if iscell(dcmHeader)
            dcmHeader = dcmHeader{1};
        end
        if isfield(dcmHeader,'PixelSpacing')
            ratioVector(1:2) = dcmHeader.PixelSpacing;
            ratioVector(3) = dcmHeader.SliceThickness;
        elseif isfield(dcmHeader,'ImagerPixelSpacing') %e.g. xray
            ratioVector(1:2) = dcmHeader.ImagerPixelSpacing;
        else
            error('No valid DICOM header.')
        end
        
        ratioVector = ratioVector.';

    elseif isobject(dcmHeader) && ~isempty(dcmHeader.getHeader('NIFTI'))
        ratioVector = dcmHeader.getHeader('NIFTI').dime.pixdim(2:4);
        
    else
        error('Input type currently not supported.')
    end
    
else
        ratioVector = zeros(3,1);
        ratioVector(1:2) = dcmHeader.PixelSpacing;
        ratioVector(3) = dcmHeader.SliceThickness;
        ratioVector = ratioVector.';
 
end
    



end