function [solutionArray,niter,residuals,outputData,opts] = sparserecovery(measurementSamples,samplingMatrix,Options)

%SPARSERECOVERY Performs sparse solution of underdetermined system
%
%
%   Syntax
%
%
%   [X,niter,residuals,outputData,opts] = SPARSERECOVERY(b,M,Options)
%
%
%
%   Description
%
%
%   [X,niter,residuals,outputData,opts] = SPARSERECOVERY(b,M,Options) Finds a 3D sparse solution X to an underdetermined
%   system, Ax = b, resulting from undersampling, where x is the vector form of X. The nx1 vector b contains the
%   measurement samples, sampling matrix M is a 3-D logical
%   array specifying the fourier domain sampling point location, i.e., this matrix is true where signal was 
%   sampled and false where signal was not sampled. This function currently
%   only supports fourier domain sampling, i.e. inputs b and M should be
%   from fourier transformed data, while output X will be in the spatial
%   domain.
%
%   The following options are currently supported:
%
%       sparsityType    - String. This string defines the type of sparse
%                         reconstruction. The following types are currently
%                         supported:
%                            'L1' - l1-minimization problem. This type is
%                                   recommended if the solution if
%                                   (approximately) sparse - in this case
%                                   the l1-norm is an appropriate convex
%                                   function (as advocated by the CS
%                                   theory)
%                            'tv' - total variation minimization problem. This
%                                   type is recommended if the solution is
%                                   a piecewise constant function.
%
%       samplingMatrixType - String. This string defines the type of
%                            sampling matrix. The following types are 
%                            currently supported:
%                             'fourier' - For missing Fourier coefficients.
%
%       method              - String. This string defines the type of
%                             minimization problem and solving method. The following types are
%                             supported:
%                               'constrained' - (default) The problem is
%                                         quadratically constrained (see NESTA_3D)
%                               'unconstrained' - The problem is not
%                                         constrained (see NESTA_3D_UP)
%
%       maxitMu             - number of continuation steps
%       maxitInner          - max number of iterations in an inner loop
%       tolerance           - tolerance for the stopping criteria L1-norm (default: 1e-5)
%       toleranceEnergy     - tolerance for the stopping criteria energy
%       (default: 1e-5)s
%       muMin               - Desired value of mu (deviation from perfect l1 norm) 
%                             at the last continuation step. A smaller mu leads to higher accuracy.
%       epsilon             - enforces how close the variable must fit the observations
%       supportMask         - optional binary mask to be included in A (to
%                             further limit sampling) in "unconstrained" quadratic term problem
%       xplug               - first guess at solution (default xplug =
%                             At(b), where At is the adjoint of A)
%       voxelAspectRatio    - Voxel size. This is required for calculating
%                             the gradient accuratly. Default: 1 1 1
%       GradientMask_x/y/z  - Binary masks specifying the spatial domain in which
%                             the TV-norm is to be minimized (only
%                             sparsityType = 'tv')
%       fourierTransformType    - Type of Fourier transform to be used. This 
%                                 switch may be used if, e.g., 2D data aquisition.
%                                 has been performed. The following settings
%                                 are supported:
%                                    '2d' - FT only in x and y direction.
%                                           Use this if 2D ac. was
%                                           performed
%                                    [] (default --> means 3d mode)
%       gradientDimension    - Dimension of gradient to be used. This 
%                                 switch may be used if, e.g., 2D data aquisition.
%                                 has been performed. The following settings
%                                 are supported:
%                                    '2d' - Gradient evaluation only in x and y direction.
%                                           Use this if 2D ac. was
%                                           performed
%                                    [] (default --> means 3d mode)
%       doForceReal          - Forces result to be real-values (default: true)
%

% 
% 
% 
%   F Schweser - 2010/02/22 - mail@ferdinand-schweser.de
%
%   Changelog
%
%   v1.0 - 2011/02/22 - first version
%   v1.1 - 2011/03/01 - F Schweser - Variable names changed (conventions!)
%   v1.2 - 2011/03/04 - K Sommer - header update
%   v1.3 - 2011/03/24 - K Sommer - support for three different gradient masks added
%   v1.4 - 2011/04/01 - K Sommer - Energy 'E' piped to Nesta
%   v1.41 - 2011/04/04 - F Schweser - Bugfix: E not set.
%   v1.5 - 2011/06/23 - K Sommer - Default Parameters set / input arguments checked
%   v1.6 - 2011/08/30 - M Atterbury - unconstrained option added
%   v1.7 - 2011/09/19 - F Schweser - voxelAspectRatio option added +  Opts.grad1
%                                    eliminated because now gradient is
%                                    calculated with DISCRETEGRADIENT()
%   v1.71 - 2011/09/24 - F Schweser - inserted real() in member functions
%                                    to make sure that spatial domain is real
%   v1.8 - 2011/11/02 - F Schweser - ConeMask option removed
%   v1.9 - 2012/01/18 - F Schweser - new option fourierTransformType
%   v2.0 - 2012/02/14 - F Schweser - new option toleranceEnergy
%   v2.1 - <2012/03/27 - F Schweser - Now piped gradientType
%   v2.11 - 2012/04/04 - F Schweser - Bugfix for case that Opts.problemMask
%                                   was not set
%                                     + now uses default gradientType
%                                     + default stopTest changed from 2 to 1            
%   v2.12 - 2012/12/03 - F Schweser - Now supports gradientDimension
%   v2.2 - 2013/03/21 - F Schweser - New feature: doForceReal
%   v2.3 - 2015/08/29 - F Schweser - Bugfix: Normalization of voxel size

%%
DEFAULT_MAXITMU = 5;
DEFAULT_MAXITINNER = 20;
DEFAULT_MUMIN = 0.1;
DEFAULT_TOLERANCE = 1e-5;
DEFAULT_TOLERANCEENERGY = 1e-5;
DEFAULT_STOPTEST = 1;
DEFAULT_EPSILON = 0;
DEFAULT_METHOD = 'constrained';
DEFAULT_MASKTHRESH = 0.01;
DEFAULT_FTTYPE = [];
DEFAULT_GRADIENTDIMENSION = [];
DEFAULT_GRADIENTTYPE = 2;
DEFAULT_DOFORCEREAL = true;
%%

    solutionDimensionVector = size(samplingMatrix);

    
    
if nargin < 3 || isempty(Options)
    disp('Default parameters will be used.')
    Options.defaultParameters = true;
end

dispstatus(mfilename(),Options,'functioncall',true)


isTVx = false;
isTVy = false;
isTVz = false;
if ~myisfield(Options,'GradientMask_x') || isempty(Options.GradientMask_x)
    Options.GradientMask_x = ones(solutionDimensionVector);
    isTVx = true;
end
if ~myisfield(Options,'GradientMask_y') || isempty(Options.GradientMask_y)
    Options.GradientMask_y = ones(solutionDimensionVector);
    isTVy = true;
end
if ~myisfield(Options,'GradientMask_z') || isempty(Options.GradientMask_z)
    Options.GradientMask_z = ones(solutionDimensionVector);
    isTVz = true;
end
if ~myisfield(Options,'doForceReal') || isempty(Options.doForceReal)
    Options.doForceReal = DEFAULT_DOFORCEREAL;
end

if ~isfield(Options,'fourierTransformType') || isempty(Options.fourierTransformType)
    Options.fourierTransformType = DEFAULT_FTTYPE;
end

if ~isfield(Options,'gradientDimension') || isempty(Options.gradientDimension)
    Options.gradientDimension = DEFAULT_GRADIENTDIMENSION;
end

if ndims(samplingMatrix) < 3 && ~strcmp(Options.gradientDimension,'2d')
    Options.gradientDimension = '2d';
    dispstatus('Detected 2D-data. Switching to x/y-gradients.')
end


if ~myisfield(Options,'muMin') || isempty(Options.muMin)
    Options.muMin = DEFAULT_MUMIN;
end
if ~myisfield(Options,'epsilon') || isempty(Options.epsilon)
    Options.epsilon = DEFAULT_EPSILON;
end
if ~myisfield(Options,'maxitInner') || isempty(Options.maxitInner)
    Options.maxitInner = DEFAULT_MAXITINNER;
end
if ~myisfield(Options,'maxitMu') || isempty(Options.maxitMu)
    Options.maxitMu = DEFAULT_MAXITMU;
end
if ~myisfield(Options,'tolerance') || isempty(Options.tolerance)
    Options.tolerance = DEFAULT_TOLERANCE;
end
if ~myisfield(Options,'toleranceEnergy') || isempty(Options.toleranceEnergy)
    Options.toleranceEnergy = DEFAULT_TOLERANCEENERGY;
end
if ~myisfield(Options,'stopTest') || isempty(Options.stopTest)
    Options.stopTest = DEFAULT_STOPTEST;
end
if ~myisfield(Options,'voxelAspectRatio') || isempty(Options.voxelAspectRatio)
    Options.voxelAspectRatio = [1 1 1];
end

Options.voxelAspectRatio = Options.voxelAspectRatio ./ max(Options.voxelAspectRatio); % required, otherwise poor convergence for preclinical data with low voxel size


% if ~myisfield(Options,'ConeMask') || isempty(Options.ConeMask)
%     error('Cone Mask must be supplied.')
% end
if isfield(Options,'totalEnergy')
    Opts.totalEnergy = Options.totalEnergy;
end
if ~myisfield(Options,'samplingMatrixType') || isempty(Options.samplingMatrixType)
    Options.samplingMatrixType = 'fourier';
end
if ~myisfield(Options,'sparsityType') || isempty(Options.sparsityType)
    error('sparsity type must be supplied.')
end

if ~myisfield(Options,'method') || isempty(Options.method)
    Options.method = DEFAULT_METHOD;
end

if ~myisfield(Options,'U') || isempty(Options.U)
    Options.U = [];
end
if ~myisfield(Options,'supportMask') || isempty(Options.supportMask)
    Options.supportMask = ones(size(samplingMatrix));
end
if isfield(Options,'xplug') 
    Opts.xplug = Options.xplug;
end
if isfield(Options,'isHighAccuracyMode') 
    Opts.isHighAccuracyMode = Options.isHighAccuracyMode;
end
if isfield(Options,'gradientType') 
    Opts.gradientType = Options.gradientType;
else
    Opts.gradientType = DEFAULT_GRADIENTTYPE;
end



switch Options.samplingMatrixType
    case 'fourier'
        handle_samplingMatrix_normal = @(inputVector)(samplingMatrix_normal(inputVector));
        handle_samplingMatrix_transpose = @(inputVector)(samplingMatrix_transpose(inputVector));
        
        handle_samplingMatrix_normal_up = @(inputVector)(samplingMatrix_normal_up(inputVector));
        handle_samplingMatrix_transpose_up = @(inputVector)(samplingMatrix_transpose_up(inputVector));
    otherwise
        error('Specified sampling matrix type currently not supported.')
end


switch Options.sparsityType
    case 'L1'
        Opts.TypeMin = 'L1';
    case 'tv'
%         Opts.grad1 = fftsave(generategradientmatrix1d(solutionDimensionVector,'x'));  % voxelsize in meter
%         Opts.grad2 = fftsave(generategradientmatrix1d(solutionDimensionVector,'y'));
%         Opts.grad3 = fftsave(generategradientmatrix1d(solutionDimensionVector,'z'));
        Opts.TypeMin = 'tv';
    otherwise
        error('Specified sparsity type currently not supported.')
end


Opts.maxiter = Options.maxitInner;
Opts.MaxIntIter = Options.maxitMu;
Opts.TolVar = Options.tolerance;
Opts.TolEnergy = Options.toleranceEnergy;

Opts.gridDimensionVector = solutionDimensionVector;
Opts.GradientMask_x = Options.GradientMask_x;
Opts.GradientMask_y = Options.GradientMask_y;
Opts.GradientMask_z = Options.GradientMask_z;
%Opts.ConeMask = Options.ConeMask;
Opts.stopTest = Options.stopTest;
Opts.U = Options.U;
Opts.isTVx = isTVx;
Opts.isTVy = isTVy;
Opts.isTVz = isTVz;
Opts.fourierTransformType = Options.fourierTransformType;
Opts.gradientDimension = Options.gradientDimension;

Opts.voxelAspectRatio = Options.voxelAspectRatio;
if isfield(Options,'problemMask')
    Opts.problemMask = closemask(Options.problemMask,3);
end

Opts.samplingMatrix = samplingMatrix;


%% call Nesta

switch Options.method
    case 'constrained'
        
          [solutionArray,niter,residuals,outputData,opts] = ...
             NESTA_3d(handle_samplingMatrix_normal,handle_samplingMatrix_transpose,measurementSamples,Options.muMin,Options.epsilon,Opts);
         solutionArray = reshape(solutionArray,solutionDimensionVector);
        
%        cd /home/schweser/tmp/NESTA_v1.1/
%        Opts2.TypeMin = 'tv';
%        Opts2.maxiter = Options.maxitInner;
%        Opts2.TolVar = 1e-5;
%         Opts2.xplug = double(samplingMatrix);
%         Opts2.xplug(samplingMatrix) = measurementSamples;
%         Opts2.xplug(~samplingMatrix) = rand(nnz(~samplingMatrix),1) * 100;
%         Opts2.xplug = Opts2.xplug(:);
%        [solutionArray,niter,residuals,outputData,opts] = ...
%            NESTA(handle_samplingMatrix_normal,handle_samplingMatrix_transpose,measurementSamples,Options.muMin,Options.epsilon,Opts2);%,Opts);
        solutionArray = reshape(solutionArray,solutionDimensionVector);
        
    case 'unconstrained'
        disp('unconstrained minimization')
        if ~myisfield(Options,'maskThresh') || isempty(Options.maskThresh)
            Options.maskThresh = DEFAULT_MASKTHRESH;
            disp(['dipole mask threshold: DEFAULT (' num2str(Options.maskThresh) ')'])
        end

        samplingMatrixMask = abs(samplingMatrix) < Options.maskThresh; 
        samplingMatrix = 1./samplingMatrix;
        samplingMatrix(samplingMatrixMask) = 0;
        Opts.samplingMatrix = samplingMatrix;
        
        
        [solutionArray,niter,residuals,outputData,opts] = ...
            NESTA_UP_3d(handle_samplingMatrix_normal_up,handle_samplingMatrix_transpose_up,measurementSamples,Options.lambda,Options.lipschitzConst,Options.muMin,Opts);
        solutionArray = reshape(solutionArray,solutionDimensionVector);
end
    
if Options.doForceReal
    solutionArray = real(solutionArray); % only to make sure...
end

dispstatus(mfilename(),Options,'functionexit',true)

    %% member functions
    
    % constrained problem
    
    function retVal = samplingMatrix_normal(inVal)

        retVal = fftsave(reshape(inVal,solutionDimensionVector),[],Options.fourierTransformType);
        retVal = retVal(samplingMatrix);

    end
    
    function retVal = samplingMatrix_transpose(inVal)
        
        retVal = zeros(solutionDimensionVector);
        retVal(samplingMatrix) = inVal;
%         retVal = ifftsave(retVal,[],'symmetric'); % does not work for some reason
        retVal = (ifftsave(retVal,[],[],Options.fourierTransformType));
        retVal = retVal(:);
        
    end
    

    % unconstrained problem

    function retVal = samplingMatrix_normal_up(inVal)

        retVal = reshape(inVal,solutionDimensionVector);
        
        retVal = retVal.*Options.supportMask;        % support mask
        
        retVal = fftsave(retVal,[],Options.fourierTransformType);                    % FT
        retVal = retVal.*samplingMatrix;             % inverse dipole matrix
        retVal = retVal(:);
    end

    function retVal = samplingMatrix_transpose_up(inVal)
        
        retVal = zeros(solutionDimensionVector);
        retVal(:) = inVal;
        retVal = retVal.*samplingMatrix;
        retVal = ifftsave(retVal,[],[],Options.fourierTransformType);
        
        retVal = retVal.*Options.supportMask;        % support mask
        
        retVal = retVal(:);
        
    end
end