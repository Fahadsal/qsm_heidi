function [dataArrayExp, maskArray] = prepareforconvolution(dataArrayObj,paddingValue,frameWidth,maskArrayIn)


%PREPAREFORCONVOLUTION Prepares for convolution operations.
%
%   Syntax
%
%   X = PREPAREFORCONVOLUTION(A)
%   X = PREPAREFORCONVOLUTION(A,v)
%   X = PREPAREFORCONVOLUTION(A,v,f)
%   [X,M] = PREPAREFORCONVOLUTION(A,v,f)
%   A = PREPAREFORCONVOLUTION(X,[],[],M)
%
%
%   Description
%
%   PREPAREFORCONVOLUTION(A) prepares N-D data array A for convolution
%   operations. This means: A is zero-padded onto a odd-sized grid. If A is
%   4-D data each 3D-volume will be trated individually.
%
%   PREPAREFORCONVOLUTION(A,v) uses v instead of zeros.
%
%   PREPAREFORCONVOLUTION(A,v,f) adds frame of total size f (in each
%   dimension) to the data matrix (default: f=2xsize(A)).
%
%   dataArray = PREPAREFORCONVOLUTION(A,v,f) returns zero-padded and
%               odd-sized data volume of the input data.
%
%   [X, M] = PREPAREFORCONVOLUTION(A,v,f) returns a
%   zero-padded odd-sized logical volume mask M to countermand the action of this
%   function. The ones of the mask represent the size of the input volume.
%   The zeros are the added frames due to this function.
%
%    A = PREPAREFORCONVOLUTION(X,[],[],M) countermands the preparation
%    operation (that gereated X) using the logical-valued mask M. This mask
%    may be obtained by calling the function with two output variables.
%
%
%
%
%   Function supports in-place calls using A.
%

% F Schweser, mail@ferdinand-schweser.de, 2009/08/03
%
%   Changelog:
%   v1.8 - 2012/03/21 F Schweser    - Now compatible with 4D data
%   v1.7 - 2011/08/08 K Sommer    - bufix: input object is now left unaltered
%   v1.6 - 2011/04/01, F Schweser - Bugfix: logical data
%   v1.6 - 2010/02/24, F Schweser - Input logical --> output logical.
%   v1.5 - 2009/11/03, F Schweser - Bugfix for undomask.
%   v1.4 - 2009/09/10, F Schweser - undoing is now supported.
%   v1.3 - 2009/09/03, A Deistung - output parameter maskArray was added.
%   v1.21 - 2009/09/03, F Schweser - Documentation updated.
%   v1.2 - 2009/08/28, F Schweser - added frameWidth argument
%   v1.1 - 2009/08/12, F Schweser - added paddingValue argument





if nargin < 4 || isempty(maskArrayIn)
    isUndoingCall = false;
else
    if ~isempty(paddingValue) || ~isempty(frameWidth)
        error('Un-doing preparation requires all unrequired arguments to be empty.');
    end
    
    if ~islogical(maskArrayIn)
        error('Un-doing mask must be logical-valued.')
    end
    
    isUndoingCall = true;
end



if nargin < 2
    paddingValue = [];
end


if isobject(dataArrayObj)
    dataArray = dataArrayObj.clone;
    dataArray = dataArray.img;
else
    dataArray = dataArrayObj;
end

dataArray = squeeze(dataArray);

if islogical(dataArray)
    isInputLogical = true;
    dataArray = double(dataArray);
else
    isInputLogical = false;
end



%% main part
if ~isUndoingCall
    % normal preparation part
    maskArray = true(size(dataArray));
    dataArray = makeeven(dataArray,paddingValue);
    maskArray = makeeven(maskArray);

   
    if nargin < 3 || isempty(frameWidth)
        frameWidth = size(dataArray);
    end

    
    dataArray = zeropadding(dataArray,size(dataArray)+frameWidth,paddingValue);
    maskArray = zeropadding(maskArray,size(maskArray)+frameWidth);
    
    dataArray = makeodd(dataArray);
    maskArray = logical(makeodd(maskArray));
    
    if isInputLogical
        dataArray = logical(dataArray);
    end
    
    
else
    % undoing preparation
    
    tmp = zealouscrop(double(maskArrayIn),[],0);
    tmp(logical(tmp)) = dataArray(maskArrayIn);
    dataArray = tmp;
    
    
    if isInputLogical
        dataArray = logical(dataArray);
    end
end




if isobject(dataArrayObj)
    dataArrayExp = dataArrayObj.clone;
    dataArrayExp.img = dataArray;
else
    dataArrayExp = dataArray;
end



end