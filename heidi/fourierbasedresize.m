function outData = fourierbasedresize(inData,desiredSize,fourierType,filterType,filterParam,isFftShift)

%FOURIERBASEDRESIZE Fourier-based resizing.
%
%   FOURIERBASEDRESIZE Resized input data by cropping or zeropadding in the
%   Fourier domain.
%
%
%   Syntax
%
%   FOURIERBASEDRESIZE(A,s)
%   FOURIERBASEDRESIZE(A,s,fourierType,filterType,filterParam,isFftShift)
%
%
%   Description
%
%   FOURIERBASEDRESIZE(A,s) resized A by cropping/padding Fourier-Domain to
%   grid-size s (vector).
%
%   FOURIERBASEDRESIZE(A,s,'2d') performs only slice-wise FT. 
%
%   FOURIERBASEDRESIZE(A,s,[],filterType,filterParam,isFftShift) applies a k-space
%   filter, e.g. to mitigate Gibbs ringing. Following filters are
%   supported (filterType):
%       kaiser  - This filter almost perfectly eliminates Gibbs ringing,
%                 but at the expense of sharpness. Recommended filter
%                 parameter is 3*pi
%       tukey   - This is the standard Tuckey filter that is commonly used
%                 in MRI. Recommended parameter is 20%.
%   isFftShift applies fftshift before and after zeropadding.

%   F Schweser, 2009/08/11, mail@ferdinand-schweser.de
%
%   Changelog:
%
%   2012/08/15 - v1.1 - F Schweser - now also supports upsampling and
%                                    FourierType
%   2012/08/21 - v1.2 - F Schweser - Bugfix no change
%   2012/10/13 - v1.3 - F Schweser - Bugfix: 3D data upsamplin
%   2013/04/12 - v1.4 - F Schweser - Bugfix: Now supports 4D downsampling
%   2015/02/18 - v1.5 - F Schweser - Now supports 5D upsampling
%   2015/11/23 - v1.6 - F Schweser - Now no FT if no resizing.
%   2016/02/05 - v1.7 - F Schweser - Minor bugfix for 4D data.
%   2016/02/08 - v1.8 - F Schweser - New feature filters
%   2017/08/17 - v1.9 - F Schweser - New feature isFftShift

if nargin < 3 || isempty(fourierType)
    fourierType = '3d';
end

if nargin < 4
    filterType = [];
end

if nargin < 6 || isempty(isFftShift)
    isFftShift = false;
end

if nnz(desiredSize(1:3) >= size(squeeze(inData(:,:,:,1)))) == 3
    inSize = size(inData); % upsampling
else
    inSize = desiredSize; % downsampling
end
if ~isempty(filterType)
    switch filterType
        case 'kaiser'
            filter = repmat(kaiser(inSize(1),filterParam),[1 inSize(2) inSize(3)]);
            filter = filter .* permute(repmat(kaiser(inSize(2),filterParam),[1 inSize(1) inSize(3)]),[2 1 3]);
            switch fourierType
                case '3d'
                    filter = filter .* permute(repmat(kaiser(inSize(3),filterParam),[1 inSize(1) inSize(2)]),[2 3 1]);
            end
        case 'tukey'
            filter = repmat(tukeywin(inSize(1),filterParam),[1 inSize(2) inSize(3)]);
            filter = filter .* permute(repmat(tukeywin(inSize(2),filterParam),[1 inSize(1) inSize(3)]),[2 1 3]);
            switch fourierType
                case '3d'
                    filter = filter .* permute(repmat(tukeywin(inSize(3),filterParam),[1 inSize(1) inSize(2)]),[2 3 1]);
            end
        otherwise
            error('Filter type not supported.')
    end
    filter = repmat(filter,[1 1 1 size(inData,4) size(inData,5) size(inData,6) size(inData,7) size(inData,8)]);
end

factor = prod(desiredSize) / numel(inData(:,:,:,1));

if nnz(desiredSize(1:3) >= size(squeeze(inData(:,:,:,1)))) == 3
    if nnz(desiredSize(1:3) == size(squeeze(inData(:,:,:,1)))) == 3
        dispstatus('No resizing performed because input size equals desired size.')
        outData = inData;
    else
        inData = fftsave(inData,[],fourierType);
        if isFftShift
            inData = fftshift(inData);
        end

        % upsampling
        %if size(inData,4) > 1
            outData = zeropadding(inData,[desiredSize(1:3) size(inData,4) size(inData,5) size(inData,6)]);
        %else
        %    outData = zeropadding(inData,desiredSize);
        %end
        
    end
    
    if ~isempty(filterType)
        filter = zeropadding(filter,[desiredSize(1:3) size(inData,4) size(inData,5) size(inData,6)]);
    end
    
else
    %downsampling
    
    inData = fftsave(inData,[],fourierType);
    if isFftShift
        inData = fftshift(inData);
    end

    filterMask = zeropadding(ones(desiredSize(1:3)),size(inData(:,:,:,1)));
    filterMask = repmat(filterMask,[1 1 1 size(inData,4) size(inData,5) size(inData,6) size(inData,7) size(inData,8) size(inData,9)]);
    outData = inData(logical(filterMask));

    
    outData = reshape(outData,[desiredSize(1:3) size(inData,4) size(inData,5) size(inData,6) size(inData,7) size(inData,8) size(inData,9)]);
end

if ~isempty(filterType)
    outData = outData .* filter;
end


if nnz(desiredSize(1:3) == size(squeeze(inData(:,:,:,1)))) ~= 3
    if isFftShift
        inData = ifftshift(inData);
    end
    outData = ifftsave(outData,[],[],fourierType) .* factor;
end


end