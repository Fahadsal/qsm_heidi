function [x, y, z, r] = mesh3d(dimension,voxel_size,isKSpace)

%MESH3D    creates 3D-rooms with centered coordinates of x, y, z and radius
%
%   Syntax
%
%   [x,y,z,r] = MESH3D(dim)
%   [x,y,z,r] = MESH3D(dim,aspect)
%   [x,y,z,r] = MESH3D(dim,aspect,true) 
%   r = MESH3D(...)
%
%
%   Desription
%
%   [x,y,z,r] = MESH3D(dim) returns four 3D matrices with x-, y-, z-, and
%   radius-coordinates, respectively. The dim-variable (scalar or 3-D vector)
%   specifies how many voxels in each direction are created. In case of
%   scalar input the value is used for all three dimensions.
%
%   [x,y,z,r] = MESH3D(dim,aspect) uses aspect ratio of voxels specified by
%   aspect-argument (default = [1 1 1]). Aspect ratio corresponds to voxel
%   size.
%
%   [x,y,z,r] = MESH3D(dim,aspect,true) returns the corresponding k-space coordinates (default: false). 
%
%   r = MESH3D(...) returns radius coordinate only.
%
%   Example:
%       [x, y, z, r] = MESH3D(121) returns three 121-by-121-by-121-matrixes
%       containing 121 121x121 layers with values from -60 to 60 in x, y
%       and z direction and a fourth 121-by-121-by-121 matrix containing
%       the distance from each voxel to the center in voxels
%
%   See also: NDGRID


%   BW Lehr

% Changelog:
%
%   v2.2  - 2013/08/16 - F Schweser - New feature: isKSpace
%   v2.11 - 2009/09/08 - F Schweser - Error handling for aspect ratio
%   argument.
%   v2.1 - 2009/09/04 - F Schweser - Bugfix: Scalar input.
%   v.2 - 2009/09/04 - F Schweser - Support for anisotropic voxels added. Calling
%   of function changed to one paramter for dimension. Documentation
%   improved and adapted.


%% check input arguments
if nargin < 1 || isempty(dimension)
    error('At least one imput argument required.')
end

if nargin < 3 || isempty(isKSpace) 
    isKSpace = false;
end

if size(dimension,2)
    dimension = [dimension dimension dimension];
end

dim = (dimension-1)/2;
nDimension = size(dim, 2);
if nargin < 2 || isempty(voxel_size)  
    voxel_size = ones(nDimension,1);
elseif sum(voxel_size < 1) && ~isKSpace
    error('Aspect ratio must not contain values below 1.')
end


%% init 
if nDimension < 3
    dim(2) = dim;
    dim(3) = dim(1);
end


%% main part

[x, y, z] = ndgrid(-dim(1):dim(1), -dim(2):dim(2), -dim(3):dim(3));


% account for aspect ratio
if ~isKSpace
    x = x .* voxel_size(1);
    y = y .* voxel_size(2);
    z = z .* voxel_size(3);
else
    FOVx = voxel_size(1)*dimension(1);
    FOVy = voxel_size(2)*dimension(2);
    FOVz = voxel_size(3)*dimension(3);
    x = x ./ FOVx;
    y = y ./ FOVy;
    z = z ./ FOVz;
end  

r = (x.^2 + y.^2 + z.^2).^(1/2);

if nargout == 1
    x = r;
end

end