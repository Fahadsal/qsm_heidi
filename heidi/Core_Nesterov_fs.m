function [xk,niter,residuals,outputData,opts,isConverged] = Core_Nesterov_fs(...
    A,At,b,mu,delta,opts)
% [xk,niter,residuals,outputData,opts] =Core_Nesterov(A,At,b,mu,delta,opts)
%
% Solves a L1 minimization problem under a quadratic constraint using the
% Nesterov algorithm, without continuation:
%
%     min_x || U x ||_1 s.t. ||y - Ax||_2 <= delta
%
% If continuation is desired, see the function NESTA.m
%
% The primal prox-function is also adapted by accounting for a first guess
% xplug that also tends towards x_muf
%
% The observation matrix A is a projector
%
% Inputs:   A and At - measurement matrix and adjoint (either a matrix, in which
%               case At is unused, or function handles).  m x n dimensions.
%           b   - Observed data, a m x 1 array
%           muf - The desired value of mu at the last continuation step.
%               A smaller mu leads to higher accuracy.
%           delta - l2 error bound.  This enforces how close the variable
%               must fit the observations b, i.e. || y - Ax ||_2 <= delta
%               If delta = 0, enforces y = Ax
%               Common heuristic: delta = sqrt(m + 2*sqrt(2*m))*sigma;
%               where sigma=std(noise).
%           opts -
%               This is a structure that contains additional options,
%               some of which are optional.
%               The fieldnames are case insensitive.  Below
%               are the possible fieldnames:
%
%               opts.isHighAccuracyMode this switch chooses the high
%                 accuracy mode that performs more iterations and results in
%                 substantially better results. Plots in HEIDI paper were
%                 calculated in this mode, for example. (default: false)
%               opts.xplug - the first guess for the primal prox-function, and
%                 also the initial point for xk.  By default, xplug = At(b)
%               opts.U and opts.Ut - Analysis/Synthesis operators
%                 (either matrices of function handles).
%               opts.normU - if opts.U is provided, this should be norm(U)
%               opts.maxiter - max number of iterations in an inner loop.
%                 default is 10,000
%               opts.TolVar - tolerance for the stopping criteria
%               opts.stopTest - which stopping criteria to apply
%                   opts.stopTest == 1 : stop when the relative
%                       change in the objective function is less than
%                       TolVar
%                   opts.stopTest == 2 : stop with the l_infinity norm
%                       of difference in the xk variable is less
%                       than TolVar
%               opts.TypeMin - if this is 'L1' (default), then
%                   minimizes a smoothed version of the l_1 norm.
%                   If this is 'tv', then minimizes a smoothed
%                   version of the total-variation norm.
%                   The string is case insensitive.
%               opts.Verbose - if this is 0 or false, then very
%                   little output is displayed.  If this is 1 or true,
%                   then output every iteration is displayed.
%                   If this is a number p greater than 1, then
%                   output is displayed every pth iteration.
%               opts.fid - if this is 1 (default), the display is
%                   the usual Matlab screen.  If this is the file-id
%                   of a file opened with fopen, then the display
%                   will be redirected to this file.
%               opts.errFcn - if this is a function handle,
%                   then the program will evaluate opts.errFcn(xk)
%                   at every iteration and display the result.
%                   ex.  opts.errFcn = @(x) norm( x - x_true )
%               opts.outFcn - if this is a function handle,
%                   then then program will evaluate opts.outFcn(xk)
%                   at every iteration and save the results in outputData.
%                   If the result is a vector (as opposed to a scalar),
%                   it should be a row vector and not a column vector.
%                   ex. opts.outFcn = @(x) [norm( x - xtrue, 'inf' ),...
%                                           norm( x - xtrue) / norm(xtrue)]
%               opts.AAtinv - this is an experimental new option.  AAtinv
%                   is the inverse of AA^*.  This allows the use of a
%                   matrix A which is not a projection, but only
%                   for the noiseless (i.e. delta = 0) case.
%                   If the SVD of A is U*S*V', then AAtinv = U*(S^{-2})*U'.
%               opts.USV - another experimental option.  This supercedes
%                   the AAtinv option, so it is recommended that you
%                   do not define AAtinv.  This allows the use of a matrix
%                   A which is not a projection, and works for the
%                   noisy ( i.e. delta > 0 ) case.
%                   opts.USV should contain three fields:
%                   opts.USV.U  is the U from [U,S,V] = svd(A)
%                   likewise, opts.USV.S and opts.USV.V are S and V
%                   from svd(A).  S may be a matrix or a vector.
%               opts.isTVx(y/z) - switch for plain total variation in
%                   selected dimension. (default: false)
%                   Warning: Currently works only if all three options set
%                   to true.
%		opts.gradientType - defindes the gradient type for the TV-mode
%		    see DISCRETEGRADIENT for details.
%
%  Outputs:
%           xk  - estimate of the solution x
%           niter - number of iterations
%           residuals - first column is the residual at every step,
%               second column is the value of f_mu at every step
%           outputData - a matrix, where each row r is the output
%               from opts.outFcn, if supplied.
%           opts - the structure containing the options that were used
%
% Written by: Jerome Bobin, Caltech
% Email: bobin@acm.caltech.edu
% Created: February 2009
% Modified: May 2009, Jerome Bobin and Stephen Becker, Caltech
% Modified: Nov 2009, Stephen Becker
%
% NESTA Version 1.1
%   See also NESTA
%
%
%   Changelog
%
%   v1.1 - 2011/03/24 - K Sommer - stopping criterion added & support for three different gradient masks
%   v1.2 - 2011/04/01 - K Sommer - ouput of difference (energy of calculated susceptibility - estimated energy 'opts.E') added
%   v1.3 - 2011/04/04 - F Schweser - Bugfix: opts.E not set by default
%   v1.31 - 2011/06/23 - K Sommer - Opts.E renamed to Opts.totalEnergy
%   v1.4 - 2011/09/19 - F Schweser - added isTVx/y/z options
%   v1.5 - 2011/11/02 - F Schweser - Bugfix: isConverged not assigned
%   v1.6 - 2011/11/21 - F Schweser - Bugfix: Lmu now determined
%                                   automatically from voxelaspectratio
%   v1.7 - 2011/12/22 - F Schweser - Minor adaption for new 2voxel gradient
%   v1.8 - 2012/01/03 - F Schweser - Memory efficiency improved + code
%                                    readability improved + execution efficiency slightly imrpoved
%   v1.9 - 2012/02/14 - F Schweser - TolEnergy convergence criterion
%                                    included
%                                    + now energy calculation only inside
%                                    problem Masksss
%   v2.0 - 2012/03/26 - F Schweser - Several substantial changes regarding convergence test 2 and 3.
%		                     These changes result in over-iteration. The normal NESTA-stopping
%				     was removed because it seemed that it resulted in non-smoothed
%			             plots for the HEIDI-manuscript. This should potentially be 
%				     recovered in a later version for improved performance.
%				     + now uses gradientType-field
%   v2.1 - 2012/04/18 - F Schweser - added isHighAccuracyMode partially
%                                    used in HEIDI paper
%   v2.2 - 2012/10/01 - F Schweser - Mempory performance optimization
%   v2.3 - 2012/12/04 - F Schweser - Now supports 2D data.

isConverged = false;
if ~isfield(opts,'totalEnergy')
    opts.totalEnergy = 1/eps;
end

tvUnconstrained(1) = 0;
previousEnergy = 0;

%---- Set defaults
% opts = [];
fid = setOpts('fid',1);
    function printf(varargin), fprintf(fid,varargin{:}); end
maxiter = setOpts('maxiter',10000,0);
TolVar = setOpts('TolVar',1e-5);
TolEnergy = setOpts('TolEnergy',1e-5);
TypeMin = setOpts('TypeMin','L1');
Verbose = setOpts('Verbose',true);
errFcn = setOpts('errFcn',[]);
outFcn = setOpts('outFcn',[]);
stopTest = setOpts('stopTest',3);
if strcmpi(TypeMin,'L1')  
    U = setOpts('U', @(x) x );
    if ~isa(U,'function_handle')
        Ut = setOpts('Ut',[]);
    else
        Ut = setOpts('Ut', @(x) x );
    end
end
xplug = setOpts('xplug',[]);
normU = setOpts('normU',1);

if delta < 0, error('delta must be greater or equal to zero'); end

if isa(A,'function_handle')
    Atfun = At;
    Afun = A;
else
    Atfun = @(x) A'*x;
    Afun = @(x) A*x;
end
Atb = Atfun(b);

AAtinv = setOpts('AAtinv',[]);
USV = setOpts('USV',[]);
if ~isempty(USV)
    if isstruct(USV)
        Q = USV.U;  % we can't use "U" as the variable name
        % since "U" already refers to the analysis operator
        S = USV.S;
        if isvector(S), s = S; S = diag(s);
        else s = diag(S); end
        V = USV.V;
    else
        error('opts.USV must be a structure');
    end
    if isempty(AAtinv)
        AAtinv = Q*diag( s.^(-2) )*Q';
    end
end
% --- for A not a projection (experimental)
if ~isempty(AAtinv)
    if isa(AAtinv,'function_handle')
        AAtinv_fun = AAtinv;
    else
        AAtinv_fun = @(x) AAtinv * x;
    end
    
    AtAAtb = Atfun( AAtinv_fun(b) );
    
else
    % We assume it's a projection
    AtAAtb = Atb;
    AAtinv_fun = @(x) x;
end

if isempty(xplug)
    xplug = AtAAtb;
end




if isempty(opts.GradientMask_x)
    opts.GradientMask_x = ones(opts.gridDimensionVector);
end
if isempty(opts.GradientMask_y)
    opts.GradientMask_y = ones(opts.gridDimensionVector);
end
if isempty(opts.GradientMask_z)
    opts.GradientMask_z = ones(opts.gridDimensionVector);
end

if ~isfield(opts,'isTVx') || isempty(opts.isTVx)
    opts.isTVx = false;
end
if ~isfield(opts,'isTVy') || isempty(opts.isTVy)
    opts.isTVy = false;
end
if ~isfield(opts,'isTVz') || isempty(opts.isTVz)
    opts.isTVz = false;
end

if ~isfield(opts,'isHighAccuracyMode') || isempty(opts.isHighAccuracyMode)
    opts.isHighAccuracyMode = false;
end


%---- Initialization
N = length(xplug);
wk = zeros(N,1);
xk = xplug;


%---- Init Variables
Ak= 0;
Lmu = normU/mu;
%yk = xk;
%zk = xk;
fmean = realmin/10;
OK = 0;
n = floor(sqrt(N));


%---- Computing Atb
%Atb = Atfun(b);
Axk = Afun(xk);% only needed if you want to see the residuals
% Axplug = Axk;


%---- TV Minimization
if strcmpi(TypeMin,'TV')
    % START REMOVED BY FS
    %    Lmu = 8*Lmu;
    %     Dv = spdiags([reshape([-ones(n-1,n); zeros(1,n)],N,1) ...
    %         reshape([zeros(1,n); ones(n-1,n)],N,1)], [0 1], N, N);
    %     Dh = spdiags([reshape([-ones(n,n-1) zeros(n,1)],N,1) ...
    %         reshape([zeros(n,1) ones(n,n-1)],N,1)], [0 n], N, N);
    %     D = sparse([Dh;Dv]);
    % STOP REMOVED BY FS
    
    % start added by fs
    %Lmu = Lmu * 12;
    Lmu = Lmu * ( ...
        (abs(-1/opts.voxelAspectRatio(1)) + abs(1/opts.voxelAspectRatio(1))).^2 + ...
        (abs(-1/opts.voxelAspectRatio(2)) + abs(1/opts.voxelAspectRatio(2))).^2 + ...
        (abs(-1/opts.voxelAspectRatio(3)) + abs(1/opts.voxelAspectRatio(3))).^2 ...
        );
    %dispstatus(['L-mu: ' num2str(Lmu)])
    % times squared matrix norm of gradient matrices (2-norm matrix norm is maximum singular value)
    % We have calculated svd(D).^2 (see above with n=100) which was 4 for 1-D
    % gradient, 8 for 2-D gradient, and 12 for 3-D gradient.
    % stop added by fs
    %
    %   if Lmu-Factor is wrong, moiree or other artifacts may occur in
    %   solution.
end


Lmu1 = 1/Lmu;
% SLmu = sqrt(Lmu);
% SLmu1 = 1/sqrt(Lmu);
lambdaY = 0;
lambdaZ = 0;

%---- setup data storage variables
[DISPLAY_ERROR, RECORD_DATA] = deal(false);
outputData = deal([]);
residuals = zeros(maxiter,2);
if ~isempty(errFcn), DISPLAY_ERROR = true; end
if ~isempty(outFcn) && nargout >= 4
    RECORD_DATA = true;
    outputData = zeros(maxiter, size(outFcn(xplug),2) );
end

for k = 0:maxiter-1,
    
    %---- Dual problem
    clear df zk yk xkp cp Acp AtAcp Ayk Azk
    if strcmpi(TypeMin,'L1')  
        [df,fx] = Perform_L1_Constraint(xk,mu,U,Ut);
    end
    
    %if strcmpi(TypeMin,'TV')  [df,fx] = Perform_TV_Constraint(xk,mu,Dv,Dh,D,U,Ut);end
    if strcmpi(TypeMin,'TV')  
        [df,fx,x_reshape,tvUnconstrained] = Perform_TV_Constraint(xk,mu,opts,tvUnconstrained);
    end %changed by FS
    
    %---- Primal Problem
    
    %---- Updating yk
    
    %
    % yk = Argmin_x Lmu/2 ||x - xk||_l2^2 + <df,x-xk> s.t. ||b-Ax||_l2 < delta
    % Let xp be sqrt(Lmu) (x-xk), dfp be df/sqrt(Lmu), bp be sqrt(Lmu)(b- Axk) and deltap be sqrt(Lmu)delta
    % yk =  xk + 1/sqrt(Lmu) Argmin_xp 1/2 || xp ||_2^2 + <dfp,xp> s.t. || bp - Axp ||_2 < deltap
    %
    
    
    cp = xk - 1/Lmu*df;  % this is "q" in eq. (3.7) in the paper
    
    Acp = Afun( cp );
    if ~isempty(AAtinv) && isempty(USV)
        AtAcp = Atfun( AAtinv_fun( Acp ) );
    else
        AtAcp = Atfun( Acp );
    end
    
    residuals(k+1,1) = norm( b-Axk);    % the residual
    residuals(k+1,2) = fx;              % the value of the objective
    %--- if user has supplied a function, apply it to the iterate
    if RECORD_DATA
        outputData(k+1,:) = outFcn(xk);
    end
    
    if delta > 0
        if ~isempty(USV)
            % there are more efficient methods, but we're assuming
            % that A is negligible compared to U and Ut.
            % Here we make the change of variables x <-- x - xk
            %       and                            df <-- df/L
            dfp = -Lmu1*df;  Adfp = -(Axk - Acp);
            bp = b - Axk;
            deltap = delta;
            % Check if we even need to project:
            if norm( Adfp - bp ) < deltap
                lambdaY = 0;  %projIter = 0;
                % i.e. projection = dfp;
                yk = xk + dfp;
                Ayk = Axk + Adfp;
            else
                lambdaY_old = lambdaY;
                [projection,~,lambdaY] = fastProjection(Q,S,V,dfp,bp,...
                    deltap, .999*lambdaY_old );
                if lambdaY > 0, disp('lambda is positive!'); keyboard; end
                yk = xk + projection;
                Ayk = Afun(yk);
                % DEBUGGING
                %                 if projIter == 50
                %                     fprintf('\n Maxed out iterations at y\n');
                %                     keyboard
                %                 end
            end
        else
            lambda = max(0,Lmu*(norm(b-Acp)/delta - 1));gamma = lambda/(lambda + Lmu);
            yk = lambda/Lmu*(1-gamma)*Atb + cp - gamma*AtAcp;
            % for calculating the residual, we'll avoid calling A()
            % by storing A(yk) here (using A'*A = I):
            Ayk = lambda/Lmu*(1-gamma)*b + Acp - gamma*Acp;
        end
    else
        % if delta is 0, the projection is simplified:
        yk = AtAAtb + cp - AtAcp;
        Ayk = b;
    end
    
    % DEBUGGING
    %     if norm( Ayk - b ) > (1.05)*delta
    %         fprintf('\nAyk failed projection test\n');
    %         keyboard;
    %     end
    
    %--- Stopping criterion
    %qp = abs(fx - mean(fmean))/mean(fmean);
%     if numel(fmean) > 1
%         qp = abs(fx - fmean(2))/fmean(2); % changed by FS (10/02/12) because seems to be more intuitive
%     else
%         qp = 1;
%     end
    qp = abs(fx - mean(fmean))/mean(fmean);
    switch stopTest
        case 1
            if opts.isHighAccuracyMode
                if qp <= TolVar; 
                    break;
                end
            else
                if qp <= TolVar && OK; break;end                                     
                if qp <= TolVar && ~OK; OK=1; end    
            end

        case 2
            % look at the l_inf change from previous iterate
            if k >= 1
                actualChange = norm( x_reshape(opts.problemMask) - x_reshapeOld(opts.problemMask), 'inf' );
                dispstatus(['    || Maximum change: ' num2str(actualChange)],[],'iteration');
                if actualChange <= TolVar
                    break
                end
            end
            x_reshapeOld = x_reshape;
        case 3
            %if norm(xk(opts.problemMask(:)))^2 > opts.totalEnergy   % added by ks
            tmp = fftsave(x_reshape.*opts.problemMask);
            currentEnergy = sum(abs(tmp(~opts.samplingMatrix)).^2)/numel(x_reshape); % only outside of energySamplingMatrix!
            if k == 0
               isActiveEnergyStoppingCriterion = false;
            end
            if currentEnergy > opts.totalEnergy   % FS: skip transition domain during energy calculation because this region is heavily corrupted by noise
                if ~isActiveEnergyStoppingCriterion
                    % this is skipped for the first iterations because when
                    % the cone is very small, the masking concolves in so much
                    % energy that the condition is fullfilled.
                    dispstatus('Energy exceeded but we will continue because we have almost not begun...',[],'iteration');
                    isConverged = false;
                else
                    isConverged = true;
                    dispstatus('Energy exceeded. We are done, buddy!',[],'iteration');
                    break
                end
            else
                isConverged = false;
                isActiveEnergyStoppingCriterion = true; % once the energy is lower than the max, the energy-stopping criterion is activated
            end
            
            % plus: look at the relative change in function value
            if qp <= TolVar %&& OK % removed by FS 2012/02/10
                dispstatus('Relative change of L1-term is too small. We should try out better L1-approximation...',[],'iteration');
                
                break;
            end
            
            relativeEnergyChange = abs(previousEnergy-currentEnergy)/opts.totalEnergy;
            if relativeEnergyChange <= TolEnergy && k > 2
                dispstatus('Relative change of energy is too small. We should try out better L1-approximation...',[],'iteration');                
                break;
            end
            %if qp <= TolVar && ~OK; OK=1; end % removed by FS 2012/02/10
            previousEnergy = currentEnergy;
    end
%     if k<2
%         fmean = fx;
%     else
        fmean = [fx,fmean];
        if (length(fmean) > 10) 
            fmean = fmean(1:10);
        end
%    end
    
    if stopTest == 3
        Diff = opts.totalEnergy - currentEnergy;
        
        %    printf('Diff: %3d \n',Diff);
        if k+1 > 1
            dispstatus(['    || Remaining energy: ' num2str(round(Diff/opts.totalEnergy*100*100)/100) '% (Current decay: ' num2str(round(relativeEnergyChange*100*100)/100) '%)'],[],'iteration');
            timeUntilLastTic = toc;
            energyDecayPerStep = relativeEnergyChange * opts.totalEnergy;
            remainingSteps = Diff / energyDecayPerStep; 
            dispstatus(['    || Estimated time to convergence: ' num2str(round(remainingSteps*timeUntilLastTic/60*10)/10) ' min. (' num2str(round(remainingSteps)) ' steps).'],[],'iteration');
        end
        tic;
    end
    
    %--- Updating zk
    
    apk =0.5*(k+1);
    Ak = Ak + apk;
    tauk = 2/(k+3);
    
    wk =  apk*df + wk;
    
    %
    % zk = Argmin_x Lmu/2 ||b - Ax||_l2^2 + Lmu/2||x - xplug ||_2^2+ <wk,x-xk>
    %   s.t. ||b-Ax||_l2 < delta
    %
    
    cp = xplug - 1/Lmu*wk;
    
    Acp = Afun( cp );
    if ~isempty( AAtinv ) && isempty(USV)
        AtAcp = Atfun( AAtinv_fun( Acp ) );
    else
        AtAcp = Atfun( Acp );
    end
    
    if delta > 0
        if ~isempty(USV)
            % Make the substitution wk <-- wk/K
            
            %             dfp = (xplug - Lmu1*wk);  % = cp
            %             Adfp= (Axplug - Acp);
            dfp = cp; Adfp = Acp;
            bp = b;
            deltap = delta;
            %             dfp = SLmu*xplug - SLmu1*wk;
            %             bp = SLmu*b;
            %             deltap = SLmu*delta;
            
            % See if we even need to project:
            if norm( Adfp - bp ) < deltap
                zk = dfp;
                Azk = Adfp;
            else
                [projection,~,lambdaZ] = fastProjection(Q,S,V,dfp,bp,...
                    deltap, .999*lambdaZ );
                if lambdaZ > 0, disp('lambda is positive!'); keyboard; end
                zk = projection;
                %             zk = SLmu1*projection;
                Azk = Afun(zk);
                
                % DEBUGGING:
                %                 if projIter == 50
                %                     fprintf('\n Maxed out iterations at z\n');
                %                     keyboard
                %                 end
            end
        else
            lambda = max(0,Lmu*(norm(b-Acp)/delta - 1));gamma = lambda/(lambda + Lmu);
            zk = lambda/Lmu*(1-gamma)*Atb + cp - gamma*AtAcp;
            % for calculating the residual, we'll avoid calling A()
            % by storing A(zk) here (using A'*A = I):
            Azk = lambda/Lmu*(1-gamma)*b + Acp - gamma*Acp;
        end
    else
        % if delta is 0, this is simplified:
        zk = AtAAtb + cp - AtAcp;
        Azk = b;
    end
    
    % DEBUGGING
    %     if norm( Ayk - b ) > (1.05)*delta
    %         fprintf('\nAzk failed projection test\n');
    %         keyboard;
    %     end
    
    %--- Updating xk
    
    xkp = tauk*zk + (1-tauk)*yk;
    xold = xk;
    xk=xkp;
    Axk = tauk*Azk + (1-tauk)*Ayk;
    
    if ~mod(k,10), Axk = Afun(xk); end   % otherwise slowly lose precision
    % DEBUG
    %     if norm(Axk - Afun(xk) ) > 1e-6, disp('error with Axk'); keyboard; end
    
    %--- display progress if desired
    if ~mod(k+1,Verbose )
        if k+1 < 10
            spacing = ' ';
        else
            spacing = '';
        end
        dispstatus(['#' num2str(k+1) spacing ' -- Objective change ' num2str(round(qp*100*100)/100) '% // Objective: ' num2str(fx) ' // Fidelity: ' num2str(residuals(k+1,1))],[],'iteration');
            
        %--- if user has supplied a function to calculate the error,
        % apply it to the current iterate and dislay the output:
        if DISPLAY_ERROR, printf(' ~ Error: %.2e',errFcn(xk)); end
        %printf('\n');
    end
    if abs(fx)>1e20 || abs(residuals(k+1,1)) >1e20 || isnan(fx)
        error('Nesta: possible divergence or NaN.  Bad estimate of ||A''A||?');
    end
end

niter = k+1;

%-- truncate output vectors
residuals = residuals(1:niter,:);
if RECORD_DATA,     outputData = outputData(1:niter,:); end



%---- internal routine for setting defaults
    function [var,userSet] = setOpts(field,default,mn,mx)
        var = default;
        % has the option already been set?
        if ~isfield(opts,field)
            % see if there is a capitalization problem:
            names = fieldnames(opts);
            for i = 1:length(names)
                if strcmpi(names{i},field)
                    opts.(field) = opts.(names{i});
                    opts = rmfield(opts,names{i});
                    break;
                end
            end
        end
        
        if isfield(opts,field) && ~isempty(opts.(field))
            var = opts.(field);  % override the default
            userSet = true;
        else
            userSet = false;
        end
        
        % perform error checking, if desired
        if nargin >= 3 && ~isempty(mn)
            if var < mn
                printf('Variable %s is %f, should be at least %f\n',...
                    field,var,mn); error('variable out-of-bounds');
            end
        end
        if nargin >= 4 && ~isempty(mx)
            if var > mx
                printf('Variable %s is %f, should be at least %f\n',...
                    field,var,mn); error('variable out-of-bounds');
            end
        end
        opts.(field) = var;
    end


end %% end of main Core_Nesterov routine


%%%%%%%%%%%% PERFORM THE L1 CONSTRAINT %%%%%%%%%%%%%%%%%%

function [df,fx,val,uk] = Perform_L1_Constraint(xk,mu,U,Ut)

if isa(U,'function_handle')
    uk = U(xk);
else
    uk = U*xk;
end
fx = uk;

uk = uk./max(mu,abs(uk));
val = real(uk'*fx);
fx = real(uk'*fx - mu/2*norm(uk)^2);

if isa(Ut,'function_handle')
    df = Ut(uk);
else
    df = U'*uk;
end
end

%%%%%%%%%%%% PERFORM THE TV CONSTRAINT %%%%%%%%%%%%%%%%%%

%function [df,fx] = Perform_TV_Constraint(xk,mu,Dv,Dh,D,U,Ut)
function [df,fx,x_reshape,tvUnconstrained] = Perform_TV_Constraint(xk,mu,opts,tvUnconstrained) %changed by FS

% Note: This function requires a lot of RAM. Optimization of this function
% was done with respect to memory requirements, rather than CPU efficiency.

x_reshape = reshape(xk,opts.gridDimensionVector);

% calculate gradient of x
if ~(opts.isTVx && opts.isTVy && opts.isTVz)
    D1x = discretegradient(x_reshape,'x',opts.voxelAspectRatio(1),opts.gradientType);
    D2x = discretegradient(x_reshape,'y',opts.voxelAspectRatio(2),opts.gradientType);
    D1x = opts.GradientMask_x .* D1x;
    D2x = opts.GradientMask_y .* D2x;
    
    if ~strcmp(opts.gradientDimension,'2d')
        D3x = discretegradient(x_reshape,'z',opts.voxelAspectRatio(3),opts.gradientType);
        D3x = opts.GradientMask_z .* D3x;
    end
else
    D1x = discretegradient(x_reshape,'x',opts.voxelAspectRatio(1),opts.gradientType);
    D2x = discretegradient(x_reshape,'y',opts.voxelAspectRatio(2),opts.gradientType);
    if ~strcmp(opts.gradientDimension,'2d')
        D3x = discretegradient(x_reshape,'z',opts.voxelAspectRatio(3),opts.gradientType);
    end
end



% calculate u_a depending on the norm of the gradient
if ~strcmp(opts.gradientDimension,'2d')
    w = max(mu,sqrt(abs(D1x(:)).^2+abs(D2x(:)).^2+abs(D3x(:)).^2)); % page 31 (u_a)
else
    w = max(mu,sqrt(abs(D1x(:)).^2+abs(D2x(:)).^2)); % page 31 (u_a)
end
wR = reshape(w,opts.gridDimensionVector);
D1x = D1x ./ wR;
D2x = D2x ./ wR;

if ~strcmp(opts.gradientDimension,'2d')
    D3x = D3x ./ wR;
end
clear wR
%Dx = [D1x(:);D2x(:);D3x(:)]; % this is D*x=[D1,D2,D3]* *x (see page 29)


if ~strcmp(opts.gradientDimension,'2d')
    u = [D1x(:);D2x(:);D3x(:)]'; % this is u_mu
else
    u = [D1x(:);D2x(:)]'; % this is u_mu
end

normUsquare = norm(u)^2;

if ~strcmp(opts.gradientDimension,'2d')
    semiNorm = u*[D1x(:).*w;D2x(:).*w;D3x(:).*w];  % u'*Dx;
else
    semiNorm = u*[D1x(:).*w;D2x(:).*w];  % u'*Dx;
end
clear u
clear w



% calculate grad f_mu(x) (page 31)
dfPattern = discretegradient(opts.GradientMask_x .*D1x,'x',opts.voxelAspectRatio(1),opts.gradientType,true);
clear D1x

dfPattern = dfPattern + discretegradient(opts.GradientMask_y .*D2x,'y',opts.voxelAspectRatio(2),opts.gradientType,true);
clear D2x

if ~strcmp(opts.gradientDimension,'2d')
    dfPattern = dfPattern + discretegradient(opts.GradientMask_z .*D3x,'z',opts.voxelAspectRatio(3),opts.gradientType,true);
    clear D3x
end
% eventuell müsste man hier auch ne transpost Gradient-Mask verwenden,


df = dfPattern(:);

% calculate f_mu
fx = real(semiNorm - mu/2 * normUsquare); % FS: page 31 (top)

end

