function [dataArray, IterationDetails, residualArray, problemMaskOne, gradientMaskReturn, anisotropyComponent,dataArrayPreConeProc,ConeMask,GradientMaskResidual_x,phaseDiscrepancy] = computesusceptibility_heidi(suscInterim,dataArray, Options, problemMask,ConstraintMask,spatialWeightingMatrix,anisotropyAngleArray,anisotropyMask)

%COMPUTESUSCEPTIBILITY Inverts the magnetic field to its sources.
%
%   Computes source distribution from relative residual field.
%
%
%   Syntax
%
%   x = COMPUTESUSCEPTIBILITY(A)
%   x = COMPUTESUSCEPTIBILITY(A,Options)
%   x = COMPUTESUSCEPTIBILITY(A,Options,M)
%   x = COMPUTESUSCEPTIBILITY(A,Options,M,C)
%   x = COMPUTESUSCEPTIBILITY(A,Options,M,C,D)
%   x = COMPUTESUSCEPTIBILITY(A,Options,M,C,D,E,F)
%   [x, Details] = COMPUTESUSCEPTIBILITY(...)
%   [x, Details, y] = COMPUTESUSCEPTIBILITY(...)
%   [x, Details, y, N] = COMPUTESUSCEPTIBILITY(...)
%   [x, Details, y, N, a] = COMPUTESUSCEPTIBILITY(...)
%
%
%
%   Description
%
%   COMPUTESUSCEPTIBILITY(A) computes a susceptibility distribution
%   based on relative difference field A. A must be a real-valued NxMxPxQ
%   data array with Q relative-difference-fields (rdf). If Q > 1
%   specification of corresponding rotational axes and angles
%   (Options-struct)
%   is required.
%
%   COMPUTESUSCEPTIBILITY(A,Options) uses the options that are defined in
%   the struct Options für the algorithm.
%
%   COMPUTESUSCEPTIBILITY(A,Options,M) uses only voxels of A that
%   are defined by the NxMxP logical array M.
%
%   COMPUTESUSCEPTIBILITY(A,Options,M,C) puts constraints regions that
%   are defined by the struct C. C may contain fields 'susceptibility' and
%   'residual'. The fields must be NxMxPxQ logical matrices that define the
%   Q regions to be constrained. Constrained regions will have constant
%   susceptibility and rotation-invariant shift, respectively.
%
%   COMPUTESUSCEPTIBILITY(A,Options,M,C,D) uses the real valued NxMxP spatial
%   weighting matrix D.
%
%   COMPUTESUSCEPTIBILITY(A,Options,M,C,D,E,F) uses anisotropy information for
%   inversion. The real valued NxMxPxQ matrix E contains for each voxel and
%   each orientation the angle of the anisotropy with respect to the
%   magnetic field. The logical mask F specifies the voxels to be used for
%   calculation of the anisotropy component (e.g., FA > 0.6).
%
%   [x, Details] = COMPUTESUSCEPTIBILITY(...) also returns a
%   struct with details about the iteration.
%
%   [x, Details, y] = COMPUTESUSCEPTIBILITY(...) returns the
%   rotation-invariant shift vector y in case residual weighting was activated (see Options).
%
%   [x, Details, y, N] = COMPUTESUSCEPTIBILITY(...) returns also the
%   mask N where susceptibility may be regarded as reliable.
%
%   [x, Details, y, N, G] = COMPUTESUSCEPTIBILITY(...) returns also the
%   gradient masks (cell).
%
%   [x, Details, y, N, G, a] = COMPUTESUSCEPTIBILITY(...) returns also the
%   anisotropy vector a in case anisotropy information was supplied.
%
%   [x, Details, y, N, G, a, P] = COMPUTESUSCEPTIBILITY(...) returns also the
%   computed susceptibility before correction of the cone in k-space.
%
%   [x, Details, y, N, G, a] = COMPUTESUSCEPTIBILITY(...) returns also the
%   mask which defines the region in k-space which is corrected in case of
%   Post-processing of the cone.
%
%
%   The following Option-fields of are supported:
%
%
%       solvingType   - String which specifies type of solver used for calculation of
%                       Susc-Map. The following types are currently
%                       supported:
%                           ''  (default)
%                           Uses LSQR-solver which solves the equation system in the
%                           spatial domain in a least-squares-sense
%
%                           'InverseFiltering'
%                           Solves the problem directly in Fourier-space
%                           and sets voxels in dipole-cone to
%                           zero. See Options-field
%                           DipoleFilter.parameter for details.
%                           his parameter is set to 0.1 by default.
%
%                           'SpatialDomainTV'
%                           the following field are optional for Options.solvingType='SpatialDomainTV':
%                               tol - tolerance for the stopping criteria (default: 1e-5)
%                               muMin     - Minimum value of mu (deviation from perfect
%                                            l1 norm) at the last continuation step.
%                                           A smaller mu leads to higher accuracy. (default: 0.1)
%                               maxitMu   - number of continuation steps (default: 2)
%                               maxitInner - max number of iterations in an inner loop (default: 10)
%                               lambda - regularization parameter; the higher the value, the smoother the solution.
%                                        (default: 0.5; values between 0.1 and 1 make quite a difference)
%                               ResidualGradientWeighting.parameter - determines relative weight of Gradient Weighting of Residual Vector
%                                                                     compared to Gradient Weighting of Susceptibility (default: 1).
%                                                                     Options.residualWeighting must be true.
%
%
%   The following fields are optional:
%
%                           tolerance
%                               convergence tolerance
%                           maxit
%                               maximum number of iterations
%                           offsetUseBool
%                               Boolean for automatic offset determination
%                               Default: true
%                           residualWeighting
%                               factor for rotation-invariant shift
%                           ConvolutionFilterCompensation
%                               Struct with parameters for compensation of
%                               convolution type filters (see documentation of
%                               GENERATEUNITDIPOLERESPONSE).
%                           DipoleFilter
%                               Filter-struct for dipole regularization.
%                               See GENERATEUNITDIPOLERESPONSE for further
%                               details.
%                           boolPreconditioner
%                               Boolean for preconditioning
%                           preconditionerParameter
%                               parameter for preconditioning
%                           PreconditionerFilter
%                               Filter-struct for regularization of
%                               preconditioning matrix
%                           chemicalShiftFactor
%                               factor for proportionality inversion
%                           multiAngleMethod
%                               method for solution using multiple angle
%                               aquisition data. Available methods are
%                               'difference' (default) and 'simultaneous'.
%
%                           PowerRegularization
%                               standard tikhonov regularization: norm of
%                               susceptibility is minimized. The following
%                               fields are supported:
%                                   'parameter' - specifies the regularization parameter
%                           TikhonovRegularizationSusceptibility
%                               This struct specifies parameters for solving with
%                               Tikhonov regularization of the susceptibility.
%                               The following fields are supported:
%                                   'type'
%                                       Specifies the Tikhonov regularization
%                                       type. Currently, the follwing types are supported:
%                                          'laplacian' - Tikhonov-matrix is a
%                                            3-D Laplacian matrix
%                                          'weightedGradient' - Gradient of
%                                            susceptiblity is weighted with
%                                            the inverse of the gradient of the
%                                            input matrix (see below))
%                                          'weightedGradientDirect' - Gradient of
%                                            susceptiblity is weighted with
%                                            the spatialMatrix (see below) directly
%                                          'partial gradient weighting'
%                                            only voxels where the gradient is
%                                            smaller than Options.GradientMask.gradientThreshMin
%                                            are used to generate a weighting mask)
%                                   'parameter'
%                                       Specifies the Tikhonov-parameter (scalar).
%                                   'spatialMatrix'
%                                       3D-data array that is used for weighting,
%                                       e.g. magnitude or phase data
%                                       (only required for 'weightedGradient',
%                                       'partial gradient weighting', or 'weightedGradientDirect').
%
%                           useConstantConstraints  - Defines whether constrained
%                               susceptibility regions are set to constant values.
%
%
%
%
%       PostProcCone - Struct which defines the post-processing type of
%                      the dipole cone structure. The following fields are
%                      required:
%
%                           type - string containing the type of
%                                  post-processing. Currently only 'tv' is
%                                  supported (Total Variation Minimization).
%
%                      The following fields are optional:
%
%                           threshold - scalar defining the maximum singular value of the dipole
%                                       structure to be replaced = NF threshold (default: 0.14)
%                           applyNCFCorrection - boolean which defines if NCF correction is applied
%                                                (Default: false)
%                           outerConeThreshold - defines the size of the outer Cone (= NCF region),
%                                                where a denoising is applied (Default: 0.3).
%                           NCFFilterOptions - defines options for denoising that is applied in the NCF region
%                                              (see diffusionimagefilter.m)
%                           kSpaceUndersamplingMatrixPhase - This binary matrix
%                                  specifies undersampling of phase k-space
%                                  during data acquisition, e.g. due to
%                                  using partial fourier (PF). Using this
%                                  matrix for sparse recovery reduces the
%                                  number of unknowns and, thus, improved
%                                  quality.
%                           kSpaceUndersamplingMatrixPhaseRelaxed - This binary matrix
%                                  specifies undersampling of k-space
%                                  during data acquisition, e.g. due to
%                                  using partial fourier (PF). Using this
%                                  matrix for sparse recovery reduces the
%                                  number of unknowns and, thus, improved
%                                  quality.
%                           tol - tolerance for the stopping criteria L1-norm (default: 1e-5)
%                           tolEnergy - tolerance for the stopping criteria energy (default: 1e-5)
%                           stopEenergy - specification of the stopping
%                                         energy. If this value is 0,
%                                         energy is determined
%                                         automatically. (default: 0)
%                           muMin     - Minimum value of mu (deviation from perfect
%                                       l1 norm) at the last continuation step.
%                                       A smaller mu leads to higher accuracy. (default: 0.1)
%                           maxitMu   - number of continuation steps (default: 5)
%                           maxMaxitMu - value to which maxitMu may be increased in case stop criterion was not fulfilled
%                           maxitInner - max number of iterations in an inner loop (default: 20)
%                           coneType - defines the shape of the cone. Currently only 'Default' is supported.
%
%
%       useHEIDI - boolean which defines if HEIDI is applied (default: true)
%                  if true, the following Options are chosen (if not defined manually):
%                  PostProcCone.type = 'tv';
%                  PostProcCone.applyNCFCorrection = false;
%                  GradientMask.applyLaplacianCorrection = true;
%                  GradientMask.applyDenoising = true;
%                  DipoleFilter.type = truncSingularValues
%                  DipoleFilter.parameter = DEFAULT
%                  residualWeighting = 0 (only if not set manually)
%
%       useHEDI - boolean which defines if HEDI is applied (default: false)
%                 
%
%
%       useMEDI - boolean which defines if MEDI is applied (default: false)
%                 if true, the following Options are chosen
%                 tolerance = 1e-5;
%                 isFourierDomainFormula = true;
%                 PostProcCone = [];
%                 residualWeighting = [];
%                 DipoleFilter = [];
%                 solvingType = 'SpatialDomainTV';
%                 offsetUseBool = true;
%                 gradientType = 2; % set to two although Tian Liu, 2012, IEEE says cental difference (but this introduces some checkerboard patterns
%                 
%
%
%
%       Rotation     - struct with position information of supplied rdf-data.
%                      This struct may consist of
%                      * 'axis' and 'angle' to define rotation axis and angle in degrees.
%                      * or 'matrix' to define the rotation matrix (3x3xnumberOfRDF)
%
%       voxelAspectRatio - Aspect ratio of voxels (default = [1 1 1]).
%                          Aspect ratio corresponds to voxel size.
%
%       problemMaskClosed - Specification of the region of internal sources.
%                      This mask is required for the HEIDI mode.
%
%       isProblemMaskClosedFlat - If this boolean is true, gradient-based reconstruction
%                      will be improved. The problemMaskClosed is used  for
%                      constraining voxels with unkown field values to be
%                      spatially flat (avoids spikes in the data).
%
%       GradientMask - generates three weighting masks by applying thresholds on the gradient of an input map.
%                      The following fields are supported:
%                           suppliedMask - in case gradient masks were generated externally,
%                                          they can be supplied in this field
%                           spatialMatrix - specifies the input data (default: input RDF)
%                           gradientThreshMin - threshold for gradient (default see source code)
%                           applyLaplacianCorrection - boolean which defines if Laplacian correction is applied
%                                                      (Default: false)
%                           laplacianThreshMin - threshold for laplacian correction (default: 0.009).
%                                                only makes sense if spatialMatrix is phase data.
%                           MagnitudeForCorrection - specifies magnitude data for correction of gradient mask.
%                           magnitudeThresholdMin - threshold for magnitude data correction (default: 3)
%                           applyDenoising - boolean which defines if denoising is applied to spatialMatrices
%                                            before gradient mask creation (Default: true)
%                           applyDenoisingAfter - boolean which defines if denoising is applied to spatialMatrices
%                                            after gradient mask creation (Default: false)
%                           filterOptions  - struct that defines options for denoising (see diffusionimagefilter.m)
%                                            note: if this struct exists, applyDenoising is automatically set to 'true'.
%
%
%       echoTime     - echo time of supplied data, used for normalization
%                      of phase data if GradientMask is used
%
%       magneticFieldStrength - field strength of supplied data, used for normalization
%                      of phase data if GradientMask is used
%
%       GradientMaskResidual - generates three weighting masks for the Residual.
%                       The following fields are supported:
%                           spatialMatrix - specifies the input data (default: Options.GradientMask.spatialMatrix). if this is not supplied,
%                                           Options.GradientMask must be true.
%                           gradientThreshMin - threshold for gradient (default see source code)
%
%
%
%
%   Notes
%
%   Apply zeropadding and makeodd to rdf before calling this function!





%   F Schweser, ferdinand.schweser@med.uni-jena.de, 2009/07/30
%
%   Changelog:]
%  v12.3 - 2018/10/08 - F Schweser - Bugfix for no zeropadding
%  v12.2 - 2018/10/02 - F Schweser - Bugfix scaling of multiecho maps
%  v12.1 - 2018/09/24 - F Schweser - Bugfixes regarding multiecho data and partial Fourier
%  v12  - 2018/02/27	 - F Schweser - Major improvement of default HEIDI parameters
%  v11.5   - 2016/07/27	 - F Schweser - Major bugfix regarding masks in isNCF
%  v11.4   - 2015/08/29	 - F Schweser - Now allows non-zero residual
%                                       weighting in HEIDI mode
%  v11.4   - 2015/08/27	 - F Schweser - Bugfixes regarding residual regularization
%  v11.3   - 2015/07/15	 - F Schweser - Changed MEDI default threshold parameter to value reflecting changes in MEDI function
%  v11.2   - 2015/06/24	 - F Schweser - Bugfixes regarding MEDI. Changed
%                                       default reg parameter to 0.1 and ermoved magnitude weighting matrix
%  v11.1   - 2014/03/31	 - F Schweser - Check rotation matrix
%  v11.0   - 2012/12/12	 - F Schweser - Default parameters changed!!!!!
%			 		maxit from 200 to 400
%				        dipole formula from spatial to FT
%					in HEIDI now no  dipole filter (was thresholded)
%					These changes were made by comparing with old GRAZ results that were better
%  v10.91   - 2012/12/04 - F Schweser - Removed some old stuff
%  v10.9   - 2012/11/15 - F Schweser - Options.problemMaskClosed now closed
%  v10.8   - 2012/11/16 . F Schweser - Major bugfix regarding all types of
%                                      QSM with offset correction. In the
%                                      transpose matrix the
%                                      offsetcalculation was incorrect
%                                      (mask not accounted for). 
%  v10.7   - 2012/11/02 - F Schweser - kSpaceUndersamplingMatrixPhaseRelaxed 
%                                      now uncommented again
%				       + DEFAULT_POSTPROC_THRESHOLD changed from 0.09 to 0.14
%				       + DEFAULT_POSTPROC_OUTERCONETHRESHOLD changed from 0.23 to 0.3
%				       These settings produced substantially better results in the GRAZ in situ data (comparable to the results used in the paper)
%  v10.6   - 2012/10/27 - F Schweser - Major bugfix regarding HEIDI. The
%                                      gradient mask was converted to
%                                      logical, although it must contain
%                                      values of 0.1
%  v10.5   - 2012/09/26 - F Schweser - Memory performance opt
%  v10.41   - 2012/07/19 - F Schweser - Bugfix: nans in rdf
%  v10.4   - 2012/06/29 - F Schweser - New mode 'useMEDI'
%  v10.3   - 2012/04/18 - F Schweser - removed log entried <8.0
%                                    + changed DEFAULT values according to
%                                      HEIDI paper:
%                                       DEFAULT_POSTPROC_THRESHOLD from 0.12 to 0.09
%                                       DEFAULT_POSTPROC_OUTERCONETHRESHOLD from 0.3 to 0.23
%                                       DEFAULT_TV_MU from 0.1 to 0.001 (reduced speckles)
%                                       DEFAULT_TV_MAXITER from 300 to 1000
%                                       DEFAULT_TV_MAXINTITER from 4 to 8
%                                       DEFAULT_CONE_STOPTEST from 3 to 1
%                                       DEFAULT_TV_TOLVAR from 1e-5 to 1e-3
%                                       DEFAULT_GRADIENTMASK_THRESHOLD from 0.00106666 to 0.00105
%                                       DEFAULT_GRADIENTMASK_LAPLACIANTRHESHOLD from 0.002 to 0.0159
%                                       DEFAULT_GRADIENTMASK_MAGNITUDETHRESHOLD from 3 to 2.4
%                                       DEFAULT_ISREPLACEWELLPOSEDSUBDOMAIN from true to false
%                                    + changed HEIDI-specific parameters:
%                                       maxit from 500 to 400
%                                       tolerance from 1e-15 to 1e-5 (see COMPUTESUSCWRAPPER for details)
%                                    + some improvements regarding the NCF
%                                      reconstruction
if nargin < 5 || isempty(spatialWeightingMatrix)
    boolUseSpatialWeightingMatrix = false;
    dispstatus( 'Spatial weighting matrix:             DEFAULT (OFF)')
else
    boolUseSpatialWeightingMatrix = true;
    dispstatus( 'Spatial weighting matrix:             ON')
end


%                                    + some minor code cleanup
%                                    + Bugfix: no undersamplingPhaseRelaxed
%                                    matrix set
%  v10.2   - 2012/04/05 - F Schweser - gradientMaskReturn default value
%  v10.1   - <2012/03/27 - F Schweser - Now returns all gradient masks as cell
%  				      + Denoising of derivative images implemented BEFORE
%					and AFTER the calculation of the derivatives. By default only
%					before.
%				      + Rplacement of zero-values in the gradient masks by fixed value 
%					reduced noise in unconstrained regions (default: 0.1)
%				      + Gradient type now piped (default: 2; no checkerboard effect)
%				      + Bugfix: COSMOS mode required problemMaskClosed
%				      + residualWeighting is not 0 in HEIDI mode by default
%				      + Default values for epsilon
%			              + Some code refactoring to reduce redundancy
%				      + Several minor bugfixes that resulted in improved quality during
%					manuscript drafting
%  v10   - 2012/01/10 - K Sommer   - call of estimatetotalenergy was wrong (PfMatrix)
%                        F Schweser - Bugfix: empty Options.kSpaceUndersamplingMatrixPhaseRelaxed 
%                                     failed
%                                     + PostProcCone.Correction factor
%                                     default value set to 1.
%                                     + now uses only energy inside of cone
%                                     + new option tolEnergy
%                                     + new option stopEnergy
%                                     + isProblemMaskClosedFlat introduced
%                                     and behaviour of problemMaskClosed
%                                     changed
%                                     + now uses dispstatus
%                                     + Bugfix: magnitude gradient was
%                                     denoised instead of the magnitude
%                                     before calculating the gradient
%                                     + DEFAULT_GRADIENTMASK_APPLYDENOISING
%                                     changed to true
%                                     +
%                                     DEFAULT_POSTPROC_NCFFILTEROPTIONS_NOITERATIONS
%                                     changed to 8 (from 5)
%                                     + normalization of phase wrt
%                                     magneticFieldStrength (new options
%                                     field) + default parameter changed
%                                     correspondingly
%                                     + Bugfix: Filteroptions not always
%                                     set by default
%                                     + default heidi value: residualWeighting = 0
%  v9.51   - 2012/01/23 - K Sommer   - default value for Laplacian threshold updated (0.017)
%  v9.5    - 2012/01/20 - F Schweser - Critical bugfix: Laplacian was divided with its maximum (normalized). 
%				       This was inconsistent with paper an produced unforseeable effects.
%				       I have set threshold to 1/eps (large--> no Laplacian reg)
%			               + Laplacian now normalized by echo time.
%				       @KS: Please repeat simulation and change default value.
%  v9.4    - 2012/01/19 - F Schweser - Magnitude gradient threshold
%                                      normalization now with respect to
%                                      average gradient of all three
%                                      directions.
%                                      + code readability improved
%  v9.34    - 2012/01/03 - F Schweser - Memory efficiency improved
%  v9.3     - 2011/12/22 - F Schweser - Now uses the 2-voxel gradient
%                                       instead of the 3-voxel gradient and
%                                       uses the automatic transpose
%                                       gradient of DISCRETEGRADIENT.
%                                       Moiree may now be eliminated.
%  v9.2     - 2011/12/16 - F Schweser - Bugfix: Now HEIDI uses
%                                       threshSingularValues automatically
%   v9.1  2011/12/14 - F Schweser   - Changed transitional-denoising
%                                     parameters to less denoising because
%                                     rio-data lost quality after
%                                     transitional domain replacement
%   v9.0  2011/11/30 - F Schweser   - New mode 'weightedGradientDirect' in
%                                     the hope that this move removes speckles
%                                       + energy correction factor default
%                                       value changed to 1/1.1 because this
%                                       resulted in substantially improved
%                                       solution with fixed post mortem
%                                       brains + uses pfMatrix now (avoids speckles)
%   v8.9  2011/11/21 - F Schweser   - Default NCF and NF parameters changed
%                                     to optimal values in NeuroImage paper
%   v8.8  2011/11/05 - F Schweser   - Performance optimization: Energy estimation improved by outsourcing to new function ESTIMATETOTALIMAGEENERGY
%                                        + several memory optimizations (only startet with this issue... :-/)
%                                   - Bugfix: Voxel aspect ratio now piped to SPARSERECOVERY
%                                   - Bugfix: denoising was only performed
%                                             when NCF was activated
%                                   - Critical Bugfix!! Laplacian correction
%                                     was in fact calculated from the
%                                     Fourier image not the spatial domain
%                                     image!!!! (extremely poor a priori
%                                     information was the result)
%   v8.7  2011/10/27 - F Schweser   - kSpaceUndersamplingMatrix renamed to kSpaceUndersamplingMatrixPhase
%   v8.63 2011/10/25 - F Schweser   - Bugfix: isNCF was not set by default
%   v8.62 2011/10/24 - K Sommer     - minor header update
%   v8.61 2011/10/24 - K Sommer     - applyMagnitudeCorrection field removed (not necessary)
%   v8.6  2011/10/21 - K Sommer     - Options.GradientMask: fields applyLaplacianCorrection, applyMagnitudeCorrection,
%                                                           applyDenoising, filterOptions added
%                                     Options.PostProcCone: fields applyNCFCorrection, outerConeThreshold
%                                                           and NCFFilterOptions added
%                                     header & input options check restructured.
%   v8.53 2011/10/19 - K Sommer     - speed increase: ifftsave(....,'symmetric') -> real(ifftsave(..))
%   v8.52 2011/10/19 - K Sommer     - Default DipoleFilter parameter set to  0.1 again. Query if PostProcCone threshold
%                                     is smaller than this parameter included.
%   v8.51 2011/10/18 - K Sommer     - voxel size piped to diffusionimagefilter
%   v8.5 2011/10/18  - K Sommer     - HEIDI option added: Denoising of input data (phase & magnitude) before gradient mask creation.
%                                     dpMask now named ConeMask. inverseFiltering Option for PostProcCone removed.
%                                     improvedCone Option removed. Default parameter for DipoleFilter set to 0.05.
%   v8.43 2011/10/17 - K Sommer     - bugfix of v8.41: magnitude threshold for gradientmask not normalized
%   v8.42 2011/10/17 - F Schweser   - Bugfix: Now works with MAA mode
%   v8.41 2011/10/17 - K Sommer     - new Options from update to v8.4 removed -> was buggy
%   v8.4 2011/10/12 - K Sommer      - HEIDI algorithm implemented (still called PostProcCone, though).
%                                     New Options-fields: PostProcCone.NFConeThreshold, PostProcCone.NCFConeThreshold, PostProcCone.NCFFilterOptions,
%                                     GradientMask.FilterOptions -> phase and magnitude data is now automatically denoised before gradient mask creation.
%                                     Automatic increase of maxItMu (was defined by maxMaxitMu) removed (too time-consuming).
%                                     Some renaming of variables in PostProcCone-routine.
%   v8.3  2011/09/22 - F Schweser   - feature added: kSpaceUndersamplingMatrix
%   v8.2  2011/09/22 - F Schweser   - Performance improvement: now uses DISCRETEGRADIENT()
%   v8.12 2011/08/02 - K Sommer     - bugfix: operand | instead of || in laplacian correction
%   v8.11 2011/08/02 - K Sommer     - bugfix: now uses suscSpatialWeightingMatrix instead of spatialWeightingMatrix
%                                     (default matrix for spatialWeightingMatrix was not set)
%   v8.1 2011/07/13 - K Sommer      - default value set for Options.PostProcCone.correctionFactor (1)
%   v8.0 2011/07/19 - F Schweser    - Significantly imporved quality of
%                                      cone post-processing with new option: problemMaskClosed. Improves
%                                      gradient-based reconstruction.
%   v8.1 2013/03/04 - A Deistung    - Minor bugfix: gradientMaskReturn is now defined for output                                     
%   v8.2 2013/03/08 - F Schweser    - Minor improvement: removed totalEnergy calculation                             
%   v8.3 2013/03/31 - F Schweser    - DEFAULT change: Now
%                                       applyNCFCorrection is false by
%                                       default. This was inconsistent
%                                       previosuly!
%   v8.4 2015/07/15 - F Schweser    - Changed default parameters for MEDI
%   v8.5 2017/08/22 - F Schweser    - Now returns phase discrepancy

dispstatus(' ')


%% constants

DEFAULT_MAXIT = 100;
DEFAULT_TOLERANCE = 1e-5;
DEFAULT_OFFSETUSEBOOL = true;
DEFAULT_USECONSTANTCONSTRAINTS = true;
DEFAULT_GRADIENTMASK_THRESHOLD = 0.00105; 
DEFAULT_GRADIENTMASK_APPLYLAPLACIAN = true; %false
DEFAULT_GRADIENTMASK_LAPLACIANTRHESHOLD = 0.0159;
DEFAULT_GRADIENTMASK_MAGNITUDETHRESHOLD =  2.4;
DEFAULT_GRADIENTMASK_APPLYDENOISING = true;
DEFAULT_GRADIENTMASK_APPLYDENOISINGAFTER = false;
DEFAULT_GRADIENTMASK_FILTEROPTIONS_TIMESTEP = 0.05;
DEFAULT_GRADIENTMASK_FILTEROPTIONS_CONDUCTANCE = 1;
DEFAULT_GRADIENTMASK_FILTEROPTIONS_NOITERATIONS = 5;
DEFAULT_GRADIENTMASKRESIDUAL_THRESHOLD = 0.0032; % 0.04;
DEFAULT_GRADIENTMASKREPLACEVALUE = 0.1;
DEFAULT_ISREPLACEWELLPOSEDSUBDOMAIN = false;

DEFAULT_USEMEDI = false;
DEFAULT_USEHEDI = false;

DEFAULT_SOLVINGTYPE = 'SpatialDomainL2';
DEFAULT_DIPOLEFILTER_PARAMETER = 0.1;
DEFAULT_DIPOLEFILTER_TYPE = 'truncSingularValues';
DEFAULT_VOXELASPECTRATIO = [1 1 1];

DEFAULT_TV_MAXINTITER = 8;%5;
DEFAULT_TV_MAXITER = 500; %20;
DEFAULT_TV_MU = eps; %substantially reduces speckles compared to 0.1
DEFAULT_TV_TOLVAR = 1e-3;
% it looks like the tolerance mainly influences how well the low frequency
% components match. If chosen 1e-2 the resulting error pattern has
% substantial low frequencay components in it. The pattern looks quite well
% when chosen 1e-3 (with perfect mask). --> set to 1e-3

DEFAULT_TV_TOLENERGY = 1e-3;
DEFAULT_TV_STOPENERGY = 0;

DEFAULT_TV_LAMBDA = 1e-3; % resulted in most appealing images (see test script /synology/projects/methods_development/QSM/reco/MEDI_parameterOptimization/test_various_parameters.m)
DEFAULT_TV_RESIDUALGRADIENTWEIGHTING_PARAMETER = 1;
DEFAULT_TV_STOPTEST = 4;

DEFAULT_CONE_STOPTEST = 1;

DEFAULT_POSTPROC_THRESHOLD = 0.14;0.09;
DEFAULT_POSTPROC_APPLYNCF = false;
DEFAULT_POSTPROC_OUTERCONETHRESHOLD = 0.3;0.23;
%DEFAULT_POSTPROC_NCFFILTEROPTIONS_TIMESTEP = 0.05;
%DEFAULT_POSTPROC_NCFFILTEROPTIONS_CONDUCTANCE = 1.7;
%DEFAULT_POSTPROC_NCFFILTEROPTIONS_NOITERATIONS = 15;
DEFAULT_POSTPROC_NCFFILTEROPTIONS_TIMESTEP = DEFAULT_GRADIENTMASK_FILTEROPTIONS_TIMESTEP;
DEFAULT_POSTPROC_NCFFILTEROPTIONS_CONDUCTANCE = DEFAULT_GRADIENTMASK_FILTEROPTIONS_CONDUCTANCE;
DEFAULT_POSTPROC_NCFFILTEROPTIONS_NOITERATIONS = 8;

DEFAULT_GRADIENTTYPE = 2;

%% Combine RDFs of multiple angle acquisitions
%(Compute difference of several RDFs)



gridDimensionVector = size(dataArray);
gridDimensionVector = gridDimensionVector(1:3);
nRdf = size(dataArray,4);




dispstatus(['Number of RDFs:                       ',num2str(nRdf)]);



    manyFieldsBool = false;
    nRdfUsed = 1;




% generate mask of voxels where at least one field value is available
problemMaskOne = false(gridDimensionVector);
for jRdf = 1:nRdf
    problemMaskOne = problemMaskOne | problemMask(:,:,:,jRdf);
end

    boolWithConstraintMaskSusceptibility = false;
    boolWithConstraintMaskResidual       = false;
    dispstatus( 'Susceptibility constraints:           DEFAULT (OFF)')
    dispstatus( 'Residual constraints:                 DEFAULT (OFF)')



if ~myisfield(Options,'problemMaskClosed') || isempty(Options.problemMaskClosed)
    if nRdf > 1
        Options.problemMaskClosed = problemMaskOne;
    else
        Options.problemMaskClosed = problemMask;
    end
    Options.problemMaskClosed = closemask(Options.problemMaskClosed,3,true); % arbitrary chosen value of 3
    dispstatus( 'Optional closed problem mask supplied:           NO')
elseif islogical(Options.problemMaskClosed)
    dispstatus( 'Optional closed problem mask supplied:           YES')
else
    error('Optional closed problem mask must be logical.')
end

if ~myisfield(Options,'isProblemMaskClosedFlat') || isempty(Options.isProblemMaskClosedFlat)
    Options.isProblemMaskClosedFlat = false;
    dispstatus( 'Force flat susceptibility in unknown-phase regions:           NO')
elseif islogical(Options.isProblemMaskClosedFlat) && Options.isProblemMaskClosedFlat
    dispstatus( 'Force flat susceptibility in unknown-phase regions:           YES')
else
    error('isProblemMaskClosedFlat must be logical.')
end




if nargin < 6 || isempty(spatialWeightingMatrix)
    boolUseSpatialWeightingMatrix = false;
    dispstatus( 'Spatial weighting matrix:             DEFAULT (OFF)')
else
    boolUseSpatialWeightingMatrix = true;
    dispstatus( 'Spatial weighting matrix:             ON')
end




% check dataArray
if ~isreal(dataArray)
    error('Relative difference field must be real-valued.')
end
if numel(dataArray(isnan(dataArray)))
    error('Relative difference field must not contain NAN-values!')
end
if numel(dataArray(isinf(dataArray)))
    error('Relative difference field must not contain INF-values!')
end
if sum(mod(gridDimensionVector,2)) ~= 3
    error('Grid dimension size must not be even.')
end




if ~islogical(problemMask)
    error('Array that defines which voxels of RDF shall be used must be logical-valued.')
end

if ~islogical(problemMask)
    error('Array that defines which voxels of RDF shall be used must be logical-valued.')
end

if boolWithConstraintMaskSusceptibility && ~islogical(ConstraintMask.susceptibility)
    error('Arrays that defines regions of constant susceptibility must be logical-valued.')
end

if boolWithConstraintMaskResidual && ~islogical(ConstraintMask.residual)
    error('Arrays that defines regions of constant residual must be logical-valued.')
end


if boolUseSpatialWeightingMatrix && ~isreal(spatialWeightingMatrix)
    error('Spatial weighting matrix must be real-valued.')
end





% set default values for Options struct
if ~myisfield(Options,'solvingType') || isempty(Options.solvingType)
    Options.solvingType = DEFAULT_SOLVINGTYPE;
end

if ~myisfield(Options,'gradientType') || isempty(Options.gradientType)
    Options.gradientType = DEFAULT_GRADIENTTYPE;
end

isResidualGradientWeighting = false;
switch Options.solvingType
    case 'SpatialDomainL2'
        dispstatus('Solving Type:                         Spatial Domain (L2)')
    case 'InverseFiltering'
        dispstatus('Solving Type:                         Inverse Filtering')
        Options.display = false;
        if ~isfield(Options,'DipoleFilter') || isempty(Options.DipoleFilter) ...
                || ~myisfield(Options.DipoleFilter,'parameter') || isempty(Options.DipoleFilter.parameter)
            Options.DipoleFilter.parameter = DEFAULT_DIPOLEFILTER_PARAMETER;
            Options.DipoleFilter.type = DEFAULT_DIPOLEFILTER_TYPE;
            Options.offsetUseBool = false;
            Options.residualWeighting = false;
        end
    case 'SpatialDomainTV'
        dispstatus('Solving Type:                         Spatial Domain (TV)')
        Options.display = false;
        if ~isfield(Options,'GradientMask')
            dispstatus('Gradient Mask for solving type "SpatialDomainTV" automatically set to: RDF.')
            Options.GradientMask.spatialMatrix = dataArray;
        end
        
        if ~myisfield(Options,'SpatialDomainTV')
            Options.SpatialDomainTV = [];
        end
        
        if ~myisfield(Options.SpatialDomainTV,'tol') || isempty(Options.SpatialDomainTV.tol)
            Options.SpatialDomainTV.tol = DEFAULT_TV_TOLVAR;
            dispstatus(['SpatialDomain (TV) ~ tolerance:       DEFAULT (',num2str(Options.SpatialDomainTV.tol),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ tolerance:       ',num2str(Options.SpatialDomainTV.tol),])
        end
        if ~myisfield(Options.SpatialDomainTV,'muMin') || isempty(Options.SpatialDomainTV.muMin)
            Options.SpatialDomainTV.muMin = DEFAULT_TV_MU;
            dispstatus(['SpatialDomain (TV) ~ minimum mu:      DEFAULT (',num2str(Options.SpatialDomainTV.muMin),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ minimum mu:      ',num2str(Options.SpatialDomainTV.muMin),])
        end
        if ~myisfield(Options.SpatialDomainTV,'maxitMu') || isempty(Options.SpatialDomainTV.maxitMu)
            Options.SpatialDomainTV.maxitMu = DEFAULT_TV_MAXINTITER;
            dispstatus(['SpatialDomain (TV) ~ max. iterations mu:          DEFAULT (',num2str(Options.SpatialDomainTV.maxitMu),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ max. iterations mu:          ',num2str(Options.SpatialDomainTV.maxitMu),])
        end
        if ~myisfield(Options.SpatialDomainTV,'maxitInner') || isempty(Options.SpatialDomainTV.maxitInner)
            Options.SpatialDomainTV.maxitInner = DEFAULT_TV_MAXITER;
            dispstatus(['SpatialDomain (TV) ~ max. iterations inner loop:  DEFAULT (',num2str(Options.SpatialDomainTV.maxitInner),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ max. iterations inner loop:  ',num2str(Options.SpatialDomainTV.maxitInner),])
        end
        
            
            
        if ~myisfield(Options.SpatialDomainTV,'lambda') || isempty(Options.SpatialDomainTV.lambda)
            Options.SpatialDomainTV.lambda = DEFAULT_TV_LAMBDA;
            dispstatus(['SpatialDomain (TV) ~ lambda:          DEFAULT (',num2str(Options.SpatialDomainTV.lambda),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ lambda:          ',num2str(Options.SpatialDomainTV.lambda),])
        end
        if ~myisfield(Options.SpatialDomainTV,'stopTest') || isempty(Options.SpatialDomainTV.stopTest)
            Options.SpatialDomainTV.stopTest = DEFAULT_TV_STOPTEST;
            dispstatus(['SpatialDomain (TV) ~ stopTest:        DEFAULT (',num2str(Options.SpatialDomainTV.stopTest),')'])
        else
            dispstatus(['SpatialDomain (TV) ~ stopTest:        ',num2str(Options.SpatialDomainTV.stopTest),])
        end
        if ~myisfield(Options.SpatialDomainTV,'ResidualGradientWeighting') || isempty(Options.SpatialDomainTV.ResidualGradientWeighting)
            dispstatus('SpatialDomain (TV) ~ Residual Gradient Weighting: OFF')
        else
            isResidualGradientWeighting = true;
            if ~myisfield(Options,'residualWeighting') || isempty(Options.residualWeighting)
                error('Residual Gradient Weighting without Residual Weighting not supported.')
            end
            dispstatus('SpatialDomain (TV) ~ Residual Gradient Weighting: ON')
            if ~myisfield(Options.SpatialDomainTV.ResidualGradientWeighting,'parameter') || isempty(Options.SpatialDomainTV.ResidualGradientWeighting.parameter)
                Options.SpatialDomainTV.ResidualGradientWeighting.parameter = DEFAULT_TV_RESIDUALGRADIENTWEIGHTING_PARAMETER;
                dispstatus(['SpatialDomain (TV) ~ Residual Gradient Weighting parameter:  DEFAULT (',num2str(Options.SpatialDomainTV.ResidualGradientWeighting.parameter),')'])
            else
                dispstatus(['SpatialDomain (TV) ~ Residual Gradient Weighting parameter:  ',num2str(Options.SpatialDomainTV.ResidualGradientWeighting.parameter),])
            end
            
            if ~myisfield(Options,'GradientMaskResidual') || isempty(Options.GradientMaskResidual) || ~myisfield(Options.GradientMaskResidual,'spatialMatrix') || isempty(Options.GradientMaskResidual.spatialMatrix)
                isGradientMaskResidualSpatialMatrix = false;
                if ~myisfield(Options,'GradientMask') || isempty(Options.GradientMask)
                    error('If spatialMatrix for GradientMaskResidual is not supplied, GradientMask is needed.')
                end
                dispstatus('Gradient Mask for Residual:           Gradient Mask for Susceptibility (Default)')
            else
                isGradientMaskResidualSpatialMatrix = true;
                dispstatus('Gradient Mask for Residual:           Spatial Matrix supplied')
            end
            if ~myisfield(Options,'GradientMaskResidual') || isempty(Options.GradientMaskResidual) ||~myisfield(Options.GradientMaskResidual,'gradientThreshMin') || isempty(Options.GradientMaskResidual.gradientThreshMin)
                Options.GradientMaskResidual.gradientThreshMin = DEFAULT_GRADIENTMASKRESIDUAL_THRESHOLD;
                dispstatus(['Gradient Mask for Residual Threshold: DEFAULT (',num2str(Options.GradientMaskResidual.gradientThreshMin),')'])
            else
                dispstatus(['Gradient Mask for Residual Threshold: ',num2str(Options.GradientMaskResidual.gradientThreshMin),])
            end
            
        end
        if myisfield(Options.SpatialDomainTV,'Lmu1') && ~isempty(Options.SpatialDomainTV.Lmu1)          % only temporary, will be deleted soon...
            dispstatus(['Lmu1:                                 ',num2str(Options.SpatialDomainTV.Lmu1)])
        end
        
    otherwise
        error('Specified solving type currently not supported.')
end
if ~myisfield(Options,'useHEDI') || isempty(Options.useHEDI) || Options.useHEDI == false;
        dispstatus('HEDI mode:                           DEFAULT (OFF)')
        Options.useHEDI = DEFAULT_USEHEDI;
elseif ~islogical(Options.useHEDI)
        error('Field Options.useHEDI must be boolean!')
end


if ~myisfield(Options,'useHEIDI') || isempty(Options.useHEIDI) || (Options.useHEIDI == false && ~Options.useHEDI);
    Options.useHEIDI = false;
    dispstatus('HEIDI mode:                           DEFAULT (OFF)')
elseif Options.useHEIDI && nRdf > 1
    error('You cannot use HEIDI with COSMOS.')
elseif ~Options.useHEDI && ~islogical(Options.useHEIDI)
    error('Field Options.useHEIDI must be boolean!')
else
    dispstatus('HEIDI mode:                           ON! Go Heidi!')
    Options.PostProcCone.type = 'tv';
    if ~myisfield(Options.PostProcCone,'applyNCFCorrection') || isempty(Options.PostProcCone.applyNCFCorrection)
        Options.PostProcCone.applyNCFCorrection = DEFAULT_POSTPROC_APPLYNCF;
    end
    if ~myisfield(Options,'GradientMask') || isempty(Options.GradientMask) || ~myisfield(Options.GradientMask,'applyLaplacianCorrection') || isempty(Options.GradientMask.applyLaplacianCorrection)
        Options.GradientMask.applyLaplacianCorrection = true;
    end
    if ~myisfield(Options.GradientMask,'MagnitudeForCorrection') || isempty(Options.GradientMask.MagnitudeForCorrection)
        dispstatus('WARNING: Magnitude Data should be supplied for HEIDI mode!!')
    end
    if ~myisfield(Options,'GradientMask') || isempty(Options.GradientMask) || ~myisfield(Options.GradientMask,'applyDenoising') || isempty(Options.GradientMask.applyDenoising)
        Options.GradientMask.applyDenoising = true;
    end
    
    if ~myisfield(Options,'problemMaskClosed') || isempty(Options.problemMaskClosed)
        error('HEIDI requires problemMaskClosed.')
    end
    
    if Options.useHEIDI
        dispstatus('Setting HEIDI default parameters...')
        Options.maxit = 400;
        %Options.maxit = 200;
        Options.tolerance = 1e-5;%1e-5;
        Options.isFourierDomainFormula = false;
        %Options.isFourierDomainFormula = false;
        Options.offsetUseBool = true; 
        %Options.DipoleFilter.type =  'threshSingularValues';
        %Options.DipoleFilter.parameter =  DEFAULT_POSTPROC_THRESHOLD-0.05; % 0.05 difference to avoid edge issues in k-space between regions
        % all the parameters commented above were changed to fit old graz
        % result
        Options.DipoleFilter = [];
        if ~isfield(Options,'residualWeighting') || isempty(Options.residualWeighting)
            Options.residualWeighting = 0;
        end
    end
end

if ~myisfield(Options,'useMEDI') || isempty(Options.useMEDI) || Options.useMEDI == false;
        dispstatus('MEDI mode:                           DEFAULT (OFF)')
        Options.useMEDI = DEFAULT_USEMEDI;
elseif ~islogical(Options.useMEDI)
        error('Field Options.useMEDI must be boolean!')
end

% if ~Options.useHEIDI
%     if Options.useMEDI || Options.useHEDI
%         dispstatus('MEDI/HEDI mode:                           ON!')
%         Options.PostProcCone = [];
%         if Options.useMEDI
%             if ~myisfield(Options,'GradientMask') || isempty(Options.GradientMask)
%                 error('MEDI needs GradientMask-field to be magnitude image.')
% %             elseif ~myisfield(Options.GradientMask,'gradientThreshMin') || isempty(Options.GradientMask.gradientThreshMin)
% %                 error('MEDI needs GradientMask.gradientThreshMin field (e.g. 1.2; ideally 70% edges).')
%             end
%         end
% 
%         dispstatus('Setting MEDI/HEDI default parameters...')
%         Options.tolerance = 1e-5
%         Options.isFourierDomainFormula = true;
%         Options.PostProcCone = [];
%         Options.residualWeighting = [];
%         Options.DipoleFilter = [];
%         Options.solvingType = 'SpatialDomainTV';
%         %Options.SpatialDomainTV.maxitMu = 4;
%         %Options.SpatialDomainTV.maxitInner = 2000;
%         %Options.SpatialDomainTV.muMin = 0.1;
%         Options.offsetUseBool = false;
%         Options.gradientType = 2; % set to two although Tian Liu, 2012, IEEE says cental difference (but this introduces some checkerboard patterns
%         Options.SpatialDomainTV.stopTest = 1;
%         Options.SpatialDomainTV.maxitMu = DEFAULT_TV_MAXINTITER;
%         Options.SpatialDomainTV.maxitInner = DEFAULT_TV_MAXITER;
%         if ~myisfield(Options.SpatialDomainTV,'lambda') || isempty(Options.SpatialDomainTV.lambda)
%             Options.SpatialDomainTV.lambda = DEFAULT_TV_LAMBDA;
%             disp('DEFAULT MEDI/HEDI regularization parameter chosen! This may result in oversmoothing...')
%         end
%         Options.SpatialDomainTV.muMin = DEFAULT_TV_MU;
%         Options.SpatialDomainTV.tol = DEFAULT_TV_TOLVAR;
%         Options.SpatialDomainTV.tolerance = DEFAULT_TV_TOLVAR;
% 
%     end
% end








isNCF = false;
isPreConeSuscSupplied = false;
if ~isfield(Options,'PostProcCone') || ~myisfield(Options.PostProcCone,'type')
    isPostProcCone = false;
    dispstatus('Post-processing of cone:                    DEFAULT (OFF)')
elseif isfield(Options,'PostProcCone') && strcmp(Options.solvingType,'SpatialDomainTV')
    error('Post-processing of cone not posssible for this solving type.')
elseif isfield(Options,'PostProcCone') && strcmp(Options.solvingType,'inverseFiltering')
    error('Post-processing of cone not posssible for this solving type.')
else
    dispstatus('Post-processing of cone:                                      ON')
    isPostProcCone = true;
    if ~myisfield(Options.PostProcCone,'threshold') || isempty(Options.PostProcCone.threshold)
        Options.PostProcCone.threshold = DEFAULT_POSTPROC_THRESHOLD;
        dispstatus(['Post-processing of cone ~ threshold:                          DEFAULT (',num2str(Options.PostProcCone.threshold),')'])
    else
        dispstatus(['Post-processing of cone ~ threshold:                          ',num2str(Options.PostProcCone.threshold),])
    end
    if myisfield(Options,'DipoleFilter') && ~isempty(Options.DipoleFilter) && myisfield(Options.DipoleFilter,'parameter') && ~isempty(Options.DipoleFilter.parameter)
        if Options.DipoleFilter.parameter > Options.PostProcCone.threshold
            error('DipoleFilter parameter must not be larger that Cone threshold!!!!')
        end
    end
    
    
    switch Options.PostProcCone.type
        case 'tv'
            
            if myisfield(Options.PostProcCone,'PreConeSuscSupplied') && ~isempty(Options.PostProcCone.PreConeSuscSupplied)
                isPreConeSuscSupplied = true;
                dispstatus('Post-processing of cone ~ PreCone Susceptibility:             Supplied')
                Options.offsetUseBool = false;
                Options.display = false;
            end
            
            if ~isfield(Options,'GradientMask')
                dispstatus('Gradient Mask for post-processing of cone automatically set to: RDF.')
                Options.GradientMask.spatialMatrix = dataArray;
            end
            
            if ~myisfield(Options.PostProcCone,'applyNCFCorrection') || isempty(Options.PostProcCone.applyNCFCorrection)
                isNCF = DEFAULT_POSTPROC_APPLYNCF;
                dispstatus('Post-processing of cone ~ NCF Correction:                     DEFAULT (OFF)')
            elseif Options.PostProcCone.applyNCFCorrection
                isNCF = Options.PostProcCone.applyNCFCorrection;
                dispstatus('Post-processing of cone ~ NCF Correction:                     ON')
                if ~myisfield(Options.PostProcCone,'outerConeThreshold') || isempty(Options.PostProcCone.outerConeThreshold)
                    Options.PostProcCone.outerConeThreshold = DEFAULT_POSTPROC_OUTERCONETHRESHOLD;
                    dispstatus(['Post-processing of cone ~ NCF Cone threshold:                 DEFAULT (',num2str(Options.PostProcCone.outerConeThreshold),')'])
                else
                    dispstatus(['Post-processing of cone ~ NCF Cone threshold:                 ',num2str(Options.PostProcCone.outerConeThreshold),])
                end
                if ~myisfield(Options.PostProcCone,'NCFFilterOptions') || isempty(Options.PostProcCone.NCFFilterOptions)
                    Options.PostProcCone.NCFFilterOptions.timeStep = DEFAULT_POSTPROC_NCFFILTEROPTIONS_TIMESTEP;
                    Options.PostProcCone.NCFFilterOptions.conductance = DEFAULT_POSTPROC_NCFFILTEROPTIONS_CONDUCTANCE;
                    Options.PostProcCone.NCFFilterOptions.numberOfIterations = DEFAULT_POSTPROC_NCFFILTEROPTIONS_NOITERATIONS;
                    dispstatus(['Post-processing of cone ~ NCF FilterOptions ~ timeStep:       DEFAULT (',num2str(Options.PostProcCone.NCFFilterOptions.timeStep),')'])
                    dispstatus(['Post-processing of cone ~ NCF FilterOptions ~ conductance:    DEFAULT (',num2str(Options.PostProcCone.NCFFilterOptions.conductance),')'])
                    dispstatus(['Post-processing of cone ~ NCF FilterOptions ~ no. iterations: DEFAULT (',num2str(Options.PostProcCone.NCFFilterOptions.numberOfIterations),')'])
                else
                    dispstatus(['Post-processing of cone ~ NCF FilterOptions ~ timeStep:       ',num2str(Options.PostProcCone.NCFFilterOptions.timeStep),])
                    dispstatus(['Post-processing of cone ~ NCF FilterOptions ~ conductance:    ',num2str(Options.PostProcCone.NCFFilterOptions.conductance),])
                    dispstatus(['Post-processing of cone ~ NCF FilterOptions ~ no. iterations: ',num2str(Options.PostProcCone.NCFFilterOptions.numberOfIterations),])
                end
            else
                dispstatus('Post-processing of cone ~ NCF Correction:                          OFF')
            end
            
            if ~myisfield(Options.PostProcCone,'tol') || isempty(Options.PostProcCone.tol)
                Options.PostProcCone.tol = DEFAULT_TV_TOLVAR;
                dispstatus(['Post-processing of cone ~ tol:                                DEFAULT (',num2str(Options.PostProcCone.tol),')'])
            else
                dispstatus(['Post-processing of cone ~ tol:                                ',num2str(Options.PostProcCone.tol),])
            end
            if ~myisfield(Options.PostProcCone,'tolEnergy') || isempty(Options.PostProcCone.tolEnergy)
                Options.PostProcCone.tolEnergy = DEFAULT_TV_TOLENERGY;
                dispstatus(['Post-processing of cone ~ tolEnergy:                                DEFAULT (',num2str(Options.PostProcCone.tolEnergy),')'])
            else
                dispstatus(['Post-processing of cone ~ tolEnergy:                                ',num2str(Options.PostProcCone.tolEnergy),])
            end
            if ~myisfield(Options.PostProcCone,'stopEnergy') || isempty(Options.PostProcCone.stopEnergy)
                Options.PostProcCone.stopEnergy = DEFAULT_TV_STOPENERGY;
                dispstatus(['Post-processing of cone ~ stopEnergy:                                DEFAULT (',num2str(Options.PostProcCone.stopEnergy),')'])
            else
                dispstatus(['Post-processing of cone ~ stopEnergy:                                ',num2str(Options.PostProcCone.stopEnergy),])
            end
            if ~myisfield(Options.PostProcCone,'muMin') || isempty(Options.PostProcCone.muMin)
                Options.PostProcCone.muMin = DEFAULT_TV_MU;
                dispstatus(['Post-processing of cone ~ minimum mu:                         DEFAULT (',num2str(Options.PostProcCone.muMin),')'])
            else
                dispstatus(['Post-processing of cone ~ minimum mu:                         ',num2str(Options.PostProcCone.muMin),])
            end
            if ~myisfield(Options.PostProcCone,'maxitMu') || isempty(Options.PostProcCone.maxitMu)
                Options.PostProcCone.maxitMu = DEFAULT_TV_MAXINTITER;
                dispstatus(['Post-processing of cone ~ iterations outer loop:              DEFAULT (',num2str(Options.PostProcCone.maxitMu),')'])
            else
                dispstatus(['Post-processing of cone ~ iterations outer loop:              ',num2str(Options.PostProcCone.maxitMu),])
            end
            if ~myisfield(Options.PostProcCone,'maxMaxitMu') || isempty(Options.PostProcCone.maxMaxitMu)
                Options.PostProcCone.maxMaxitMu = Options.PostProcCone.maxitMu;
                dispstatus(['Post-processing of cone ~ max. iterations outer loop:         DEFAULT (',num2str(Options.PostProcCone.maxMaxitMu),')'])
            else
                if Options.PostProcCone.maxMaxitMu < Options.PostProcCone.maxitMu
                    error('Maximum number of outer loop iterations cannot be smaller that initial number of outer loop iterations.')
                end
                dispstatus(['Post-processing of cone ~ max. iterations outer loop:         ',num2str(Options.PostProcCone.maxMaxitMu),])
            end
            if ~myisfield(Options.PostProcCone,'maxitInner') || isempty(Options.PostProcCone.maxitInner)
                Options.PostProcCone.maxitInner = DEFAULT_TV_MAXITER;
                dispstatus(['Post-processing of cone ~ iterations inner loop:              DEFAULT (',num2str(Options.PostProcCone.maxitInner),')'])
            else
                dispstatus(['Post-processing of cone ~ iterations inner loop:              ',num2str(Options.PostProcCone.maxitInner),])
            end
            if ~myisfield(Options.PostProcCone,'stopTest') || isempty(Options.PostProcCone.stopTest)
                Options.PostProcCone.stopTest = DEFAULT_CONE_STOPTEST;
                dispstatus(['Post-processing of cone ~ Stopping criterion:                 DEFAULT (',num2str(Options.PostProcCone.stopTest),')'])
            else
                dispstatus(['Post-processing of cone ~ Stopping criterion:                 ',num2str(Options.PostProcCone.stopTest),])
            end
            if ~myisfield(Options.PostProcCone,'correctionFactor') || isempty(Options.PostProcCone.correctionFactor)
                Options.PostProcCone.correctionFactor = 1;
            end
            
        otherwise
            error('Specified post-processing type for the cone is not supported.')
    end
end

%
% if ~myisfield(Options.TikhonovRegularizationSusceptibility,'LaplacianThreshMax') || isempty(Options.TikhonovRegularizationSusceptibility.LaplacianThreshMax)
%     Options.TikhonovRegularizationSusceptibility.LaplacianThreshMax = DEFAULT_LAPLACIANTHRESHOLD;
% end



if ~myisfield(Options,'voxelAspectRatio') || isempty(Options.voxelAspectRatio)
    Options.voxelAspectRatio = DEFAULT_VOXELASPECTRATIO;
end
if ~myisfield(Options,'epsilon') || isempty(Options.epsilon)
    Options.epsilon = 0;
end

if ~myisfield(Options,'gradientMaskReplaceValue') || isempty(Options.gradientMaskReplaceValue)
    Options.gradientMaskReplaceValue = DEFAULT_GRADIENTMASKREPLACEVALUE;
end


if ~isfield(Options,'tolerance') || isempty(Options.tolerance)
    Options.tolerance = DEFAULT_TOLERANCE;
    dispstatus(['Tolerance:                            DEFAULT (',num2str(Options.tolerance),')'])
else
    dispstatus(['Tolerance:                            ',num2str(Options.tolerance),''])
end

if ~isfield(Options,'isReplaceWellPosedDomain') || isempty(Options.isReplaceWellPosedDomain)
    Options.isReplaceWellPosedDomain = DEFAULT_ISREPLACEWELLPOSEDSUBDOMAIN; 
    dispstatus(['Replace well-posed domain:                 DEFAULT (',num2str(Options.isReplaceWellPosedDomain),')'])
end


if ~myisfield(Options,'useConstantConstraints')
    Options.useConstantConstraints = DEFAULT_USECONSTANTCONSTRAINTS;
    dispstatus(['Constant constraints:                 DEFAULT (',num2str(Options.useConstantConstraints),')'])
else
    dispstatus(['Constant constraints:                 ',num2str(Options.useConstantConstraints),''])
end

if ~myisfield(Options,'maxit')
    Options.maxit = DEFAULT_MAXIT;
    dispstatus(['Maximum no of cycles:                 DEFAULT (',num2str(Options.maxit),')'])
else
    dispstatus(['Maximum no of cycles:                 ',num2str(Options.maxit)])
end

if ~myisfield(Options,'offsetUseBool')
    Options.offsetUseBool = DEFAULT_OFFSETUSEBOOL;
    dispstatus( 'Automatic offset determination: DEFAULT (YES)')
else
    if Options.offsetUseBool
        dispstatus('Automatic offset determination:       YES')
    else
        dispstatus('Automatic offset determination:       OFF')
    end
end

if ~myisfield(Options,'display')
    Options.display = true;
end

if ~myisfield(Options,'residualWeighting') || isempty(Options.residualWeighting) || Options.residualWeighting == 0
    Options.residualWeighting = 0;
    dispstatus( 'Residual regularization:              DEFAULT (OFF)');
    residualArray = zeros(gridDimensionVector);
else
    dispstatus(['Residual regularization:              ON (',num2str(Options.residualWeighting),')'])
end



if Options.residualWeighting && nargout < 3
    error('Residual-Weighting requires three output variables.')
end


if ~myisfield(Options,'TikhonovRegularizationSusceptibility') || isempty(Options.TikhonovRegularizationSusceptibility)
    isTikhonovRegularizationSusceptibility = false;
    dispstatus( 'Tikhonov Regularization Susc.:        DEFAULT (OFF)')
else
    isTikhonovRegularizationSusceptibility = true;
    if strcmp(Options.TikhonovRegularizationSusceptibility.type,'partial gradient weighting')   % GradientMask is used for Option 'partial gradient weighting'. For Option 'weightedGradient',
        if ~myisfield(Options,'GradientMask')                                                    %   GradientMask is not activated to ensure compatibility with older versions
            Options.GradientMask = [];    % GradientMask activated, default values are set below
        end
    end
end

if ~isPostProcCone && (~myisfield(Options,'GradientMask') || isempty(Options.GradientMask)) || nRdf > 1
    isGradientMask = false;
    dispstatus('Gradient Mask:                                  DEFAULT (OFF)')
else
    dispstatus('Gradient Mask:                                  ON')
    isGradientMask = true;
    if myisfield(Options.GradientMask,'suppliedMask') && ~isempty(Options.GradientMask.suppliedMask)
        isGradientMaskSupplied = true;
    else
        isGradientMaskSupplied = false;
    end
    if ~myisfield(Options.GradientMask,'gradientThreshMin') || isempty(Options.GradientMask.gradientThreshMin)
        Options.GradientMask.gradientThreshMin = DEFAULT_GRADIENTMASK_THRESHOLD;
        dispstatus(['Gradient Mask Threshold:                        DEFAULT (',num2str(Options.GradientMask.gradientThreshMin),')'])
    else
        dispstatus(['Gradient Mask Threshold:                        ',num2str(Options.GradientMask.gradientThreshMin),])
    end
    if ~myisfield(Options.GradientMask,'spatialMatrix') || isempty(Options.GradientMask.spatialMatrix)
        Options.GradientMask.spatialMatrix = dataArray;
    end
    if ~myisfield(Options,'echoTime') || isempty(Options.echoTime)
        error('Echo time of input data must be supplied for Gradient Mask feature.')
    end
    if ~myisfield(Options,'magneticFieldStrength') || isempty(Options.magneticFieldStrength)
        error('Magnetic field strength of input data must be supplied for Gradient Mask feature.')
    end
    
    if ~myisfield(Options.GradientMask,'applyLaplacianCorrection') || isempty(Options.GradientMask.applyLaplacianCorrection) || Options.GradientMask.applyLaplacianCorrection == false
        Options.GradientMask.applyLaplacianCorrection = DEFAULT_GRADIENTMASK_APPLYLAPLACIAN;
        dispstatus('Laplacian correction of Gradient Mask:          DEFAULT (OFF)')
        isLaplacianCorrection = false;
    elseif ~islogical(Options.GradientMask.applyLaplacianCorrection)
        error('Field Options.GradientMask.applyLaplacianCorrection must be boolean!')
    else
        dispstatus('Laplacian correction of Gradient Mask:          ON')
        isLaplacianCorrection = true;
        if ~myisfield(Options.GradientMask,'laplacianThreshMin') || isempty(Options.GradientMask.laplacianThreshMin)
            Options.GradientMask.laplacianThreshMin = DEFAULT_GRADIENTMASK_LAPLACIANTRHESHOLD;
            dispstatus(['Laplacian correction of Gradient Mask ~ value:  DEFAULT (',num2str(Options.GradientMask.laplacianThreshMin),')'])
        else
            dispstatus(['Laplacian correction of Gradient Mask ~ value:  ',num2str(Options.GradientMask.laplacianThreshMin),])
        end
    end
    
    if ~myisfield(Options.GradientMask,'MagnitudeForCorrection') || isempty(Options.GradientMask.MagnitudeForCorrection)
        dispstatus('Magnitude Correction of Gradient Mask:          DEFAULT (OFF)')
        isMagnitudeCorrection = false;
    else
        dispstatus('Magnitude correction of Gradient Mask:          ON')
        isMagnitudeCorrection = true;
        if ~myisfield(Options.GradientMask,'magnitudeThresholdMin') || isempty(Options.GradientMask.magnitudeThresholdMin)
            Options.GradientMask.magnitudeThresholdMin = DEFAULT_GRADIENTMASK_MAGNITUDETHRESHOLD;
            dispstatus(['Magnitude correction of Gradient Mask ~ value:  DEFAULT (',num2str(Options.GradientMask.magnitudeThresholdMin),')'])
        else
            dispstatus(['Magnitude correction of Gradient Mask ~ value:  ',num2str(Options.GradientMask.magnitudeThresholdMin),])
        end
    end
    
    if ~myisfield(Options.GradientMask,'applyDenoising') || isempty(Options.GradientMask.applyDenoising)
        Options.GradientMask.applyDenoising = DEFAULT_GRADIENTMASK_APPLYDENOISING;
        dispstatus('Denoising before Gradient Mask creation:        DEFAULT (OFF)')
        isDenoisingBeforeGradientCalc = DEFAULT_GRADIENTMASK_APPLYDENOISING;
    elseif Options.GradientMask.applyDenoising == false
        dispstatus('Denoising before Gradient Mask creation:        DEFAULT (OFF)')
        isDenoisingBeforeGradientCalc = false;
    elseif ~islogical(Options.GradientMask.applyDenoising)
        error('Field Options.GradientMask.applyDenoising must be boolean!')
    else
        dispstatus('Denoising before Gradient Mask creation:        ON')
        isDenoisingBeforeGradientCalc = true;
    end
    
    if ~myisfield(Options.GradientMask,'applyDenoisingAfter') || isempty(Options.GradientMask.applyDenoisingAfter)
        Options.GradientMask.applyDenoisingAfter = DEFAULT_GRADIENTMASK_APPLYDENOISINGAFTER;
        dispstatus('Denoising after Gradient Mask creation:        DEFAULT (OFF)')
        isDenoisingAfterGradientCalc = DEFAULT_GRADIENTMASK_APPLYDENOISINGAFTER;
    elseif Options.GradientMask.applyDenoisingAfter == false
        dispstatus('Denoising after Gradient Mask creation:        DEFAULT (OFF)')
        isDenoisingAfterGradientCalc = false;
    elseif ~islogical(Options.GradientMask.applyDenoisingAfter)
        error('Field Options.GradientMask.applyDenoisingAfter must be boolean!')
    else
        dispstatus('Denoising after Gradient Mask creation:        ON')
        isDenoisingAfterGradientCalc = true;
    end
    
    if ~myisfield(Options.GradientMask,'filterOptions') || isempty(Options.GradientMask.filterOptions)
        Options.GradientMask.filterOptions.timeStep = DEFAULT_GRADIENTMASK_FILTEROPTIONS_TIMESTEP;
        Options.GradientMask.filterOptions.conductance = DEFAULT_GRADIENTMASK_FILTEROPTIONS_CONDUCTANCE;
        Options.GradientMask.filterOptions.numberOfIterations = DEFAULT_GRADIENTMASK_FILTEROPTIONS_NOITERATIONS;
        dispstatus(['GradientMask ~ FilterOptions ~ timeStep:        DEFAULT (',num2str(Options.GradientMask.filterOptions.timeStep),')'])
        dispstatus(['GradientMask ~ FilterOptions ~ conductance:     DEFAULT (',num2str(Options.GradientMask.filterOptions.conductance),')'])
        dispstatus(['GradientMask ~ FilterOptions ~ no. iterations:  DEFAULT (',num2str(Options.GradientMask.filterOptions.numberOfIterations),')'])
    else
        dispstatus(['GradientMask ~ FilterOptions ~ timeStep:        ',num2str(Options.GradientMask.filterOptions.timeStep),])
        dispstatus(['GradientMask ~ FilterOptions ~ conductance:     ',num2str(Options.GradientMask.filterOptions.conductance),])
        dispstatus(['GradientMask ~ FilterOptions ~ no. iterations:  ',num2str(Options.GradientMask.filterOptions.numberOfIterations),])
    end
    
end

%if  Options.residualWeighting == 0 && nargout >= 3
%    error('Only two output variables required if no residual-weighting is activated.')
%end




if myisfield(Options,'ConvolutionFilterCompensation')  && ~isempty(Options.ConvolutionFilterCompensation)
    % if ~myisfield(Options,'dipoleCroppingRadius')
    %     Options.dipoleCroppingRadius = [];
    %     dispstatus( 'Cropped dipole response:        DEFAULT (OFF)')
    % elseif ~isempty(Options.dipoleCroppingRadius)
    %     dispstatus(['Cropped dipole response:        ',num2str(Options.dipoleCroppingRadius),'px'])
    % end
    dispstatus(['Conv. filter compensation:            ',Options.ConvolutionFilterCompensation.type]);
else
    dispstatus( 'Conv. filter compensation:            DEFAULT (OFF)')
    Options.ConvolutionFilterCompensation = [];
end

if ~myisfield(Options,'DipoleFilter') || isempty(Options.DipoleFilter)
    Options.DipoleFilter = [];
    dispstatus( 'Dipole response filter:               DEFAULT (OFF)')
else
    if ~isstruct(Options.DipoleFilter) || ~myisfield(Options.DipoleFilter,'type') || ~myisfield(Options.DipoleFilter,'parameter')
        error('Specification of parameter and type field of dipole-filter struct required!')
    end
    dispstatus(['Dipole response filter:               ',Options.DipoleFilter.type,' (',num2str(Options.DipoleFilter.parameter),')']);
end

if myisfield(Options,'smvErrorTermThickness')
    error('smvErrorTermThickness field is deprecated!');
    %     Options.smvErrorTermThickness = false;
    %     dispstatus( 'SMV-Surfaceterm:                DEFAULT (OFF)')
    % elseif Options.smvErrorTermThickness
    %     dispstatus( 'SMV-Surfaceterm:                ON')
end

if ~myisfield(Options,'boolPreconditioner')
    Options.boolPreconditioner = false;
    dispstatus( 'Preconditioning:                      DEFAULT (OFF)')
elseif Options.boolPreconditioner
    dispstatus( 'Preconditioning:                      ON')
    
    if  ~myisfield(Options,'preconditionerParameter')
        error('No parameter for preconditioning specified.')
    elseif  ~myisfield(Options,'PreconditionerFilter')
        Options.PreconditionerFilter = [];
        dispstatus(['Preconditioning filter:               ',Options.PreconditionerFilter.type,' (',num2str(Options.PreconditionerFilter.parameter),')']);
        
    end
end

if ~myisfield(Options,'chemicalShiftFactor')
    Options.chemicalShiftFactor = false;
    dispstatus( 'Chemical Shift Factor:                DEFAULT (OFF)')
elseif Options.chemicalShiftFactor
    dispstatus(['Chemical Shift Factor:                ',num2str(Options.chemicalShiftFactor)])
end

nProblemMaskVoxel = numel(problemMask(problemMask == true));
dispstatus(['Available data samples (total): ',num2str(nProblemMaskVoxel)])

nProblemMaskVoxelIndividual = zeros(nRdfUsed,1);
for jRdf = 1:nRdfUsed
    tmp = problemMask(:,:,:,jRdf);
    nProblemMaskVoxelIndividual(jRdf) = numel(tmp(tmp == true));
    
    dispstatus(['Available data samples RDF',num2str(jRdf),': ',num2str(nProblemMaskVoxelIndividual(jRdf))])
    clear tmp
end

nProblemMaskVoxelOne = numel(problemMaskOne(problemMaskOne == true));




% determine number of constrained regions
if boolWithConstraintMaskSusceptibility
    if ndims(ConstraintMask.susceptibility) == 3 && sum(ConstraintMask.susceptibility(:))
        nConstrainedRegionSusceptibility = 1;
    else
        
        % remove all empty regions
        nConstrainedRegionSusceptibility = 0;
        for jDim = 1:size(ConstraintMask.susceptibility,4)
            if sum(sum(sum(ConstraintMask.susceptibility(:,:,:,jDim))))
                nConstrainedRegionSusceptibility = nConstrainedRegionSusceptibility + 1;
                ConstraintMask.susceptibility(:,:,:,nConstrainedRegionSusceptibility) = ConstraintMask.susceptibility(:,:,:,jDim);
            end
        end
        if nConstrainedRegionSusceptibility
            ConstraintMask.susceptibility = ConstraintMask.susceptibility(:,:,:,1:nConstrainedRegionSusceptibility);
        else
            boolWithConstraintMaskSusceptibility = false;
            nConstrainedRegionSusceptibility = 0;
        end
    end
    dispstatus(['Number of constrained regions (S):',num2str(nConstrainedRegionSusceptibility)])
end

if boolWithConstraintMaskResidual
    if ndims(ConstraintMask.residual) == 3 %%&& sum(ConstraintMask.susceptibility(:))
        nConstrainedRegionResidual = 1;
    else
        
        % remove all empty regions
        nConstrainedRegionResidual = 0;
        for jDim = 1:size(ConstraintMask.residual,4)
            if sum(sum(sum(ConstraintMask.residual(:,:,:,jDim))))
                nConstrainedRegionResidual = nConstrainedRegionResidual + 1;
                ConstraintMask.residual(:,:,:,nConstrainedRegionResidual) = ConstraintMask.residual(:,:,:,jDim);
            end
        end
        if nConstrainedRegionResidual
            ConstraintMask.residual = ConstraintMask.residual(:,:,:,1:nConstrainedRegionResidual);
        else
            boolWithConstraintMaskResidual = false;
            nConstrainedRegionResidual = 0;
        end
    end
    dispstatus(['Number of constrained regions (R):',num2str(nConstrainedRegionResidual)])
end




% check that constrained regions are disjoint
if boolWithConstraintMaskSusceptibility
    tmp = false(gridDimensionVector);
    for jConstrainedRegionSusceptibility = 1:nConstrainedRegionSusceptibility
        tmp = tmp & ConstraintMask.susceptibility(:,:,:,jConstrainedRegionSusceptibility);
    end
    if sum(tmp(:))
        error('Regions of constrained susceptibility must be disjoint!')
    end
    clear tmp
end
if boolWithConstraintMaskResidual
    tmp = false(gridDimensionVector);
    for jConstrainedRegionResidual = 1:nConstrainedRegionResidual
        tmp = tmp & ConstraintMask.residual(:,:,:,jConstrainedRegionResidual);
    end
    if sum(tmp(:))
        error('Regions of constrained residual must be disjoint!')
    end
    clear tmp
end





% determine vector and matrix sizes

if boolWithConstraintMaskSusceptibility
    
    freeRegion = zeros(gridDimensionVector);
    if Options.useConstantConstraints
        for jConstrainedRegion = 1:nConstrainedRegionSusceptibility
            freeRegion = freeRegion + ConstraintMask.susceptibility(:,:,:,jConstrainedRegion);
        end
    end
    freeRegion = freeRegion == 0;
    nVoxelFreeRegion = sum(freeRegion(:));
    
    nDegreeFreedomSuscept = nVoxelFreeRegion + nConstrainedRegionSusceptibility;
else
    nDegreeFreedomSuscept = prod(gridDimensionVector);
    freeRegion = true(gridDimensionVector);
end



if boolWithConstraintMaskResidual
    
    freeRegionResidual = zeros([gridDimensionVector nRdfUsed]);
    for jConstrainedRegion = 1:nConstrainedRegionResidual
        freeRegionResidual = freeRegionResidual + ConstraintMask.residual(:,:,:,jConstrainedRegion);
    end
    freeRegionResidual = freeRegionResidual == 0 & problemMaskOne;
    nVoxelFreeRegionResidual = sum(freeRegionResidual(:) & problemMaskOne(:));
    nDegreeFreedomResidual = nVoxelFreeRegionResidual + nConstrainedRegionResidual;
elseif Options.residualWeighting
    nDegreeFreedomResidual = sum(problemMaskOne(:));
    nVoxelFreeRegionResidual = sum(problemMaskOne(:));
    freeRegionResidual = problemMaskOne;
else
    nDegreeFreedomResidual = 0;
end

dispstatus(['Degrees of Freedom Susceptibility: ',num2str(nDegreeFreedomSuscept)])
dispstatus(['Degrees of Freedom Residual: ',num2str(nDegreeFreedomResidual)])

dispstatus(['Percent determined (optimal): ',num2str(floor(nProblemMaskVoxel/(nProblemMaskVoxelOne+nDegreeFreedomResidual)*100)),'%'])
dispstatus(['Percent determined (bad background correction): ',num2str(floor(nProblemMaskVoxel/(nDegreeFreedomSuscept+nDegreeFreedomResidual)*100)),'%'])
dispstatus(['Virtual percent determined (susceptibility only): ',num2str(floor(nProblemMaskVoxel/(nProblemMaskVoxelOne)*100)),'%'])

if Options.isProblemMaskClosedFlat
    forceSmoothSusceptibilityMask = Options.problemMaskClosed & ~problemMask;
else
    forceSmoothSusceptibilityMask = false(size(problemMask));
end

if isGradientMask
    
    if isGradientMaskSupplied
        GradientMask_x = Options.GradientMask.suppliedMask(:,:,:,1);
        GradientMask_y = Options.GradientMask.suppliedMask(:,:,:,2);
        GradientMask_z = Options.GradientMask.suppliedMask(:,:,:,3);
    else
        if ~Options.useMEDI
            normalizationFactor = 1/Options.echoTime ./ Options.magneticFieldStrength;
        else
            normalizationFactor = 1;
        end
            
        if isDenoisingBeforeGradientCalc
            Options.GradientMask.filterOptions.voxelsize = Options.voxelAspectRatio;
            PhaseDenoised = diffusionimagefilter(Options.GradientMask.spatialMatrix.*problemMask,Options.GradientMask.filterOptions);
            

            Phase_Gradient_x = abs(discretegradient(PhaseDenoised .* normalizationFactor,'x',Options.voxelAspectRatio(1),DEFAULT_GRADIENTTYPE));
            Phase_Gradient_y = abs(discretegradient(PhaseDenoised .* normalizationFactor,'y',Options.voxelAspectRatio(2),DEFAULT_GRADIENTTYPE));
            Phase_Gradient_z = abs(discretegradient(PhaseDenoised .* normalizationFactor,'z',Options.voxelAspectRatio(3),DEFAULT_GRADIENTTYPE));
        else
            Phase_Gradient_x = abs(discretegradient(Options.GradientMask.spatialMatrix  .* normalizationFactor,'x',Options.voxelAspectRatio(1),DEFAULT_GRADIENTTYPE));
            Phase_Gradient_y = abs(discretegradient(Options.GradientMask.spatialMatrix  .* normalizationFactor,'y',Options.voxelAspectRatio(2),DEFAULT_GRADIENTTYPE));
            Phase_Gradient_z = abs(discretegradient(Options.GradientMask.spatialMatrix  .* normalizationFactor,'z',Options.voxelAspectRatio(3),DEFAULT_GRADIENTTYPE));
        end
        
        if isDenoisingAfterGradientCalc
            Phase_Gradient_x = diffusionimagefilter(Phase_Gradient_x,Options.GradientMask.filterOptions);
            Phase_Gradient_y = diffusionimagefilter(Phase_Gradient_y,Options.GradientMask.filterOptions);
            Phase_Gradient_z = diffusionimagefilter(Phase_Gradient_z,Options.GradientMask.filterOptions);
        end
        
        % calculate normalization of magnitude in case of MEDI
        if Options.useMEDI
            Phase_Gradient_x_Mean = mean(Phase_Gradient_x(problemMask));
            Phase_Gradient_y_Mean = mean(Phase_Gradient_y(problemMask));
            Phase_Gradient_z_Mean = mean(Phase_Gradient_z(problemMask));
            normalizationFactor2 = mean([Phase_Gradient_x_Mean,Phase_Gradient_y_Mean,Phase_Gradient_z_Mean]);
        else
            normalizationFactor2 = 1;
        end
        
        


            GradientMask_x = Phase_Gradient_x < Options.GradientMask.gradientThreshMin * normalizationFactor2;
            GradientMask_y = Phase_Gradient_y < Options.GradientMask.gradientThreshMin * normalizationFactor2;
            GradientMask_z = Phase_Gradient_z < Options.GradientMask.gradientThreshMin * normalizationFactor2;
        
        if isLaplacianCorrection
            Options.Laplacian = '2';
            
            if isDenoisingBeforeGradientCalc
                Phase_Laplacian = abs(ifftsave(fftsave(PhaseDenoised) .* fftsave(generatelaplacian3d(gridDimensionVector,Options))));
            else
                Phase_Laplacian = abs(ifftsave(fftsave(Options.GradientMask.spatialMatrix) .* fftsave(generatelaplacian3d(gridDimensionVector,Options))));
            end
            
            Phase_Laplacian = Phase_Laplacian ./ Options.echoTime ./ Options.magneticFieldStrength;
            PhaseMask_Laplacian = Phase_Laplacian > Options.GradientMask.laplacianThreshMin;
        end
        
        
        clear PhaseDenoised Magn_Gradient_x Magn_Gradient_y Magn_Gradient_z
        
        clear MagnitudeGradientMask_x MagnitudeGradientMask_y MagnitudeGradientMask_z
        if isLaplacianCorrection
            PhaseMask_Laplacian = PhaseMask_Laplacian | forceSmoothSusceptibilityMask; % to avoid (wrong) non-zero values in PhaseMask_LaplacianSmall due to sharp edges in problemMask
            
            GradientMask_x = GradientMask_x .* ~PhaseMask_Laplacian ;   % where Laplacian of Phase is very large, Gradient Mask is set to zero
            GradientMask_y = GradientMask_y .* ~PhaseMask_Laplacian ;
            GradientMask_z = GradientMask_z .* ~PhaseMask_Laplacian ;
        end
        clear PhaseMask_Laplacian
        
        if myisfield(Options.GradientMask,'additionalCorrectionMask_x') && ~isempty(Options.GradientMask.additionalCorrectionMask_x)
            GradientMask_x = GradientMask_x & Options.GradientMask.additionalCorrectionMask_x;
            GradientMask_y = GradientMask_y & Options.GradientMask.additionalCorrectionMask_y;
            GradientMask_z = GradientMask_z & Options.GradientMask.additionalCorrectionMask_z;
        end
        
        GradientMask_x = (GradientMask_x.*problemMask) | forceSmoothSusceptibilityMask;
        GradientMask_y = (GradientMask_y.*problemMask) | forceSmoothSusceptibilityMask;
        GradientMask_z = (GradientMask_z.*problemMask) | forceSmoothSusceptibilityMask;
        
        clear Phase_Laplacian PhaseMask_LaplacianBig Phase_Gradient_x Phase_Gradient_y Phase_Gradient_z
    end
    
else
    GradientMask_x = false(gridDimensionVector);
    GradientMask_y = false(gridDimensionVector);
    GradientMask_z = false(gridDimensionVector);
end



PercentGradientMask = (sum(GradientMask_x(:))/nProblemMaskVoxelOne) * 100;
dispstatus(['Percentage of Gradient Mask: ',num2str(floor(PercentGradientMask)),'%'])


    GradientMaskResidual_x = [];
clear forceSmoothSusceptibilityMask





nPowerRegularizationVoxel = 0;
if ~myisfield(Options,'PowerRegularization') || isempty(Options.PowerRegularization)
    isPowerRegularization = false;
    dispstatus( 'Tikhonov Regularization Susc. power-term: DEFAULT (OFF)')
else
    isPowerRegularization = true;
    dispstatus(['Tikhonov Regularization Susc. power-term: (',num2str(Options.PowerRegularization.parameter),')'])
    nPowerRegularizationVoxel = nProblemMaskVoxelOne;
    if ~myisfield(Options.PowerRegularization,'spatialMatrix') || ~isempty(Options.PowerRegularization.spatialMatrix)
        PowerTermWeightingMatrix = problemMaskOne;
    else
        PowerTermWeightingMatrix = Options.PowerRegularization.spatialMatrix;
        PowerTermWeightingMatrix = 1./PowerTermWeightingMatrix;
        PowerTermWeightingMatrix(isinf(PowerTermWeightingMatrix)) = 0;
        PowerTermWeightingMatrix = PowerTermWeightingMatrix ./ max(PowerTermWeightingMatrix(:));
    end
end




nTikhonovTermSusceptibilityVoxel = 0;



%% initialization

jIterationCycle = 0;




dataArray = dataArray .* problemMask;

if nargout > 9 && ~manyFieldsBool
    % in this case we need the RDF below
    rdf = dataArray;
end



rdfVectorGoodVoxels = dataArray(problemMask);

% add tikhonov-zeros
if isPowerRegularization
    rdfVectorGoodVoxels = cat(1,rdfVectorGoodVoxels,zeros(nPowerRegularizationVoxel,1));
end
if isTikhonovRegularizationSusceptibility
    rdfVectorGoodVoxels = cat(1,rdfVectorGoodVoxels,zeros(3*nTikhonovTermSusceptibilityVoxel,1));
end



% generate dipole response to be used for inversion
boolFourierDomain = true;
    
    dataArray = generateunitdipoleresponse(gridDimensionVector, boolFourierDomain,Options,true);
    
    
    % note: dataArray is now dipole response! (before: RDF)
    
    dataArrayOrig = dataArray;
    if isfield(Options,'kSpaceUndersamplingMatrixPhaseRelaxed')
        dataArray = dataArray .* Options.kSpaceUndersamplingMatrixPhaseRelaxed;
    end
   


if isPostProcCone
    
    ConeMask = abs(dataArrayOrig) > Options.PostProcCone.threshold; % ConeMask is the real DIPOLE mask. This mask does not contain kspacesamplingmatrix

    if myisfield(Options.PostProcCone,'outerConeThreshold') && Options.PostProcCone.outerConeThreshold < Options.PostProcCone.threshold
        error('The NCF correction cone threshold must not be smaller than the normal cone threshold. This would not make sense.')
    end
    
    if isNCF
        ConeMaskOuter = abs(dataArrayOrig) > Options.PostProcCone.outerConeThreshold;
        ConeMaskIntermediate = logical(ConeMask - ConeMaskOuter);
    end
end
clear dataArrayOrig


% include linear term into dipole-response function (if chemical-shift factor is used)
if Options.chemicalShiftFactor
    error('This method may not work any more... (deprecated/development stopped)')
    %dataArray = dataArray + Options.chemicalShiftFactor * (1-fftsave(discretizesphereshell(gridDimensionVector, Options.dipoleCroppingRadius, Options.smvErrorTermThickness)));
end



% calculate RDF of constant susceptibility region (constraint)
% if boolWithConstraintMaskSusceptibility
%     rdfSusceptibilityConstraint = zeros([gridDimensionVector,nConstrainedRegionSusceptibility,nRdf]);
%     for jConstrainedRegion = 1:nConstrainedRegionSusceptibility
%         for jRdf = 1:nRdf
%             rdfSusceptibilityConstraint(:,:,:,jConstrainedRegion,jRdf) = real(ifftsave(fftsave(ConstraintMask.susceptibility(:,:,:,jConstrainedRegion)) .* dataArray(:,:,:,jRdf)));
%         end
%     end
% end

% calculate RDF of constant susceptibility region (constraint)
% if isTikhonovRegularizationSusceptibility
%     if tikhonovSusceptibilityMatrixType
%         if boolWithConstraintMaskSusceptibility
%             gradientSusceptibilityConstraint = zeros([gridDimensionVector,nConstrainedRegionSusceptibility,nRdf]);
%             for jConstrainedRegion = 1:nConstrainedRegionSusceptibility
%                 for jRdf = 1:nRdf
%                     gradientSusceptibilityConstraint(:,:,:,jConstrainedRegion,jRdf) = Options.TikhonovRegularizationSusceptibility.parameter .* tikhonovMatrixSusceptibility .* real(ifftsave(tikhonovMatrixSusceptibilityFt .* dataArray(:,:,:,jRdf)));
%                 end
%             end
%         end
%     end
% end


% preprocessing for spatial weighting
if boolUseSpatialWeightingMatrix
    suscSpatialWeightingMatrix = zeros(size(problemMask));
    suscSpatialWeightingMatrix(logical(problemMask)) = spatialWeightingMatrix(logical(problemMask));
else
    suscSpatialWeightingMatrix = ones(size(problemMask));
end
clear spatialWeightingMatrix

%% main part (iterative solving)


gradientMaskReturn = [];
clear tmp





if isTikhonovRegularizationSusceptibility
    IterationDetails.GradientMask_x = GradientMask_x;
    IterationDetails.GradientMask_y = GradientMask_y;
    IterationDetails.GradientMask_z = GradientMask_z;
end
%% postprocess results


clear phaseNoisePattern
% get susceptibility distribution

clear freeRegion
residualArray = [];
anisotropyComponent = [];
clear resultVector






if isPostProcCone
    
    dataArrayPreConeProc = suscInterim;
    
    if isfield(Options,'kSpaceUndersamplingMatrixPhaseRelaxed')
        dataArray = real(ifftsave(fftsave(suscInterim).*ConeMask .* Options.kSpaceUndersamplingMatrixPhaseRelaxed));
    else
        dataArray = real(ifftsave(fftsave(suscInterim).*ConeMask));
    end
    dataArrayFt = fftsave(dataArray);
    clear dataArray
    clear tmp
    

    clear ConeMaskNoEnergyDet
    clear dataArrayFtDenoised
    
    if isfield(Options,'kSpaceUndersamplingMatrixPhaseRelaxed') && ~isempty(Options.kSpaceUndersamplingMatrixPhaseRelaxed)
        ConeMask = ConeMask | ~Options.kSpaceUndersamplingMatrixPhaseRelaxed; % only cone inside sampled area shall be filled
    end
    
    
    PercentConeMask = (sum(~ConeMask(:))/nDegreeFreedomSuscept) * 100;
    dispstatus(['Percentage of Cone Mask: ',num2str(floor(PercentConeMask)),'%'])
    
    
    gradientMaskReturn = GradientMask_x + GradientMask_y + GradientMask_z;
     gradientMaskReturnZeroMask = (gradientMaskReturn == false) & problemMask;
      GradientMask_x = double(GradientMask_x);
      GradientMask_y = double(GradientMask_y);
      GradientMask_z = double(GradientMask_z);
      GradientMask_x(gradientMaskReturnZeroMask) = Options.gradientMaskReplaceValue;
      GradientMask_y(gradientMaskReturnZeroMask) = Options.gradientMaskReplaceValue;
      GradientMask_z(gradientMaskReturnZeroMask) = Options.gradientMaskReplaceValue;

      
     GradientMask_x = GradientMask_x.*problemMask;
     GradientMask_y = GradientMask_y.*problemMask;
     GradientMask_z = GradientMask_z.*problemMask;
     clear gradientMaskReturn
     gradientMaskReturn{1} = GradientMask_x;
     gradientMaskReturn{2} = GradientMask_y;
     gradientMaskReturn{3} = GradientMask_z;
     gradientMaskReturn{4} = single(GradientMask_x + GradientMask_y + GradientMask_z);
    
    Opts.GradientMask_x = GradientMask_x;% | ~problemMask;
    Opts.GradientMask_y = GradientMask_y;% | ~problemMask;
    Opts.GradientMask_z = GradientMask_z;% | ~problemMask;

    
    %Opts.epsilon = Options.epsilon;
    Opts.sparsityType = 'tv';
    Opts.samplingMatrixType = 'fourier';
    Opts.tolerance = Options.PostProcCone.tol;
    Opts.toleranceEnergy = Options.PostProcCone.tolEnergy;
    Opts.stopEnergy = Options.PostProcCone.stopEnergy;
    Opts.muMin = Options.PostProcCone.muMin;
    Opts.maxitInner = Options.PostProcCone.maxitInner;
    Opts.maxitMu = Options.PostProcCone.maxitMu - 1;  % because of +1 in loop below
    %Opts.ConeMask = ConeMask;
    Opts.stopTest = Options.PostProcCone.stopTest;
    Opts.voxelAspectRatio = Options.voxelAspectRatio;
    Opts.gradientType = Options.gradientType;
    Opts.isHighAccuracyMode = true;
    Diff = -1;
    
    
    Opts.epsilon = 0;
    warning('eps == 0')
    clear tmp
    
    while Opts.maxitMu < Options.PostProcCone.maxMaxitMu  &&  Diff < 0
        
        Opts.maxitMu = Opts.maxitMu + 1;
        
        % we now downsample the data to the actually acquired data. This
        % significantly speeds up the computation
        if isfield(Options,'kSpaceUndersamplingMatrixPhaseRelaxed') && ~isempty(Options.kSpaceUndersamplingMatrixPhaseRelaxed)
            [Opts.maskZC,Opts.zcUndo] = zealouscrop(Options.kSpaceUndersamplingMatrixPhaseRelaxed,[],0,[],true);
            Opts.maskZC(Opts.maskZC) = ConeMask(Options.kSpaceUndersamplingMatrixPhaseRelaxed);
        else
            Opts.maskZC = ConeMask;
        end
        %Options.tmpUSM = Opts.maskZC;
        %Opts.maskZC = Opts.maskZC | ~Options.tmpUSM; % this is needed because otherwise will fill all unsampled regions with coefficients and this may wipe out actual information
        %Options.tmpUSM = [];
        %Opts.maskZC(:,:,1) = 0;
        %Opts.maskZC = Opts.maskZC(:,:,2:end);
        % not sure why the above two lines were here. They broke my
        % computation in mice
        
        
        Opts.GradientMask_x = fourierbasedresize(Opts.GradientMask_x,size(Opts.maskZC)) > 0.5;
        Opts.GradientMask_y = fourierbasedresize(Opts.GradientMask_y,size(Opts.maskZC)) > 0.5;
        Opts.GradientMask_z = fourierbasedresize(Opts.GradientMask_z,size(Opts.maskZC)) > 0.5;
        Opts.problemMaskClosed = fourierbasedresize(Options.problemMaskClosed,size(Opts.maskZC)) > 0.5;
        Opts.voxelAspectRatio = Opts.voxelAspectRatio./(size(Opts.GradientMask_x)./size(dataArrayFt));
        Opts.dataArrayFt = fftsave((fourierbasedresize((ifftsave(dataArrayFt)),size(Opts.maskZC))));
        tic
        [dataArray,niter] = sparserecovery(Opts.dataArrayFt(Opts.maskZC),Opts.maskZC,Opts);
        toc
        
        
    end
    
    IterationDetails.EnergyDifference = Diff;
    IterationDetails.nIter = niter;
    
    
    
    
    if isNCF
        dispstatus('NCF reconstruction ...')
        
        
        dataArray(~Options.problemMaskClosed) = mean(dataArray(Options.problemMaskClosed)); % do this to eliminate waste outside that compromises the algorithm
        dataArrayFT = fftsave(dataArray);
        
        % intermediate Cone is replaced with denoised post-processed Susc information
        Options.PostProcCone.NCFFilterOptions.voxelsize = Options.voxelAspectRatio;
        dataArray_DenoisedFT = fftsave(diffusionimagefilter(dataArray,Options.PostProcCone.NCFFilterOptions));
        dataArray_DenoisedFT(~ConeMaskIntermediate) = dataArrayFT(~ConeMaskIntermediate);
        dataArray_DenoisedFT(ConeMask & ~ConeMaskIntermediate) = dataArrayFT(ConeMask & ~ConeMaskIntermediate); % this was changed right before submission of 2nd revision because it improves the diff-FT-spectrum
        if isfield(Options,'kSpaceUndersamplingMatrixPhaseRelaxed')
            dataArray_DenoisedFT = dataArray_DenoisedFT .* Options.kSpaceUndersamplingMatrixPhaseRelaxed;
        end
        clear dataArrayFT
        dataArray = real(ifftsave(dataArray_DenoisedFT));
    end
    
    %     pause(1)
    
else
    ConeMask = false(gridDimensionVector);
    if nargout > 6
        dataArrayPreConeProc = false(gridDimensionVector);
    end
end

if nargout > 9 && ~manyFieldsBool
    dipole = generateunitdipoleresponse(gridDimensionVector, boolFourierDomain,Options,true);
    phaseDiscrepancy = real(ifftsave(fftsave(dataArray) .* dipole)) - rdf;
end






end
