function figH = adshow(indata,contrast,nRowsCols,strColormap)
%ADSHOW display multiple images
%
% adshow(indata)
% adshow(indata, contrast)
% adshow(indata, contrast, nRowsCols)
% adshow(indata, contrast, nRowsCols,colormap)
%
% Purpsoe: Display multiple image frames as rectangular montage.
%          This function is a wrapper for montage.
%
% Input: indata   -   2D or 3D data set
%        contrast -   vector that defines contrast of the images
%                     by typing the lowest and highest signal intensity present in the images
%                     [low high]. If the contrast parameter is not defined
%                     than the images are scaled automatically between their
%                     lowest and highest signal intensity.
%        nRowsCols -  A 2-element vector, [NROWS NCOLS], specifying the number
%                     of rows and columns in the montage. 
%        colormap  - Uses a colormap.
%
% Changelog:
%   v1.1 - F Schweser - Bugfix: 2D data now allowed
%   v1.2 - A Deistung - volume is now squeezed to a 2D or 3D volume; 
%                       help was extended.
%   v1.3 - A Deistung - nRowsCols was added.
%   v1.4 - F Schweser - Bugfix: Now also displays int8 data
%   v1.5 - A Deistung - Bugfix: check of input parameters
%   v1.6 - F Schweser - Now flips and permutes the data so that normal
%                       orientation is achieved
%   v1.61 - F Schweser - Bugfix regarding v1.6
%   v1.7 - F Schweser - Now supports colormap
%   v1.71 - A Deistung - BugFix: colormap
%   v1.8  - F Schweser - Now returns figure handle.

%
%% Check input parameters


indata = squeeze(double(indata));
szindata = size(indata);

if ~((ndims(indata) >= 2 ) && (ndims(indata) <= 3 ))
    error('This function only supports 2D and 3D data sets.')
end

if nargin < 3 || isempty(nRowsCols)      
    nRowsCols = NaN;
    if nargin < 2 || isempty(contrast)        
        contrast = [];
    end
end   

if nargin < 4
    strColormap = 'gray';
end   


%% rearrange data to be in "normal" orientation
indata = flipdim(permute(indata,[2 1 3]),1);


%% Display the images

if size(indata,3) == 1
    figH = imshow(indata,contrast,'Colormap',colormap(strColormap));
elseif isnan(nRowsCols)
    colormap(strColormap)
    figH = montage(reshape(indata, szindata(2), szindata(1), 1,szindata(3)), 'DisplayRange', contrast);
else
    colormap(strColormap)
    figH = montage(reshape(indata, szindata(2), szindata(1), 1,szindata(3)), 'DisplayRange', contrast, 'Size', nRowsCols);
end


